package com.gazaay.core.Controller;

import com.gazaay.core.Service.GenericService;

import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class GenericController<T extends Object, ID extends Serializable> {

	@Autowired
	protected GenericService<T, ID> genericService;

	protected Class<T> daoType;

	@SuppressWarnings("unchecked")
	public GenericController() {
		this.daoType = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * Find T by this id
	 * 
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public String findByID(@PathVariable ID id, Model model) {

		T t = genericService.findOne(id, daoType);

		model.addAttribute("msg", t);

		return "A";
	}

	/**
	 * Find List of type T Objects
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public String findAll(Model model, HttpServletResponse response) {
		try {
			System.out.println("hello1");
			PrintWriter out = response.getWriter();
			List<T> list = (List<T>) genericService.findAll(daoType);
			out.println(list);
			model.addAttribute("msg", list);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "A";
	}

	/**
	 * Create and save this T object into database
	 * 
	 * @param t
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String create(@RequestBody T t, @PathVariable String name,
			Model model, HttpServletRequest request) {

		genericService.saveOrUpdate(t);
		model.addAttribute("msg", "asj");
		return "A";
	}

	/**
	 * Delete a T type Object by this id
	 * 
	 * @param id
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String deleteByID(@PathVariable ID id, Model model,
			HttpServletRequest request) {
		genericService.delete(id, daoType);
		return "A";
	}

	/**
	 * Delete this T type object from the database
	 * 
	 * @param t
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteByObject(Model model, HttpServletResponse response) {
		genericService.delete(daoType);

		// model.addAttribute("msg", "data delete");
		return "A";
	}

	/**
	 * Update this T type object from the database
	 * 
	 * @param t
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/Update", method = RequestMethod.GET)
	public String Update(@RequestBody T t, Model model,
			HttpServletRequest request) {
		genericService.saveOrUpdate(t);
		model.addAttribute("msg", "data delete");
		return "A";
	}
}
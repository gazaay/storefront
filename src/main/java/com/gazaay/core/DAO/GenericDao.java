package com.gazaay.core.DAO;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("genericDao")
public class GenericDao<T extends Object, ID extends Serializable> implements
		GenericdaoInter<T, ID> {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * save and update data
	 * 
	 * @param t
	 */
	@Override
	public void saveOrUpdate(T t) {
		sessionFactory.openSession().saveOrUpdate(t);
	}

	/**
	 * find one of the data by id
	 * 
	 */
	@Override
	public T findOne(ID id, Class<T> daoType) {
		@SuppressWarnings("unchecked")
		T t = (T) sessionFactory.openSession().createCriteria(daoType)
				.add(Restrictions.idEq(id)).uniqueResult();
		return t;
	}

	/**
	 * find the all coloumn of the data
	 * 
	 * @param daoType
	 * @return
	 */
	@Override
	public List<T> findAll(final Class<T> daoType) {
		
		@SuppressWarnings("unchecked")
		List<T> documents=(List<T>) sessionFactory.openSession().createCriteria(daoType).list();

		return documents;
	}

	/**
	 * flush the session
	 * 
	 */
	@Override
	public void flush() {
		sessionFactory.openSession().flush();

	}

	/**
	 * delete data by id
	 * 
	 */
	@Override
	public void delete(ID id, Class<T> daoType) {
		T t = findOne(id, daoType);
		sessionFactory.openSession().delete(t);

	}

	/**
	 * delete all data into database
	 * 
	 * @param daoType
	 */
	@Override
	public void delete(Class<T> daoType) {
		Session session=sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<T> documents=(List<T>) session.createCriteria(daoType).list();
		session.delete(documents);

	}
}

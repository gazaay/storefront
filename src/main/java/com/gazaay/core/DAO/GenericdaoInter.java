package com.gazaay.core.DAO;

import java.io.Serializable;
import java.util.List;

public interface GenericdaoInter<T extends Object, ID extends Serializable> {

    public T findOne(ID id, Class<T> daoType);

    public List<T> findAll(Class<T> daoType);

    public void flush();

    public void saveOrUpdate(T t);

    public void delete(ID id, Class<T> daoType);

    public void delete(Class<T> daoType);
}

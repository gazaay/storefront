package com.gazaay.core.Service;

import com.gazaay.core.DAO.GenericDao;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("genericService")
public class GenericService<T extends Object, ID extends Serializable> {

	@Autowired(required = true)
	private GenericDao<T, ID> genericDao;

	/**
	 * find the data by id
	 * 
	 * @param id
	 * @param daoType
	 * @return
	 */
	public T findOne(ID id, Class<T> daoType) {

		return this.genericDao.findOne(id, daoType);
	}

	/**
	 * find the all the throw database
	 * 
	 * @param daoType
	 * @return
	 */
	public List<T> findAll(Class<T> daoType) {
		System.out.println("hello1");
		return this.genericDao.findAll(daoType);
	}

	/**
	 * flush the session
	 */
	public void flush() {
		this.genericDao.flush();
	}

	/**
	 * saved and update the data
	 * 
	 * @param t
	 */
	public void saveOrUpdate(T t) {
		this.genericDao.saveOrUpdate(t);
	}

	/**
	 * delete the data by id
	 * 
	 * @param id
	 * @param daoType
	 */
	public void delete(ID id, Class<T> daoType) {
		this.genericDao.delete(id, daoType);
	}

	/**
	 * delete all data
	 * 
	 * @param daoType
	 */
	public void delete(Class<T> daoType) {
		this.genericDao.delete(daoType);
	}
}

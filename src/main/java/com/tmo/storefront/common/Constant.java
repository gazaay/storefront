package com.tmo.storefront.common;

public class Constant {
	public static final String Development = "Development";
	public static final String IntegrationTest = "IntegrationTest";
	public static final String Production = "Production";
	public static final String Fabrics = "fabrics";
	public static final String Collar = "collar";
	public static final String Cuff = "cuff";
	public static final String Pocket = "pocket";
	public static final String Yoke = "yoke";
	public static final String Thread = "thread";
	public static final String ShirtType = "shirt_type";
	public static final String Back = "back";
	public static final String ShirtSleeve = "shirt_sleeve";
	public static final String Buttons = "buttons";
	public static final String BottomCuts = "bottom_cuts";
	public static final String Fastening = "fastening";
	public static final String ButtonThread = "button_thread";
	public static final String CollarOuter = "collar_outer";
	public static final String CollarInner = "collar_inner";
	public static final String CuffOuter = "cuff_outer";
	public static final String CuffInner = "cuff_inner";
	public static final String PlacketOuter = "placket_outer";
	public static final String PlackerInner = "placket_inner";
	public static final String PlacketFlip = "placket_flip";
	public static final String AddOn = "add_on";
//	public static final String StandardMeasure = "standard_measure";
//	public static final String BestMeasure = "best_measure";
//	public static final String OwnMeasure = "own_measure";
	public static final String DefaultMeasureName = "default";
	public static final String Placket = "placket";
	public static final String CuffShortOuter = "cuff_short_outer";
	public static final String Seed = "SecureTh!s";
	public static final String DefaultGST = "SG_GST";
	public static final String TieFix = "tie_fix";
	public static final String Handkerchief = "handkerchief";
	public static final String OPsEmail = "order_email";
	public static final String ChineseLocale = "zh_CN";
	public static final String EnglishLocale = "en";
	public static final String Currency = "currency";
	public static final String PaypalAccount = "paypal_account";
	public static final String Embroidery = "embroidery_fonts";
	public static final String ShippingDays = "shipping_days";
	public static final String PickUpFromShop = "pickup_from_shop";
	public static final String CartItemKey = "item_seq_num";
	public static final int MAXCategory = 1000;
	public static final String AdminMeasureName = "admin";
	public static final String UserIdKey = "users_seq_num";
	
	public enum MeasurementTypes{
		BESTMEASURE,
		OWNMEASURE,
		STANDARDMEASURE
	}
}
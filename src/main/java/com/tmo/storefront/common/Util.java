package com.tmo.storefront.common;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.springframework.web.multipart.MultipartFile;

import com.tmo.storefront.domain.Address;
import com.tmo.storefront.domain.Item;
import com.tmo.storefront.web.services.InvoiceTemplateService;

public class Util {
	private static Logger logger = Logger.getLogger(Util.class);

	public static float floatToDecimal(Float value, Integer decimal)
			throws IOException {
		return Float.parseFloat(String.format("%." + decimal + "f", value));
	}

	public static String getValueFromJSONString(String key, String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		try {
			HashMap<String, Object> option = new ObjectMapper().readValue(
					jsonString, HashMap.class);
			return option.get(key).toString();
		} catch (Exception e) {
			logger.error("there is an error getting JSON value '" + key
					+ "' from JSON '" + jsonString + "'");
			return null;
		}
	}

	public static Address getAddressObjectFromJSON(String jsonString)
			throws JsonParseException, JsonMappingException, IOException {
		HashMap<String, Object> option = new ObjectMapper().readValue(
				jsonString, HashMap.class);
		Address address = new Address();
		// Sample address JSON
		// {"mailing_name":"f","mailing_phone":"f","mailing_address1":"f",
		// "mailing_address2":"f","mailing_city":"f","mailing_state":"f",
		// "mailing_postcode":"f","mailing_country":"Singapore"}
		address.setFullname((String) option.get("mailing_name"));
		address.setPhone((String) option.get("mailing_phone"));
		address.setLine1((String) option.get("mailing_address1"));
		address.setLine2((String) option.get("mailing_address2"));
		address.setCity((String) option.get("mailing_city"));
		address.setRegion((String) option.get("mailing_state"));
		address.setZip((String) option.get("mailing_postcode"));
		address.setCountry((String) option.get("mailing_country" + ""));

		return address;
	}

	public static Date addDays(int add_day) {
		Calendar c = new GregorianCalendar();
		c.set(Calendar.HOUR_OF_DAY, 0); // anything 0 - 23
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		c.add(Calendar.DATE, add_day);
		Date d1 = c.getTime();
		return d1;
	}

	public static boolean isEven(int number, int base) {
		return (number % base) == 0;
	}

	public static HashMap<String, Integer> getIntegerOptions(
			HashMap<String, Object> options) {
		HashMap<String, Integer> result = new HashMap<String, Integer>();
		for (Entry<String, Object> entry : options.entrySet()) {
			if (entry.getValue() instanceof Integer) {
				result.put(entry.getKey(),
						Integer.parseInt(entry.getValue().toString()));
			}
		}

		return result;
	}

	public static String generateRandomString() {

		SecureRandom random = new SecureRandom();
		return new BigInteger(50, random).toString(32);
	}

	public static String getFileExtention(MultipartFile file) {
		String origin = file.getOriginalFilename();
		return origin.replaceAll(".*\\.", "");
	}

	public static void createDirectory(String path) {
		File theDir = new File("/");
		String[] dirs = path.split("/");
		// if the directory does not exist, create it
		createDirectories(theDir, dirs, dirs.length);
		logger.info("DIR created");
	}

	private static void createDirectories(File root, String[] dirs, int depth) {
		if (depth == 0)
			return;
		for (String s : dirs) {
			File subdir = new File(root, s);
			if (!subdir.exists()) {
				logger.info("creating directory: " + s);
				boolean result = subdir.mkdir();
				if (result) {
					logger.info("directory created: " + s);
				}
			}
			root = subdir;
//			createDirectories(subdir, dirs, depth - 1);
		}
	}

	public String trimString(String toTrim) {
		return (toTrim != null) ? toTrim.replace(" ", "") : "";
	}
}

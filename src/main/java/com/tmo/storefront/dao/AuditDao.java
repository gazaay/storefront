package com.tmo.storefront.dao;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class AuditDao {
	Logger logger = Logger.getLogger(AuditDao.class);

	public void addTrails(String username, String action,
			String action_content, String action_template) {
		// TODO Auto-generated method stub
		logger.info("Username: " + username + " has action " + action);
		logger.info("Username: " + username + "with template "+ action_template +" with content " + action_content);
		
	}

}

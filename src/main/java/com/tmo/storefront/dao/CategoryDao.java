package com.tmo.storefront.dao;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Category;


@Component
@Transactional
public class CategoryDao {

	Logger logger = Logger.getLogger(CategoryDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public Category getCategoryById(Integer id) {
	// Just do something here and there to test conflict
		return (Category) sessionFactory.getCurrentSession()
				.createCriteria(Category.class).add(Restrictions.idEq(id))
				.uniqueResult();
	}

}

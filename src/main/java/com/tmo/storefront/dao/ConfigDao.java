package com.tmo.storefront.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Category;
import com.tmo.storefront.domain.Config;
import com.tmo.storefront.domain.Invoice;

//TMOA-250
@Component
@Transactional
public class ConfigDao {

	@Autowired
	private SessionFactory sessionFactory;
	private Logger logger = Logger.getLogger(ConfigDao.class);

	public String getValueStringByKey(String key) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Config.class).add(
				Restrictions.eq("config_name", key));
		logger.info("getting from reference factory with key:" + key);
		Config result = (Config) criteria.uniqueResult();
		return result.getConfig_value();
	}

	public Config getConfigByKey(String key) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Config.class).add(
				Restrictions.eq("config_name", key));
		logger.info("getting from reference factory with key:" + key);
		Config result = (Config) criteria.uniqueResult();
		return result;
	}

	public void saveConfig(Config config) {
		Session session = sessionFactory.getCurrentSession();

		session.saveOrUpdate(config);
	}

	@SuppressWarnings("unchecked")
	public List<Config> getList() {
		Session session = sessionFactory.getCurrentSession();

		return (List<Config>) session.createCriteria(Config.class).list();
	}

	public Config getConfigById(String id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Config.class);
		criteria = criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
		logger.debug("Getting Config object:" + id);
		return (Config) criteria.uniqueResult();
	}

}

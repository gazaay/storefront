package com.tmo.storefront.dao;

import java.util.List;

import org.apache.derby.vti.Restriction;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.tmo.storefront.domain.Alter;
import com.tmo.storefront.domain.Category;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.ShirtAttribute;

@Component
@Transactional
public class InventoryDao {

	Logger logger = Logger.getLogger(InventoryDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<InventoryAttribute> getAttributesByComponentName(
			String componentName, int maxnumber, int pagenumber) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Category.class).add(
				Restrictions.eq("name", componentName));
		logger.debug("Getting component name from category:'" + componentName
				+ "'");
		Category cat = (Category) criteria.uniqueResult();

		Criteria criteria_inv = sessionFactory.getCurrentSession()
				.createCriteria(InventoryAttribute.class)
				.createAlias("categories", "cat")
				.add(Restrictions.eq("cat.id", cat.getCategoryId()));
		// mysql doesn't support paging.
		// criteria_inv.setFirstResult(pagenumber * maxnumber);
		// criteria_inv.setMaxResults(maxnumber);
		// Criteria criteria_inv = sessionFactory.getCurrentSession()
		// .createCriteria(InventoryAttribute.class, "inv")
		// .createAlias("shirtAttributes", "shirtAttr")
		// .createCriteria("categories")
		// .add(Restrictions.eq("name", componentName));
		// criteria.setFirstResult(pagenumber * maxnumber);
		// criteria.setMaxResults(maxnumber);
		// List<InventoryAttribute> results = criteria.setProjection(
		// Projections.projectionList()
		// .add(Projections.groupProperty("inv.id"),"Id")
		// .add(Projections.groupProperty("inv.uri"),"uri")
		// .add(Projections.groupProperty("inv.price"),"price")
		// .add(Projections.groupProperty("inv.fabric_name"),"fabric_name")
		// .add(Projections.groupProperty("inv.composition"),"composition")
		// .add(Projections.groupProperty("inv.yarn"),"yarn")
		// .add(Projections.groupProperty("inv.colour"),"colour")
		// .add(Projections.groupProperty("inv.weaving"),"weaving")
		// .add(Projections.groupProperty("inv.treatment"),"treatment")
		// .add(Projections.groupProperty("inv.type"),"type")
		// .add(Projections.groupProperty("inv.new_code"),"new_code")
		// )
		// .setResultTransformer(Transformers.aliasToBean(InventoryAttribute.class))
		// .list();
		// sessionFactory.getCurrentSession().update(cat);
		List<InventoryAttribute> result = criteria_inv.list();
		return result;
	}

	public Category getCategoryById(Integer id) {
		return (Category) sessionFactory.getCurrentSession()
				.createCriteria(Category.class).add(Restrictions.idEq(id))
				.uniqueResult();
	}

	public List<Category> getCategories(int maxnumber) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Category.class);
		//logger.info("Caate");
//		criteria.setMaxResults(maxnumber);
		return criteria.list();
	}

	public Alter getAlterBy(Integer id) {
		return (Alter) sessionFactory.getCurrentSession()
				.createCriteria(Alter.class).add(Restrictions.idEq(id))
				.uniqueResult();
	}

	public InventoryAttribute getAttributesById(Integer id) {
		// TODO Auto-generated method stub
		return (InventoryAttribute) sessionFactory.getCurrentSession()
				.createCriteria(InventoryAttribute.class)
				.add(Restrictions.idEq(id)).uniqueResult();
	}

	public void saveOrUpdateInventoryAttribute(InventoryAttribute inventory) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(inventory);
	}

	public InventoryAttribute getInventoryAttributeById(Integer id) {
		return (InventoryAttribute) sessionFactory.getCurrentSession()
				.createCriteria(InventoryAttribute.class)
				.add(Restrictions.idEq(id)).uniqueResult();
	}

}

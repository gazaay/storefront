package com.tmo.storefront.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Config;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Promotion;

@Component
@Transactional
public class InvoiceDao {
	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = Logger.getLogger(InvoiceDao.class);

	public Invoice getInvoiceByNumber(String invoice_number) {
		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(Invoice.class).add(
				Restrictions.eq("number", invoice_number));
		Invoice result = (Invoice) criteria.uniqueResult();
		return result;

	}

	public Invoice saveInvoice(Invoice invoice) {
		Session session = sessionFactory.getCurrentSession();

		session.saveOrUpdate(invoice);
		session.flush();
		session.refresh(invoice);
		return invoice;
	}

	public List<Invoice> getInvoices() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Invoice.class);
		return criteria.list();
	}

	public List<Invoice> getInvoicesInfo() {
		List<Invoice> results = new ArrayList<Invoice>();
		;
		try {
			logger.debug("Get Invoices");
			Criteria criteria = sessionFactory.getCurrentSession()
					.createCriteria(Invoice.class);
			criteria = criteria.setProjection(Projections.projectionList()
					.add(Projections.property("number"))
					.add(Projections.property("username"))
					.add(Projections.property("status"))
					.add(Projections.property("promotion_code"))
					.add(Projections.property("discount"))
					.add(Projections.property("due"))
					.add(Projections.property("total")));
			// criteria = criteria.setResultTransformer(Transformers
			// .aliasToBean(Invoice.class));
			logger.debug("Query database");
			List<Object[]> objects = criteria.list();
			for (Object[] obj : objects) {
				Invoice temp = new Invoice();
				temp.setNumber((String) obj[0]);
				temp.setUsername((String) obj[1]);
				temp.setStatus((String) obj[2]);
				temp.setPromotion_code((String) obj[3]);
				temp.setDiscount((Float) obj[4]);
				Timestamp time = (Timestamp) obj[5];
				if (time != null)
					temp.setDue(new Date(time.getTime()));
				temp.setTotal((Float) obj[6]);
				results.add(temp);
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e);
		}
		return results;
	}

	public Invoice getInvoiceById(Integer id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Invoice.class);
		criteria = criteria.add(Restrictions.eq("id", id));
		logger.debug("Getting promotion object:" + id);
		return (Invoice) criteria.uniqueResult();
	}

	public Invoice getLastInvoice(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Invoice.class);
		criteria = criteria.add(Restrictions.eq("username", username));
		criteria = criteria.add(Restrictions.isNotNull("address_json"));
		criteria = criteria.addOrder(Order.desc("id"));
//TODO: not sure why doesn't work for MYSQL
		//		criteria = criteria.setFetchSize(1);
		
		List<Invoice> list = criteria.list();
		return  (list!=null && list.size() >0 ) ?(Invoice) list.get(0) : null ;
	}

}

package com.tmo.storefront.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.domain.Category;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.web.services.MeasurementService;

@Component
@Transactional
public class MeasurementDao {
	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = Logger.getLogger(MeasurementDao.class);

	public Measurement getMeasurement(String uid, String measure_type,
			String defaultmeasurename) {
		logger.debug("Getting Measurement from database: username - " + uid
				+ " type: " + measure_type + " default Measure name: " + defaultmeasurename);
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(Measurement.class)
				.add(Restrictions.eq("user_id", uid));
//		if (measure_type != null && !measure_type.equals("all")) {
//			criteria = criteria.add(Restrictions.eq("measure_type", measure_type));
//		}
		if (defaultmeasurename != null && !defaultmeasurename.equals(Constant.DefaultMeasureName)) {
			criteria = criteria.add(Restrictions.eq("measure_name", defaultmeasurename));
		}

		return (Measurement) criteria.uniqueResult();
	}

	public void createNew(Measurement newMeasure) {
		logger.debug("creating new measurement");
		sessionFactory.getCurrentSession().saveOrUpdate(newMeasure);

	}

	public void saveOrUpdate(Measurement measurement) {
		logger.debug("save or update");
		sessionFactory.getCurrentSession().saveOrUpdate(measurement);

	}

}

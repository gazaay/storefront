package com.tmo.storefront.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Promotion;

@Component
@Transactional
public class PromotionDao {
	@Autowired
	private SessionFactory sessionFactory;

	Logger logger = Logger.getLogger(PromotionDao.class);

	public List<Promotion> getPromotions() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Promotion.class);
		return (List<Promotion>) criteria.list();
	}

	public Promotion getPromotionById(String id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Promotion.class);
		criteria = criteria.add(Restrictions.eq("id", Integer.parseInt(id)));
		logger.debug("Getting promotion object:" + id);
		return (Promotion) criteria.uniqueResult();
	}

	public void saveOrUpdate(Promotion promotion) {
		sessionFactory.getCurrentSession().saveOrUpdate(promotion);

	}

	public Promotion getPromotionByCode(String promotion) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Promotion.class);
		criteria = criteria.add(Restrictions.eq("code", promotion));
		return (Promotion) criteria.uniqueResult();
	}

	public void delete(Promotion promotion) {
		sessionFactory.getCurrentSession().delete(promotion);
	}

	public void delete(Integer id) {
		Promotion p = (Promotion) sessionFactory.getCurrentSession().get(Promotion.class, id);
		 sessionFactory.getCurrentSession().delete(p);
	}

}

package com.tmo.storefront.dao;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.ReferenceDataService;

@Component
@Transactional
public class SecurityDao {

	Logger logger = Logger.getLogger(SecurityDao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private ReferenceDataService refService;


	public void saveUser(Users user) throws Exception {
		user.setId(refService.getUserId());
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	public Users getUserByUsername(String username) {
		return (Users) sessionFactory.getCurrentSession().createCriteria(Users.class)
				.add(Restrictions.eq("username", username)).uniqueResult();
	}
}

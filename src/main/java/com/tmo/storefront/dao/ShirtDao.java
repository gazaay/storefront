package com.tmo.storefront.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.derby.vti.Restriction;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.domain.Zoom;
import com.tmo.storefront.web.controller.ShirtController;

@Component
@Transactional
public class ShirtDao {

	Logger logger = Logger.getLogger(ShirtDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public void addAttribute(String name, String describtion) throws Exception {
		// TODO Auto-generated method stub
		ShirtAttribute Shirt_Attribute = new ShirtAttribute(name, describtion);
		sessionFactory.getCurrentSession().save(Shirt_Attribute);
	}

	public ShirtAttribute getAttributeByName(String name) {
		// TODO Auto-generated method stub
		return (ShirtAttribute) sessionFactory.getCurrentSession()
				.createCriteria(ShirtAttribute.class)
				.add(Restrictions.eq("name", name)).list().get(0);
	}

	public void createShirt(Shirt shirt) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(shirt);
	}

	public List<Zoom> getZoomsBy(HashMap<String, Integer> options,
			String selection) {
		String query_master =
		// "Select result.* from Zoom result"
		// + " inner join Category_Zoom cz on cz.Zoom_id = result.id "
		// + " inner join category cat on cat.id = cz.category_id "
		// + "where  cat.Name = '"
		// + selection
		// + "' and "
		// + "result.Id in (" +
		"Select DISTINCT z.* from Zoom_Inventory_Attribute a "
				+ "left outer join Zoom_Inventory_Attribute b "
				+ " on "
				+ "("
				+ getOptionStringForQuery("a.inventory_attribute_id", options)
				+ ") and "
				// + "a.inventory_attribute_id in ("
				// + getOptionStringForQuery(options)
				// + ") "
				// + "and "
				+ "a.Zoom_id = b.Zoom_id "
				+ " and a.inventory_attribute_id < b.inventory_attribute_id "
				+ "left outer join Zoom_Inventory_Attribute c "
				+ " on "
				+ "("
				+ getOptionStringForQuery("b.inventory_attribute_id", options)
				+ ") and "
				// + "b.inventory_attribute_id in ("
				// + getOptionStringForQuery(options)
				// + ") "
				// + "and "
				+ "b.Zoom_id = c.Zoom_id "
				+ " and b.inventory_attribute_id < c.inventory_attribute_id "
				// + "left outer join Zoom_Inventory_Attribute d"
				// + " on "
				// + "(" + getOptionStringForQuery("c.inventory_attribute_id",
				// options) +") and "
				// // + "c.inventory_attribute_id in ("
				// // + getOptionStringForQuery(options)
				// // + ") "
				// // + "and "
				// + "c.Zoom_id = d.Zoom_id "
				// + " and c.inventory_attribute_id < d.inventory_attribute_id "
				+ "inner join Zoom z on z.id = a.Zoom_id"
				+ " inner join Category_Zoom cz on cz.Zoom_id = z.id "
				+ " inner join Category cat on cat.id = cz.category_id "
				+ "where " + "  cat.Name = '" + selection + "' and (";
		// +" where ";

		String where_clause = " ";

		where_clause = createZoomCollarQuery(options, where_clause);
		where_clause = createZoomCuffQuery(options, where_clause);
		where_clause = createZoomButtonQuery(options, where_clause);
		where_clause = createZoomFabricQuery(options, where_clause);
		where_clause = createZoomButtonThreadQuery(options, where_clause);
		// where_clause += createZoomBackQuery(options, where_clause);

		where_clause = where_clause.substring(0, where_clause.length() - 3);
		logger.debug(where_clause);
		String group_by = ")";
		// "Group by (z.Id))";
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
				query_master + where_clause + group_by);
		logger.info("Execute SQL:" + query_master + where_clause + group_by);
		query.addEntity(Zoom.class);
		return query.list();

	}

	private String getOptionStringForQuery(String attr,
			HashMap<String, Integer> options) {
		StringBuilder result = new StringBuilder();
		Iterator<Integer> iter = options.values().iterator();
		HashMap<Integer, Integer> checker = new HashMap<Integer, Integer>();
		Collection<Integer> sortedOptions = options.values();
		List<Integer> sortedList = new ArrayList(sortedOptions);
		Collections.sort(sortedList);
		for (Integer option : sortedList) {
			if (!checker.containsKey(option)) {
				result.append(attr + "=" + option.toString() + " or ");
			}
			checker.put(option, option);
		}
		return result.delete(result.length() - 3, result.length()).toString();
	}

	private String createZoomButtonThreadQuery(
			HashMap<String, Integer> options, String where_clause) {
		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(1, new String[] { "fabrics" });
		// select_parameters.put(2, new String[] { "fabrics", "collar",
		// "collar_outer", "collar_inner", "button_thread", "buttons" });
		select_parameters.put(2, new String[] { "button_thread" });
		select_parameters.put(3, new String[] { "buttons" });
		String menu = "button_thread";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, menu);
		// where_clause = where_clause.substring(0, where_clause.length() - 3);
		return where_clause;
	}

	private String getOptionStringForQuery(HashMap<String, Integer> options) {
		StringBuilder result = new StringBuilder();
		Iterator<Integer> iter = options.values().iterator();
		HashMap<Integer, Integer> checker = new HashMap<Integer, Integer>();
		Collection<Integer> sortedOptions = options.values();
		List<Integer> sortedList = new ArrayList(sortedOptions);
		Collections.sort(sortedList);
		for (Integer option : sortedList) {
			if (!checker.containsKey(option)) {
				result.append(option.toString() + " ,");
			}
			checker.put(option, option);
		}
		return result.deleteCharAt(result.length() - 1).toString();
	}

	private String createZoomFabricQuery(HashMap<String, Integer> options,
			String where_clause) {
		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(0, new String[] { "fabrics" });
		String menu = "fabric";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, menu);
		// where_clause = where_clause.substring(0, where_clause.length() - 3);
		return where_clause;
	}

	private String createZoomBackQuery(HashMap<String, Integer> options,
			String where_clause) {
		// TODO Auto-generated method stub
		return null;
	}

	private String createZoomButtonQuery(HashMap<String, Integer> options,
			String where_clause) {
		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(1, new String[] { "fabrics" });
		// select_parameters.put(2, new String[] { "fabrics", "collar",
		// "collar_outer", "collar_inner", "button_thread", "buttons" });
		select_parameters.put(2, new String[] { "button_thread" });
		select_parameters.put(3, new String[] { "buttons" });
		String menu = "buttons";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, menu);
		// where_clause = where_clause.substring(0, where_clause.length() - 3);
		return where_clause;
	}

	private String createZoomCuffQuery(HashMap<String, Integer> options,
			String where_clause) {
		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(0, new String[] { "fabrics" });
		select_parameters.put(1, new String[] { "cuff", "cuff_outer" });
		// select_parameters.put(2, new String[] { "fabrics", "collar",
		// "collar_outer", "collar_inner", "button_thread", "buttons" });
		select_parameters.put(2, new String[] { "cuff", "cuff_inner" });
		select_parameters.put(3, new String[] { "cuff", "button_thread" });
		select_parameters.put(4, new String[] { "cuff", "buttons" });
		String menu = "cuff";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, menu);
		// where_clause = where_clause.substring(0, where_clause.length() - 3);
		return where_clause;
	}

	private String createSelfJoinQuery(HashMap<String, Integer> options,
			String where_clause, Map<Integer, String[]> select_parameters,
			String menu) {
		String cat_where = "(" + " cat.Name like '" + "%" + menu + "%"
				+ "' and ";

		if (menu == null) {
			cat_where = "(";
		}
		for (Entry<Integer, String[]> selection : select_parameters.entrySet()) {
			LinkedList<String> table_list = new LinkedList();
			table_list.add("a");
			table_list.add("b");
			table_list.add("c");
			table_list.add("d");
			String layer_id = String.format("%02d", selection.getKey());
			String[] selections_value = selection.getValue();
			where_clause += cat_where + "z.layer = " + selection.getKey() + "";
			// where_clause += "(z.layer = " + selection.getKey() + "";
			List<Integer> sortedList = new ArrayList<Integer>();
			for (String inv_id : selections_value) {
				Integer option_key = options.get(inv_id);
				if (option_key == null) {
					continue;
				}
				sortedList.add(option_key);
			}
			Collections.sort(sortedList);
			for (Integer option_key : sortedList) {
				where_clause += " and " + table_list.poll()
						+ ".inventory_attribute_id=" + option_key.toString();
			}
			where_clause += ")";
			// if (selection.getKey() != 16) {
			where_clause += " or ";
			// }
		}
		return where_clause;
	}

	private String createZoomCollarQuery(HashMap<String, Integer> options,
			String where_clause) {
		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(1, new String[] { "fabrics" });
		// select_parameters.put(2, new String[] { "fabrics", "collar",
		// "collar_outer", "collar_inner", "button_thread", "buttons" });
		select_parameters.put(3, new String[] { "fabrics", "placket" });
		select_parameters.put(4, new String[] { "collar", "collar_inner" });
		select_parameters.put(5, new String[] { "collar", "collar_outer" });
		select_parameters.put(6, new String[] { "collar", "button_thread" });
		select_parameters.put(7, new String[] { "collar", "buttons" });
		select_parameters.put(8, new String[] { "button_thread" });
		select_parameters.put(9, new String[] { "buttons" });
		String menu = "collar";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, menu);
		// where_clause = where_clause.substring(0, where_clause.length() - 3);
		return where_clause;
	}

	public List<ShirtAttribute> getAttributesBy(HashMap<String, Integer> options) {

		Map<Integer, String[]> select_parameters = new HashMap<Integer, String[]>();
		select_parameters.put(1, new String[] { "fabrics", "shirt_type",
				"bottom_cuts" });
		select_parameters.put(3, new String[] { "fabrics", "fastening" });
		select_parameters.put(4, new String[] { "fabrics", "pocket" });
		select_parameters.put(5, new String[] { "fabrics", "shirt_type",
				"shirt_sleeve" });
		select_parameters.put(6, new String[] { "fastening", "button_thread" });
		select_parameters.put(7, new String[] { "fastening", "buttons" });
		select_parameters.put(8, new String[] { "collar_inner", "collar" });
		select_parameters.put(9, new String[] { "collar_outer", "collar" });
		select_parameters.put(10, new String[] { "collar", "button_thread" });
		select_parameters.put(11, new String[] { "buttons" });
		select_parameters.put(12, new String[] { "cuff_outer", "cuff" });
		select_parameters.put(13, new String[] { "cuff_inner", "cuff" });
		select_parameters.put(14, new String[] { "cuff", "button_thread" });
		select_parameters.put(15, new String[] { "cuff", "buttons" });
		select_parameters.put(16, new String[] { "cuff", "cuff_outer" });

		String query_master =
		// "Select result.* from Shirt_Attribute result where result.Id in (" +
		"Select DISTINCT z.* from Shirt_Attribute_Inventory_Attribute a "
				+ "LEFT OUTER join Shirt_Attribute_Inventory_Attribute b on a.Shirt_Attribute_id = b.Shirt_Attribute_id "
				+ "and a.inventory_attribute_id < b.inventory_attribute_id "
				+ "and ("
				+ getOptionStringForQuery("a.inventory_attribute_id", options)
				+ ") "
				// + " and a.inventory_attribute_id in ("
				// + getOptionStringForQuery(options)
				// + ") "
				+ "LEFT OUTER join Shirt_Attribute_Inventory_Attribute c on b.Shirt_Attribute_id = c.Shirt_Attribute_id "
				+ "and b.inventory_attribute_id < c.inventory_attribute_id "
				+ "and ("
				+ getOptionStringForQuery("b.inventory_attribute_id", options)
				+ ") "
				// + " and b.inventory_attribute_id in ("
				// + getOptionStringForQuery(options)
				// + ") "
				// +
				// "LEFT OUTER join Shirt_Attribute_Inventory_Attribute d on c.Shirt_Attribute_id = d.Shirt_Attribute_id "
				// + "and c.inventory_attribute_id < d.inventory_attribute_id  "
				// + "and (" +
				// getOptionStringForQuery("a.inventory_attribute_id", options)
				// +") "
				// + " and c.inventory_attribute_id in ("
				// + getOptionStringForQuery(options)
				// + ") "
				+ "LEFT OUTER join Shirt_Attribute z on z.id = a.Shirt_Attribute_id where ";

		String where_clause = " ";

		where_clause = createSelfJoinQuery(options, where_clause,
				select_parameters, null);

		// for (Entry<Integer, String[]> selection :
		// select_parameters.entrySet()) {
		// LinkedList<String> table_list = new LinkedList();
		// table_list.add("a");
		// table_list.add("b");
		// table_list.add("c");
		// String layer_id = String.format("%02d", selection.getKey());
		// String[] selections_value = selection.getValue();
		// where_clause += "(z.attribute_name = '" + layer_id + "'";
		// for (String inv_id : selections_value) {
		// Integer option_key = options.get(inv_id);
		// if (option_key == null) {
		// continue;
		// }
		// where_clause += " and " + table_list.poll()
		// + ".inventory_attribute_id=" + option_key.toString();
		// }
		// where_clause += ")";
		// if (selection.getKey() != 16) {
		// where_clause += " or ";
		// }
		// }
		where_clause = where_clause.substring(0, where_clause.length() - 3);

		logger.debug(where_clause);
		String group_by = "";
		// " Group by (z.*)";
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
				query_master + where_clause + group_by);
		query.addEntity(ShirtAttribute.class);
		logger.info("Execute SQL:" + query_master + where_clause + group_by);
		return query.list();
		//
		// StringBuilder builder = new StringBuilder();
		// Formatter query_formatter = new Formatter(builder);
		//
		// String query_template =
		// " and (shirtA.id in (Select shirtB.id from ShirtAttribute shirtB join shirtB.inventoryAttributes invB where invB.id=%1$s) or shirtA.name not in (Select shirtB.name from ShirtAttribute shirtB join shirtB.inventoryAttributes invB where invB.id=%1$s))";
		// String query_template_master =
		// "Select shirtA from ShirtAttribute shirtA join shirtA.inventoryAttributes invA where invA.id=";
		// String query_str = query_template_master + options.get("fabrics");
		// for (Entry<String, Integer> entity : options.entrySet()) {
		// if (entity.getKey() == "fabrics") {
		// } else {
		// query_str += query_formatter.format(query_template,
		// entity.getValue());
		// }
		// logger.info(query_str);
		// }
		//
		// Query query =
		// sessionFactory.getCurrentSession().createQuery(query_str);

	}

	public List<ShirtAttribute> getAttributesBySelections(
			List<String> inventoryAttributesIds) {
		Criteria shirtAttributeCriteria = sessionFactory.getCurrentSession()
				.createCriteria(ShirtAttribute.class)
				.createCriteria("inventoryAttributes");
		Criterion orRestructions = null;
		for (String id : inventoryAttributesIds) {
			if (orRestructions == null) {
				orRestructions = Restrictions.eq("id", Integer.parseInt(id));
			} else {
				orRestructions = Restrictions.or(
						Restrictions.eq("id", Integer.parseInt(id)),
						orRestructions);
			}
		}
		shirtAttributeCriteria.add(orRestructions);
		return shirtAttributeCriteria.list();
	}

	public boolean isOptionsContainsShortSleeves(
			HashMap<String, Integer> options) {
		Criteria shortSleevesCriteria = sessionFactory
				.getCurrentSession()
				.createCriteria(InventoryAttribute.class)
				.add(Restrictions.and(Restrictions.in("id", options.values()),
						Restrictions.or(
								Restrictions.like("uri", "%short%sleeve%"),
								Restrictions.like("uri", "%sleeve%short%"))));
		return shortSleevesCriteria.list().size() > 0;
	}

	public Shirt getShirtByReferenceKey(String key) {
		Criteria shirtCriteria = sessionFactory.getCurrentSession()
				.createCriteria(Shirt.class)
				.add(Restrictions.eq("reference_key", key));
		return (Shirt) shirtCriteria.uniqueResult();
	}

	public List<Shirt> getShirtsByReferenceCriteria(String key) {
		Criteria shirtCriteria = sessionFactory.getCurrentSession()
				.createCriteria(Shirt.class)
				.add(Restrictions.like("reference_key", "%" + key + "%"));
		return shirtCriteria.list();
	}

	public void saveOrUpdate(Shirt shirt) {
		sessionFactory.getCurrentSession().saveOrUpdate(shirt);
	}

	public List<Shirt> getShirts() {
		return  sessionFactory.getCurrentSession()
		.createCriteria(ShirtAttribute.class).list();
	}

	public Shirt getShirtById(Integer id) {
		return (Shirt) sessionFactory.getCurrentSession().byId(Shirt.class).load(id);
	}

}

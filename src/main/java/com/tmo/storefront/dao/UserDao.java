package com.tmo.storefront.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.ReferenceDataService;

@Component
@Transactional
public class UserDao {
	Logger logger = Logger.getLogger(UserDao.class);

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private ReferenceDataService refService;

	public List<Users> getUsers() {
		return sessionFactory.getCurrentSession().createCriteria(Users.class)
				.list();
	}

	public Users getUserByUsername(String username) {
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(Users.class)
				.add(Restrictions.eq("username", username));
		return (Users) criteria.uniqueResult();
	}

	@Deprecated
	public boolean createOrUpdate(Users users) {

		sessionFactory.getCurrentSession().saveOrUpdate(users);
		return true;
	}

}

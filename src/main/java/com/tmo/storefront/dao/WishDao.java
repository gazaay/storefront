package com.tmo.storefront.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Wish;
import com.tmo.storefront.web.services.WishService;

@Component
@Transactional
public class WishDao {
	Logger logger = Logger.getLogger(WishDao.class);
	@Autowired
	private SessionFactory sessionFactory;

	public Wish getLatestWish(String username, String session_id) {
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(Wish.class)
				.add(Restrictions.eq("session_Id", session_id))
				.addOrder(Order.desc("modified_Date"));
		if (username != null) {
			criteria = criteria.add(nullableRestriction("username",username));
		}
		List<Wish> listOfWishes = criteria.list();
		if (listOfWishes.size() > 0) {
			return listOfWishes.get(0);
		}
		return null;
	}

	public void saveOrUpdate(Wish wish) {
		sessionFactory.getCurrentSession().saveOrUpdate(wish);
	}

	public Wish getWishBy(String username, String session_id) {
		return (Wish) sessionFactory.getCurrentSession()
				.createCriteria(Wish.class)
				.add(nullableRestriction("username", username))
				.add(nullableRestriction("session_Id", session_id)).list().get(0);
	}
	
	public List<Wish> getWishesBy(String username, String session_id) {
		return (List<Wish>) sessionFactory.getCurrentSession()
				.createCriteria(Wish.class)
				.add(nullableRestriction("username", username))
				.add(nullableRestriction("session_Id", session_id)).list();
	}

	private SimpleExpression nullableRestriction(String dbfield, String value) {
		if (value == null) {
			return Restrictions.like(dbfield, '%');
		}
		return Restrictions.eq(dbfield, value);
	}
}

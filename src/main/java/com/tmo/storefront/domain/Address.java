package com.tmo.storefront.domain;

import org.springframework.stereotype.Component;

@Component
public class Address {
	private String fullname;
	private String phone;
	private String line1;
	private String line2;
	private String city;
	private String region;
	private String zip;
	private String country;

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getFullname() {
		return fullname;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getLine2() {
		return line2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegion() {
		return region;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getZip() {
		return zip;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}
}

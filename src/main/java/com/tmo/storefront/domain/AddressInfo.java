package com.tmo.storefront.domain;

import org.springframework.stereotype.Component;


@Component
public class AddressInfo {
	
	private String mailing_name;
	private String mailing_phone;
	private String mailing_address1;
	private String mailing_address2;
	private String mailing_city;
	private String mailing_state;
	private String mailing_postcode;
	private String mailing_country;
	public void setMailing_name(String mailing_name) {
		this.mailing_name = mailing_name;
	}
	public String getMailing_name() {
		return mailing_name;
	}
	public void setMailing_phone(String mailing_phone) {
		this.mailing_phone = mailing_phone;
	}
	public String getMailing_phone() {
		return mailing_phone;
	}
	public void setMailing_address1(String mailing_address1) {
		this.mailing_address1 = mailing_address1;
	}
	public String getMailing_address1() {
		return mailing_address1;
	}
	public void setMailing_address2(String mailing_address2) {
		this.mailing_address2 = mailing_address2;
	}
	public String getMailing_address2() {
		return mailing_address2;
	}
	public void setMailing_city(String mailing_city) {
		this.mailing_city = mailing_city;
	}
	public String getMailing_city() {
		return mailing_city;
	}
	public void setMailing_state(String mailing_state) {
		this.mailing_state = mailing_state;
	}
	public String getMailing_state() {
		return mailing_state;
	}
	public void setMailing_postcode(String mailing_postcode) {
		this.mailing_postcode = mailing_postcode;
	}
	public String getMailing_postcode() {
		return mailing_postcode;
	}
	public void setMailing_country(String mailing_country) {
		this.mailing_country = mailing_country;
	}
	public String getMailing_country() {
		return mailing_country;
	}

}

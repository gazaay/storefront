package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "AlterShirt")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Alter implements Serializable {
	private String name;
	
	private Integer Id;
	
	private List<Category> categories;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return this.Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="AlterShirt_Group_Name")
	public String getName() {
		return name;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = Category.class)
	@JoinColumn(name = "AlterShirt_Id")
	public List<Category> getCategories() {
		return categories;
	}
	
	

}

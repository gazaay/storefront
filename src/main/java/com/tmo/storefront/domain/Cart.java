package com.tmo.storefront.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.web.controller.CartController;
import com.tmo.storefront.web.services.CartService;
import com.tmo.storefront.web.services.DummyCartService;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cart {
	private List<Item> items;
	private String username;
	// TODO: get it from database later TMOA-275
	private Integer shirtId = 0;
	private Float discount = 0f;
	private String shippingMethod;

	Logger logger = Logger.getLogger(Cart.class);

	public Cart() {
		items = new ArrayList<Item>();
	}

	public int getTotalItems() {
		Integer total = 0;
		for (Item tempItem : items) {
			total += tempItem.getQuantity();
		}
		return total;
	}

	public void setItem(List<Item> Item) {
		this.items = Item;
	}

	public List<Item> getItem() {
		logger.debug("Number of items: " + items.size());
		return items;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public float getTotalCost() throws JsonParseException,
			JsonMappingException, IOException {
		Integer decimal = 2;
		return Util.floatToDecimal((getTotalShirtCost() * (1 - this.discount))
				+ getShippingCost(), decimal);
	}

	public float getTotalCostWithoutShipping() throws JsonParseException,
			JsonMappingException, IOException {
		Integer decimal = 2;
		return Util.floatToDecimal(getTotalShirtCost(), decimal);
	}

	public float getTotalShirtCost() throws JsonParseException,
			JsonMappingException, IOException {
		CartService service;
		service = new DummyCartService();
		return Float.parseFloat(String.format("%.2f",
				service.getCartTotalCost(this)));
	}

	public float getShippingCost() {
		if (this.shippingMethod != null && !this.shippingMethod.isEmpty()
				&& this.shippingMethod.equals(Constant.PickUpFromShop)) {
			return 0.0f;
		}
		int items_count = getTotalItems();
		float total = 0.00f;
		if (items_count < 1) {
			total = 0.00f;
		} else if (items_count == 1) {
			total = 9.99f;
		} else if (items_count == 2) {
			total = 14.99f;
		} else if (items_count <= 10) {
			total = 14.99f + 4 * (items_count - 2);
		} else if (items_count <= 15) {
			total = 46.99f + 3 * (items_count - 10);
		} else {
			total = 61.99f + 2 * (items_count - 15);
		}
		return Float.parseFloat(String.format("%.2f", total));
	}

	public void removeItem(Integer index) {
		Item toBeRemove = items.get(index);
		Integer quantity = toBeRemove.getQuantity();
		toBeRemove.setQuantity(quantity - 1);
		if (toBeRemove.getQuantity() <= 0) {
			items.remove(index);
		}
	}

	public void setShirtId(Integer shirtId) {
		this.shirtId = shirtId;
	}

	public Integer getShirtId() {
		return shirtId;
	}

	// @JsonIgnore
	public Integer getNextShirtId() {
		return shirtId++;
	}

	public void addItem(int index) {
		Item toBeAdd = items.get(index);
		Integer quantity = toBeAdd.getQuantity();
		logger.info("Item index with : " + index + " is adding one to "
				+ quantity);
		toBeAdd.setQuantity(quantity + 1);
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public Item getItem(Integer item_id) {
		for (Item item : items) {
			if (item.getId().equals(item_id)){
				return item;
			}
		}
		return null;
	}
}

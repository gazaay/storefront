package com.tmo.storefront.domain;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartInfo {
	private int totalItems;
	private Float totalCost;
	private Float totalCostWithoutShipping;
	private Float totalShirtCost;
	private String shippingMethod;
	private Float shippingCost;
	private Float discount;

	public CartInfo(){
		
	}
	public void setTotalItems(int totalItems) {
		this.totalItems = totalItems;
	}

	public int getTotalItems() {
		return totalItems;
	}

	public void setTotalCost(Float totalCost) {
		this.totalCost = totalCost;
	}

	public Float getTotalCost() {
		return totalCost;
	}

	public void setTotalCostWithoutShipping(Float totalCostWithoutShipping) {
		this.totalCostWithoutShipping = totalCostWithoutShipping;
	}

	public Float getTotalCostWithoutShipping() {
		return totalCostWithoutShipping;
	}

	public void setTotalShirtCost(Float totalShirtCost) {
		this.totalShirtCost = totalShirtCost;
	}

	public Float getTotalShirtCost() {
		return totalShirtCost;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingCost(Float shippingCost) {
		this.shippingCost = shippingCost;
	}

	public Float getShippingCost() {
		return shippingCost;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Float getDiscount() {
		return discount;
	}

}

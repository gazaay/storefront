package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Category")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Serializable {

	private String name;

	private Integer categoryId;

	@JsonIgnore
	private List<InventoryAttribute> inventoryAttributes;

	public Category(String name) {
		this.name = name;
	}
	public Category() {
	
	}

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getCategoryId() {
		return this.categoryId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public void setInventoryAttributes(
			List<InventoryAttribute> inventoryAttributes) {
		this.inventoryAttributes = inventoryAttributes;
	}

	// @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL,
	// targetEntity = InventoryAttribute.class)
	// @JoinColumn(name = "Id")
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Inventory_Attribute_Category", inverseJoinColumns = { @JoinColumn(name = "Inventory_Attribute_Id", nullable = false, updatable = false) }, joinColumns = { @JoinColumn(name = "Category_Id", nullable = false, updatable = false) })
	public List<InventoryAttribute> getInventoryAttributes() {
		return inventoryAttributes;
	}
}

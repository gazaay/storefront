package com.tmo.storefront.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "System_Config")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Config {
	private Integer id;
	private String config_name;
	private String config_value;
	private String config_usage;
	public void setConfig_name(String config_name) {
		this.config_name = config_name;
	}
	public String getConfig_name() {
		return config_name;
	}
	public void setConfig_value(String config_value) {
		this.config_value = config_value;
	}
	public String getConfig_value() {
		return config_value;
	}
	public void setConfig_usage(String config_usage) {
		this.config_usage = config_usage;
	}
	public String getConfig_usage() {
		return config_usage;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return id;
	}
	

}

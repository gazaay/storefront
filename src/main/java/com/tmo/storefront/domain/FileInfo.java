package com.tmo.storefront.domain;

public class FileInfo {
	private String filename;
	private String fileurl;

	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}

	public String getFileurl() {
		return fileurl;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}
}

package com.tmo.storefront.domain;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm {
	 
    private List<MultipartFile> files;

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}
     
    //Getter and setter methods
}
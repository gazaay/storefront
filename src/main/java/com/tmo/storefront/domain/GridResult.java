package com.tmo.storefront.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class GridResult<T> {
	private List<T> rows;
	private String page;
	private Integer total;
	private String records;
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
	public List<T> getRows() {
		return rows;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getPage() {
		return page;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getTotal() {
		return total;
	}
	public void setRecords(String records) {
		this.records = records;
	}
	public String getRecords() {
		return records;
	}

}

package com.tmo.storefront.domain;

import java.util.List;

public class GridRow<T> {
private String id;
private T cell;
public void setId(String id) {
	this.id = id;
}
public String getId() {
	return id;
}
public void setCell(T cell) {
	this.cell = cell;
}
public T getCell() {
	return cell;
}
}

package com.tmo.storefront.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Image_storage")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageStorage {
	private Integer id;
	private Integer layer;
	private String url;
	
	@JsonIgnore
	private List<ShirtAttribute> shirtAttrs;

	public void setId(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}

	public Integer getLayer() {
		return layer;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setShirtAttrs(List<ShirtAttribute> shirtAttrs) {
		this.shirtAttrs = shirtAttrs;
	}

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = ShirtAttribute.class)
	@JoinColumn(name = "Image_storage_Id")
	public List<ShirtAttribute> getShirtAttrs() {
		return shirtAttrs;
	}

}

package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Inventory_Attribute")
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryAttribute implements Serializable {
	private String uri;

	private String uriSelected;

	private String uriCloseUp;

	private String description;

	private String new_code;

	private String fabric_name;

	private float price;

	private float price_collar_contrast;

	private float price_cuff_contrast;

	private float price_placket;

	private String composition;

	private String yarn;

	private String colour;

	private String weaving;

	private String treatment;

	private String type;

	private String old_fabric_code;

	private boolean active;

	private String selection_category;

	@JsonIgnore
	private List<Category> categories;

	@JsonIgnore
	private List<ShirtAttribute> shirtAttributes;

	@JsonIgnore
	private List<Zoom> zooms;

	private Integer Id;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return this.Id;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Column(name = "URI")
	public String getUri() {
		return uri;
	}

	public void setUriSelected(String uriSelected) {
		this.uriSelected = uriSelected;
	}

	@Column(name = "URI_Selected")
	public String getUriSelected() {
		return uriSelected;
	}

	public void setUriCloseUp(String uriCloseUp) {
		this.uriCloseUp = uriCloseUp;
	}

	@Column(name = "URI_CloseUp")
	public String getUriCloseUp() {
		return uriCloseUp;
	}

	// public void setCategory(Category category) {
	// this.category = category;
	// }
	//
	// @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL,
	// targetEntity=Category.class)
	// @JoinColumn(name = "Category_Id", referencedColumnName = "Id")
	// public Category getCategory() {
	// return category;
	// }

	public void setShirtAttributes(List<ShirtAttribute> shirtAttributes) {
		this.shirtAttributes = shirtAttributes;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Shirt_Attribute_Inventory_Attribute", inverseJoinColumns = { @JoinColumn(name = "Shirt_Attribute_Id", nullable = false, updatable = false) }, joinColumns = { @JoinColumn(name = "Inventory_Attribute_Id", nullable = false, updatable = false) })
	public List<ShirtAttribute> getShirtAttributes() {
		return shirtAttributes;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Inventory_Attribute_Category", inverseJoinColumns = { @JoinColumn(name = "Category_Id", nullable = false, updatable = false) }, joinColumns = { @JoinColumn(name = "Inventory_Attribute_Id", nullable = false, updatable = false) })
	public List<Category> getCategories() {
		return categories;

	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setNew_code(String new_code) {
		this.new_code = new_code;
	}

	public String getNew_code() {
		return new_code;
	}

	public void setFabric_name(String fabric_name) {
		this.fabric_name = fabric_name;
	}

	public String getFabric_name() {
		return fabric_name;
	}

	public void setPrice(float price) {
		this.price = Float.parseFloat(String.format("%.2f", price));
	}

	public float getPrice() {
		return Float.parseFloat(String.format("%.2f", price));
	}

	public void setPrice_collar_contrast(float price_collar_contrast) {
		this.price_collar_contrast = Float.parseFloat(String.format("%.2f",
				price_collar_contrast));
	}

	public float getPrice_collar_contrast() {
		return Float.parseFloat(String.format("%.2f", price_collar_contrast));
	}

	public void setPrice_cuff_contrast(float price_cuff_contrast) {
		this.price_cuff_contrast = Float.parseFloat(String.format("%.2f",
				price_cuff_contrast));
	}

	public float getPrice_cuff_contrast() {
		return Float.parseFloat(String.format("%.2f", price_cuff_contrast));
	}

	public void setPrice_placket(float price_placket) {
		this.price_placket = Float.parseFloat(String.format("%.2f",
				price_placket));
	}

	public float getPrice_placket() {
		return Float.parseFloat(String.format("%.2f", price_placket));
	}

	public void setComposition(String composition) {
		this.composition = composition;
	}

	public String getComposition() {
		return composition;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getColour() {
		return colour;
	}

	public void setWeaving(String weaving) {
		this.weaving = weaving;
	}

	public String getWeaving() {
		return weaving;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setOld_fabric_code(String old_fabric_code) {
		this.old_fabric_code = old_fabric_code;
	}

	public String getOld_fabric_code() {
		return old_fabric_code;
	}

	public void setYarn(String yarn) {
		this.yarn = yarn;
	}

	public String getYarn() {
		return yarn;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	public void setSelection_category(String selection_category) {
		this.selection_category = selection_category;
	}

	@Transient
	public String getSelection_category() {
		return selection_category;
	}


}

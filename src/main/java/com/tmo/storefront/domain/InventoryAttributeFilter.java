package com.tmo.storefront.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Inventory_Attribute_Filter")
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryAttributeFilter implements Serializable {
	@Column(name = "Red")
	private Integer red;
	@Column(name = "Green")
	private Integer green;
	@Column(name = "Blue")
	private Integer blue;
	@Column(name = "Pattern")
	private String pattern;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private InventoryAttribute inventoryAttribute;
	private Integer Id;

	public InventoryAttributeFilter() {
	}

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return this.Id;
	}

	public void setInventoryAttribute(InventoryAttribute inventoryAttribute) {
		this.inventoryAttribute = inventoryAttribute;
	}

	public InventoryAttribute getInventoryAttribute() {
		return inventoryAttribute;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getPattern() {
		return pattern;
	}

	public void setBlue(Integer blue) {
		this.blue = blue;
	}

	public Integer getBlue() {
		return blue;
	}

	public void setGreen(Integer green) {
		this.green = green;
	}

	public Integer getGreen() {
		return green;
	}

	public void setRed(Integer red) {
		this.red = red;
	}

	public Integer getRed() {
		return red;
	}

	public void setId(Integer id) {
		Id = id;
	}

}

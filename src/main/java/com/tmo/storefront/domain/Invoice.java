package com.tmo.storefront.domain;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;

import com.tmo.storefront.common.Util;
import com.tmo.storefront.web.services.InvoiceTemplateService;

@Entity
@Component
@Table(name = "Invoice")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Invoice {
	private Logger logger = Logger.getLogger(Invoice.class);

	private Float total;
	private Date due;
	private String items_json;

	private List<Item> items;
	private Float shipping;
	private Float beforeGST;
	private Float GST;
	private Float GSTRate;
	private String number;
	private Date issue;
	private Integer Id;
	private Float discount;
	private String promotion_code;
	private String status;
	private String address_json;
	private String measurement_json;
	private String username;
	private String remarks;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return this.Id;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Float getTotal() {
		return total;
	}

	public void setItems(List<Item> items) {
		this.items = items;
		this.setItems_json(createItemsJSON(items));
	}

	
	private String createItemsJSON(List<Item> items2) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(items2);
		} catch (JsonGenerationException e) {
			logger.error(e);
		} catch (JsonMappingException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		return this.items_json;
	}

	@Transient
	public List<Item> getItems() throws JsonParseException,
			JsonMappingException, IOException {
		TypeReference<List<Item>> typeRef = new TypeReference<List<Item>>() {
		};
		if (this.items_json == null) {
			return null;
		}
		List<Item> result = new ObjectMapper().readValue(this.items_json,
				typeRef);
		return result;
	}

	public void setShipping(Float shipping) {
		this.shipping = shipping;
	}

	public Float getShipping() throws IOException {
		if (shipping == null) {
			return 0f;
		}
		return Util.floatToDecimal(shipping, 2);
	}

	@Transient
	public Float getBeforeGST() throws IOException {
		if (total == null) {
			return 0f;
		}
		return Util.floatToDecimal((total / (1 + this.getGSTRate())), 2);
	}

	@Transient
	public Float getGST() throws IOException {
		if (GSTRate == null) {
			return 0f;
		}
		return Util.floatToDecimal(this.getBeforeGST() * this.getGSTRate(), 2);
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getNumber() {
		return number;
	}

	public void setItems_json(String items_json) {
		this.items_json = items_json;
	}

	public String getItems_json() {
		return items_json;
	}

	public void setDue(Date due) {
		this.due = due;
	}

	public Date getDue() {
		return due;
	}

	public void setIssue(Date issue) {
		this.issue = issue;
	}

	public Date getIssue() {
		return issue;
	}

	public void setGSTRate(Float gSTRate) {
		GSTRate = gSTRate;
	}

	public Float getGSTRate() {
		if (GSTRate == null) {
			return 0f;
		}
		return GSTRate;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setGST(Float gST) {
		GST = gST;
	}

	public void setBeforeGST(Float beforeGST) {
		this.beforeGST = beforeGST;
	}

	public void setAddress_json(String address) {
		this.address_json = address;
	}

	public String getAddress_json() {
		return address_json;
	}

	public void setMeasurement_json(String measurement_json) {
		logger.debug("Setting measurement json to the invoice object."
				+ measurement_json);
		this.measurement_json = measurement_json;
	}

	public String getMeasurement_json() {
		return measurement_json;
	}

	public void loadAttributes() throws JsonParseException,
			JsonMappingException, IOException {
		// TODO Auto-generated method stub
		for (Item item : this.getItems()) {
			item.loadAttributes();
		}
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setPromotion_code(String promotion_code) {
		this.promotion_code = promotion_code;
	}

	public String getPromotion_code() {
		return promotion_code;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@Transient
	public String getRemarks() {
		return remarks;
	}

	@Transient
	public Item getItem(Integer item_id) {
//		try {
//			for (Item item : this.getItems()) {
//				if (item.getId().equals(item_id)){
//					return item;
//				}
//			}
//		} catch (JsonParseException e) {
//			logger.error(e);
//		} catch (JsonMappingException e) {
//			logger.error(e);
//		} catch (IOException e) {
//			logger.error(e);
//		}
		return null;
	}

}

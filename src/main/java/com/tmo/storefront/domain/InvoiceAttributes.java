package com.tmo.storefront.domain;

import org.springframework.stereotype.Component;

@Component
public class InvoiceAttributes extends InventoryAttribute {
	private String key;
	private InventoryAttribute inventoryAttr;

	public InvoiceAttributes(String key, InventoryAttribute invatt) {
		setKey(key);
		setInventoryAttr(invatt);
	}

	public InvoiceAttributes() {
	}

	public void setInventoryAttr(InventoryAttribute inventoryAttr) {
		this.inventoryAttr = inventoryAttr;
	}

	public InventoryAttribute getInventoryAttr() {
		return inventoryAttr;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

}

package com.tmo.storefront.domain;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.web.services.InventoryService;
import com.tmo.storefront.web.services.PricingService;
import com.tmo.storefront.web.services.ShirtServices;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Serializable {
	Logger logger = Logger.getLogger(Item.class);

	
	@Autowired
	private PricingService priceService;
	
	@Autowired
	private InventoryService inventoryService;
	
	private String thumb;
    private String name;
	private Shirt shirt;
	private String customer_comments ="";
	private String operator_remarks ="";
	private float price;
	private Integer quantity = 1;
	private List<InventoryAttribute> attributes;
	private String readytowear_Id;
	private Integer id;
	

	public void setShirt(Shirt shirt) {
		this.shirt = shirt;
	}

	public Shirt getShirt() {
		return shirt;
	}

//	public float getPrice() throws JsonParseException, JsonMappingException, IOException {
////		if (priceService == null){
////			priceService = new DummyPriceService();
////		}
////		float price = priceService.getShirtPrice(shirt);
////		return price;'priceService
//		HashMap<String, Integer> option = new ObjectMapper().readValue(shirt.getPattern_json(),
//				HashMap.class);
//		return priceService.getShirtPrice(option);
//	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getThumb() {
		return thumb;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantity() {
		if (quantity < 0) {
			return 0;
		}
		return quantity;
	}

	public void setReadytowear_Id(String readytowear_Id) {
		this.readytowear_Id = readytowear_Id;
	}

	public String getReadytowear_Id() {
		return readytowear_Id;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getPrice() {
		return  Float.parseFloat(String.format("%.2f", price));
	}

	public void setName(String itemName) {
		this.name = itemName;
	}

	public String getName() {
		return name;
	}

	@Transient
	@JsonIgnore
	public List<InventoryAttribute> loadAttributes() throws JsonParseException, JsonMappingException, IOException {
		Shirt shirt = this.shirt;
		
//		logger.debug("getting attributes from item: " + pattern);
//		List<InventoryAttribute> result = inventoryService.getInventoryList(shirt.getPattern_json());
//		this.setAttributes(result);
//		return result;
		return null;
	}

	public void setAttributes(List<InventoryAttribute> attributes) {
		this.attributes = attributes;
	}

	@Transient
	@JsonIgnore
	public List<InventoryAttribute> getAttributes() {
		return attributes;
	}

	public void setCustomer_comments(String customer_comments) {
		this.customer_comments = customer_comments;
	}

	public String getCustomer_comments() {
		return customer_comments;
	}

	public void setOperator_remarks(String operator_remarks) {
		this.operator_remarks = operator_remarks;
	}

	public String getOperator_remarks() {
		return operator_remarks;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

}

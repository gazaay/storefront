package com.tmo.storefront.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Measurement")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Measurement {
	private String selection;
	private String json;
	private String measure_type;
	private String standard_size;
	private Float best_collar;
	private Float best_chest;
	private Float best_waist;
	private Float best_length_to_waist;
	private Float best_width;
	private Float best_shirt_length;
	private Float best_shoulder_width;
	private Float best_long_uplen;
	private Float best_long_lowlen;
	private Float best_short_uplen;
	private Float best_short_lowlen;
	private Float best_cuff;
	private Float best_sleeve_width;
	private Float best_short_opening;
	private Float own_neck;
	private Float own_chest;
	private Float own_len_chest;
	private Float own_waist;
	private Float own_len_waist;
	private Float own_shoulder_width;
	private Float own_seat;
	private Float own_len_seat;
	private Float own_shirt_len;
	private Float own_arm_len;
	private Float own_short_len;
	private Float own_wrist;
	private Float own_biceps;
	private Float own_armpit;
	private String measure_name;
	private String size_measure_best;
	private String size_measure_own;
	private Integer id;
	private String user_id;
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId() {
		return this.id;
	}
	
	public void setSelection(String selection) {
		this.selection = selection;
	}

	public String getSelection() {
		return selection;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getJson() {
		return json;
	}

	public void setMeasure_type(String measure_type) {
		this.measure_type = measure_type;
	}

	public String getMeasure_type() {
		return measure_type;
	}

	public void setStandard_size(String standard_size) {
		this.standard_size = standard_size;
	}

	public String getStandard_size() {
		return standard_size;
	}

	public void setBest_collar(Float best_collar) {
		this.best_collar = best_collar;
	}

	public Float getBest_collar() {
		return best_collar;
	}

	public void setBest_chest(Float best_chest) {
		this.best_chest = best_chest;
	}

	public Float getBest_chest() {
		return best_chest;
	}

	public void setBest_waist(Float best_waist) {
		this.best_waist = best_waist;
	}

	public Float getBest_waist() {
		return best_waist;
	}

	public void setBest_length_to_waist(Float best_length_to_waist) {
		this.best_length_to_waist = best_length_to_waist;
	}

	public Float getBest_length_to_waist() {
		return best_length_to_waist;
	}

	public void setBest_width(Float best_width) {
		this.best_width = best_width;
	}

	public Float getBest_width() {
		return best_width;
	}

	public void setBest_shirt_length(Float best_shirt_length) {
		this.best_shirt_length = best_shirt_length;
	}

	public Float getBest_shirt_length() {
		return best_shirt_length;
	}

	public void setBest_shoulder_width(Float best_shoulder_width) {
		this.best_shoulder_width = best_shoulder_width;
	}

	public Float getBest_shoulder_width() {
		return best_shoulder_width;
	}

	public void setBest_long_uplen(Float best_long_uplen) {
		this.best_long_uplen = best_long_uplen;
	}

	public Float getBest_long_uplen() {
		return best_long_uplen;
	}

	public void setBest_long_lowlen(Float best_long_lowlen) {
		this.best_long_lowlen = best_long_lowlen;
	}

	public Float getBest_long_lowlen() {
		return best_long_lowlen;
	}

	public void setBest_short_uplen(Float best_short_uplen) {
		this.best_short_uplen = best_short_uplen;
	}

	public Float getBest_short_uplen() {
		return best_short_uplen;
	}

	public void setBest_short_lowlen(Float best_short_lowlen) {
		this.best_short_lowlen = best_short_lowlen;
	}

	public Float getBest_short_lowlen() {
		return best_short_lowlen;
	}

	public void setBest_cuff(Float best_cuff) {
		this.best_cuff = best_cuff;
	}

	public Float getBest_cuff() {
		return best_cuff;
	}

	public void setBest_sleeve_width(Float best_sleeve_width) {
		this.best_sleeve_width = best_sleeve_width;
	}

	public Float getBest_sleeve_width() {
		return best_sleeve_width;
	}

	public void setBest_short_opening(Float best_short_opening) {
		this.best_short_opening = best_short_opening;
	}

	public Float getBest_short_opening() {
		return best_short_opening;
	}

	public void setOwn_neck(Float own_neck) {
		this.own_neck = own_neck;
	}

	public Float getOwn_neck() {
		return own_neck;
	}

	public void setOwn_chest(Float own_chest) {
		this.own_chest = own_chest;
	}

	public Float getOwn_chest() {
		return own_chest;
	}

	public void setOwn_len_chest(Float own_len_chest) {
		this.own_len_chest = own_len_chest;
	}

	public Float getOwn_len_chest() {
		return own_len_chest;
	}

	public void setOwn_waist(Float own_waist) {
		this.own_waist = own_waist;
	}

	public Float getOwn_waist() {
		return own_waist;
	}

	public void setOwn_len_waist(Float own_len_waist) {
		this.own_len_waist = own_len_waist;
	}

	public Float getOwn_len_waist() {
		return own_len_waist;
	}

	public void setOwn_shoulder_width(Float own_shoulder_width) {
		this.own_shoulder_width = own_shoulder_width;
	}

	public Float getOwn_shoulder_width() {
		return own_shoulder_width;
	}

	public void setOwn_seat(Float own_seat) {
		this.own_seat = own_seat;
	}

	public Float getOwn_seat() {
		return own_seat;
	}

	public void setOwn_len_seat(Float own_len_seat) {
		this.own_len_seat = own_len_seat;
	}

	public Float getOwn_len_seat() {
		return own_len_seat;
	}

	public void setOwn_shirt_len(Float own_shirt_len) {
		this.own_shirt_len = own_shirt_len;
	}

	public Float getOwn_shirt_len() {
		return own_shirt_len;
	}

	public void setOwn_arm_len(Float own_arm_len) {
		this.own_arm_len = own_arm_len;
	}

	public Float getOwn_arm_len() {
		return own_arm_len;
	}

	public void setOwn_short_len(Float own_short_len) {
		this.own_short_len = own_short_len;
	}

	public Float getOwn_short_len() {
		return own_short_len;
	}

	public void setOwn_wrist(Float own_wrist) {
		this.own_wrist = own_wrist;
	}

	public Float getOwn_wrist() {
		return own_wrist;
	}

	public void setOwn_biceps(Float own_biceps) {
		this.own_biceps = own_biceps;
	}

	public Float getOwn_biceps() {
		return own_biceps;
	}

	public void setOwn_armpit(Float own_armpit) {
		this.own_armpit = own_armpit;
	}

	public Float getOwn_armpit() {
		return own_armpit;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setMeasure_name(String measure_name) {
		this.measure_name = measure_name;
	}

	public String getMeasure_name() {
		return measure_name;
	}

	public void setSize_measure_best(String size_measure_best) {
		this.size_measure_best = size_measure_best;
	}

	public String getSize_measure_best() {
		return size_measure_best;
	}

	public void setSize_measure_own(String size_measure_own) {
		this.size_measure_own = size_measure_own;
	}

	public String getSize_measure_own() {
		return size_measure_own;
	}

}

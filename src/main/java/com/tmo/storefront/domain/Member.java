package com.tmo.storefront.domain;

public class Member {
private String name;
private String username;
private String password;
private boolean authenticate;
	public Member (){
		
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setAuthenticate(boolean authenticate) {
		this.authenticate = authenticate;
	}
	public boolean isAuthenticate() {
		return authenticate;
	}
}

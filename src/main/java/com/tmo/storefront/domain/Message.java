package com.tmo.storefront.domain;

import java.awt.List;
import java.util.HashMap;
import java.util.Map;

public class Message {
	private String description;
	private boolean isValid;
	private String messageCode;
	private Map<String, String> messages;
	private String content;

	public Message() {
		messages = new HashMap<String, String>();
		
		isValid = true;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void addMessage(String key, String value) {
		messages.put(key, value);
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
}

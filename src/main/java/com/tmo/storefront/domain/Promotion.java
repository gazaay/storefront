package com.tmo.storefront.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Promotion")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion {
	private Integer id;
	private String promo_type;
	private String code;
	private String target;
	private Integer limits;
	private Integer claims;
	private Float discount;
	public void setId(Integer id) {
		this.id = id;
	}
	

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId() {
		return id;
	}
	public void setPromo_type(String promo_type) {
		this.promo_type = promo_type;
	}
	public String getPromo_type() {
		return promo_type;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getTarget() {
		return target;
	}
	public void setLimits(Integer limit) {
		this.limits = limit;
	}
	public Integer getLimits() {
		return limits;
	}
	public void setClaims(Integer claims) {
		this.claims = claims;
	}
	public Integer getClaims() {
		return claims;
	}
	public void setDiscount(Float discount) {
		this.discount = discount;
	}
	public Float getDiscount() {
		return discount;
	}

}

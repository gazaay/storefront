package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import com.tmo.storefront.web.controller.CartController;

@Entity
@Table(name = "Shirt")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shirt implements Serializable {
	Logger logger = Logger.getLogger(Shirt.class);

	private String invoice;
	private String title;
	private String sub_title;
	private String details;
	private String fit;
	private String sleeve;
	private Integer id;
	private String code;
	private float price;
	private String pattern_json;
	private String reference_key;
	private String thumb_url;
	private String thumb_2_url;
	private String thumb_3_url;
	private String thumb_4_url;
	private String main_image_url;
	private Wish wish;
	private List<ShirtAttribute> attributes;
	private List<Zoom> zooms;
	private List<String> extras;

	@Id
	@GeneratedValue(generator = "identity")
	@GenericGenerator(name = "identity", strategy = "identity")
	@Column(name = "Id")
	public Integer getShirtId() {
		return this.id;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setShirtId(Integer shirtId) {
		this.id = shirtId;
	}

	public void setZooms(List<Zoom> zooms) {
		this.zooms = zooms;
	}

	@Transient
	public List<Zoom> getZooms() {
		return zooms;
	}

	public void setPattern_json(String pattern_json) {
		this.pattern_json = pattern_json;
	}

	public String getPattern_json() {
		logger.debug("Pattern JSON: " + pattern_json);
		return pattern_json;
	}

	public void setReference_key(String reference_key) {
		this.reference_key = reference_key;
	}

	public String getReference_key() {
		return reference_key;
	}

	public void setAttributes(List<ShirtAttribute> attributes) {
		// TODO Auto-generated method stub
		this.attributes = attributes;
	}

	@Transient
	public List<ShirtAttribute> getAttributes() {
		return attributes;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public void setFit(String fit) {
		this.fit = fit;
	}

	public String getFit() {
		return fit;
	}

	public void setSleeve(String sleeve) {
		this.sleeve = sleeve;
	}

	public String getSleeve() {
		return sleeve;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public void setMain_image_url(String main_image_url) {
		this.main_image_url = main_image_url;
	}

	public String getMain_image_url() {
		return main_image_url;
	}

	public void setExtras(List<String> extras) {
		this.extras = extras;
	}

	@Transient
	public List<String> getExtras() {
		logger.info("getting extra in shirt:");
		if (extras == null) {
			return new ArrayList();
		}
		for (String item : extras) {
			logger.info("extra settings: " + item);
		}
		return extras;
	}

	public void setWish(Wish wish) {
		this.wish = wish;
	}

	@OneToOne(mappedBy = "shirt", cascade = CascadeType.ALL)
	public Wish getWish() {
		return wish;
	}

	public void setSub_title(String sub_title) {
		this.sub_title = sub_title;
	}

	public String getSub_title() {
		return sub_title;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getDetails() {
		return details;
	}

	public void setThumb_2_url(String thumb_2_url) {
		this.thumb_2_url = thumb_2_url;
	}

	public String getThumb_2_url() {
		return thumb_2_url;
	}

	public void setThumb_3_url(String thumb_3_url) {
		this.thumb_3_url = thumb_3_url;
	}

	public String getThumb_3_url() {
		return thumb_3_url;
	}

	public void setThumb_4_url(String thumb_4_url) {
		this.thumb_4_url = thumb_4_url;
	}

	public String getThumb_4_url() {
		return thumb_4_url;
	}

}

package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Shirt_Attribute")
public class ShirtAttribute implements Serializable {
	private String name;

	private String path;
	
	private String layer;
	
	private ImageStorage image;
	
	@JsonIgnore
	private List<InventoryAttribute> inventoryAttributes;

//	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinTable(name = "Shirt_Shirt_Attribute", joinColumns = { @JoinColumn(name = "Shirt_Id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "Shirt_Attribute_Id", nullable = false, updatable = false) })
//	private List<Shirt> shirts;

	private Integer id;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public ShirtAttribute() {

	}

	public ShirtAttribute(String name, String path) {
		this.name = name;
		this.path = path;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "Attribute_name")
	public String getName() {
		return name;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "Attribute_description")
	public String getPath() {
		return path;
	}



	public void setInventoryAttributes(List<InventoryAttribute> inventoryAttributes) {
		this.inventoryAttributes = inventoryAttributes;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Shirt_Attribute_Inventory_Attribute", joinColumns = { @JoinColumn(name = "Shirt_Attribute_Id", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "Inventory_Attribute_Id", nullable = false, updatable = false) })
	public List<InventoryAttribute> getInventoryAttributes() {
		return inventoryAttributes;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setImage(ImageStorage image) {
		this.image = image;
	}

	@ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = ImageStorage.class)
	@JoinColumn(name = "Id")
	public ImageStorage getImage() {
		return image;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getLayer() {
		return layer;
	}

//	public void setInventoryAttributes(List<InventoryAttribute> inventoryAttributes) {
//		this.inventoryAttributes = inventoryAttributes;
//	}
//
//	public List<InventoryAttribute> getInventoryAttributes() {
//		return inventoryAttributes;
//	}

//	public void setShirts(List<Shirt> shirts) {
//		this.shirts = shirts;
//	}
//
//	public List<Shirt> getShirts() {
//		return shirts;
//	}
}

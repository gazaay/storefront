package com.tmo.storefront.domain;

import java.util.List;

public class ShirtUIModel {
//	private List<ShirtAttribute> shirtAttributes(layer, shirt, fabric, size);
//	private lookup shirtattribute and inventory - shirtattributeid, inventoryid
//	private Map<CategoryId, LayerId, URI, closeup, zoom, InventoryAttributeId>;
//	InventoryAttributeFilter (Color, pattern, )
//    Category UI

    private List<ShirtAttribute> shirtAttributes;
//    private List<Inventory> selectedInventories;
    
    
    
	public void setShirtAttributes(List<ShirtAttribute> shirtAttributes) {
		this.shirtAttributes = shirtAttributes;
	}

	public List<ShirtAttribute> getShirtAttributes() {
		return shirtAttributes;
	}

}

package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UIShirtSelection implements Serializable {

	private List<String> inventoryAttributesIds;

	public void setInventoryAttributesIds(List<String> inventoryAttributesIds) {
		this.inventoryAttributesIds = inventoryAttributesIds;
	}

	public List<String> getInventoryAttributesIds() {
		return inventoryAttributesIds;
	}
	

}

package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name = "Wish")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Wish implements Serializable {

	Logger logger = Logger.getLogger(Wish.class);

	private Shirt shirt;
	private Integer id;
	private String username;
	private String status;
	private Date created_Date;
	private Date modified_Date;
	private String session_Id;

	@Id
	@Column(name = "Id")
	@GeneratedValue(generator = "foreign")
	@GenericGenerator(name = "foreign", strategy = "foreign", parameters = @Parameter(name = "property", value = "shirt"))
	public Integer getId() {
		return this.id;
	}

	public void setShirt(Shirt shirt) {
		this.shirt = shirt;
	}

	@OneToOne
	@PrimaryKeyJoinColumn
	@JsonBackReference
	public Shirt getShirt() {
		return shirt;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setCreated_Date(Date created_Date) {
		this.created_Date = created_Date;
	}

	public Date getCreated_Date() {
		return created_Date;
	}

	public void setModified_Date(Date modified_Date) {
		this.modified_Date = modified_Date;
	}

	public Date getModified_Date() {
		return modified_Date;
	}

	public void setSession_Id(String session_Id) {
		this.session_Id = session_Id;
	}

	public String getSession_Id() {
		return session_Id;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

}
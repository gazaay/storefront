package com.tmo.storefront.domain;

import java.util.Date;

public class WishCustomerView {

	private Integer id;
	private String username;
	private Date modified_date;
	private Date created_date;
	private String shirtName;
	
	public WishCustomerView(Wish wish){
		this.setId(wish.getId());
		this.setUsername(wish.getUsername());
		this.setModified_date(wish.getModified_Date());
		this.setCreated_date(wish.getCreated_Date());
		this.setShirtName(wish.getShirt().getTitle());
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}

	public Date getModified_date() {
		return modified_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setShirtName(String shirtName) {
		this.shirtName = shirtName;
	}

	public String getShirtName() {
		return shirtName;
	}
}

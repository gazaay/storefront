package com.tmo.storefront.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Zoom")
@JsonIgnoreProperties(ignoreUnknown = true)

public class Zoom implements Serializable {
	private String description;
	
	@JsonIgnore
	private List<InventoryAttribute> inventoryAttributes;
	
	private Integer layer;
	
	private Integer Id;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Column(name = "Id")
	public Integer getId() {
		return this.Id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setInventoryAttributes(List<InventoryAttribute> inventoryAttributes) {
		this.inventoryAttributes = inventoryAttributes;
	}
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Zoom_Attribute_Category", inverseJoinColumns = { @JoinColumn(name = "Inventory_Attribute_Id", nullable = false, updatable = false) }, joinColumns = { @JoinColumn(name = "Zoom_Id", nullable = false, updatable = false) })
	public List<InventoryAttribute> getInventoryAttributes() {
		return inventoryAttributes;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}

	public Integer getLayer() {
		return layer;
	}


}

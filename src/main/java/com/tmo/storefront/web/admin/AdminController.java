package com.tmo.storefront.web.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.domain.FileUploadForm;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Item;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.controller.UsersController;
import com.tmo.storefront.web.services.InventoryService;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.MeasurementService;
import com.tmo.storefront.web.services.ReferenceDataService;
import com.tmo.storefront.web.services.UserService;

@Controller
public class AdminController {
	Logger logger = Logger.getLogger(AdminController.class);

	@Autowired
	private InventoryService service;
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private MeasurementService measurementService;
	@Autowired
	private ReferenceDataService refService;

	@ModelAttribute("authz")
	private Authentication getPrincipal() {
		Object authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		logger.info(authentication);
		if (authentication instanceof Authentication) {
			return (Authentication) authentication;
		} else
			return null;

	}

	// @Autowired
	// private AdminInvService service;
	@Autowired
	private UserService userServices;

	@RequestMapping(value = "admin{admin_pages}", method = RequestMethod.GET)
	public String showAdmin(@PathVariable String admin_pages, Model model,
			HttpServletRequest request) {
		if (admin_pages.length() <= 0) {
			return "admin/main";
		}
		// TODO Auto-generated method stub
		InventoryAttribute inv = new InventoryAttribute();
		String uri = "resources/design/thread/Icon-Thread-0001.jpg";
		inv.setUri(uri);
		Integer id = 0;
		inv.setId(id);
		String description = "testing only";
		inv.setDescription(description);
		InventoryAttribute inv1 = new InventoryAttribute();
		inv.setUri(uri);
		inv.setId(id);
		inv.setDescription(description);
		InventoryAttribute inv2 = new InventoryAttribute();
		inv.setUri(uri);
		inv.setId(id);
		inv.setDescription(description);

		String category_name = "fabrics";
		int max = 3000;
		int page = 0;
		List<InventoryAttribute> fabrics = service.getAttributes(category_name,
				max, page);
		List<InventoryAttribute> buttons = service.getAttributes(
				Constant.Buttons, max, page);
		List<InventoryAttribute> cuffs = service.getAttributes(Constant.Cuff,
				max, page);
		List<InventoryAttribute> button_threads = service.getAttributes(
				Constant.ButtonThread, max, page);
		List<InventoryAttribute> collars = service.getAttributes(
				Constant.Collar, max, page);
		List<InventoryAttribute> backs = service.getAttributes(Constant.Back,
				max, page);
		List<InventoryAttribute> yokes = service.getAttributes(Constant.Yoke,
				max, page);
		model.addAttribute("fabrics", fabrics);
		model.addAttribute("buttons", buttons);
		model.addAttribute("cuffs", cuffs);
		model.addAttribute("button_threads", button_threads);
		model.addAttribute("collars", collars);
		model.addAttribute("backs", backs);
		model.addAttribute("yokes", yokes);
		model.addAttribute("uploadForm", new FileUploadForm());
		model.addAttribute("measurementService", measurementService);
		model.addAttribute("users", userServices.getUsers());

		List<AdminUIInventoryList> configList = new ArrayList<AdminUIInventoryList>();
		configList.add(AdminUIInventoryList.newInstance()
				.setInventoryAttributeList(service, Constant.BottomCuts));
		configList.add(AdminUIInventoryList.newInstance()
				.setInventoryAttributeList(service, Constant.Fastening));
		configList.add(AdminUIInventoryList.newInstance()
				.setInventoryAttributeList(service, Constant.ShirtSleeve));
		model.addAttribute("configList", configList);

		// new ArrayList<InventoryAttribute>();
		// fabrics.add(inv);
		// fabrics.add(inv1);
		// fabrics.add(inv2);

		return "admin/" + admin_pages;
	}

	@RequestMapping(value = "adminonceoff/fix/item_id")
	public Message fixItemId(Model model, HttpServletRequest request)
			throws Exception {
		List<Invoice> invoices = invoiceService.getInvoices();

		logger.debug("number of invoices : " + invoices.size());
		for (Invoice invoice : invoices) {
			try {
				logger.debug("invoice : " + invoice.getId());
				if (invoice == null || invoice.getItems_json() == null) {
					continue;
				}
				List<Item> items = invoice.getItems();
				logger.debug("items size: " + items.size());
				for (Item item : items) {
					logger.debug("items id: " + item.getId());
					if (item.getId() == null || item.getId().equals(0)) {
						item.setId(refService.getNextItemId());
						logger.debug("item next Id: " + item.getId());
					}
				}
				invoice.setItems(items);
				invoiceService.saveOrUpdate(invoice);
				logger.debug("invoice save done " + invoice.getId());
			} catch (Exception ex) {
				logger.error("Error creating once off fix", ex);
			}
		}
		return new Message();
	}

	@RequestMapping(value = "adminonceoff/fix/extras")
	public Message fixExtras(@RequestParam(required=false) Integer shirtId,   Model model, HttpServletRequest request)
			throws Exception {
		invoiceService.batchUpdateInvoiceItems(shirtId);
		return new Message();
	}
}

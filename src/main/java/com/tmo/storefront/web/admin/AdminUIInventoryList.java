package com.tmo.storefront.web.admin;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.web.services.InventoryService;

@Service
public class AdminUIInventoryList {
	Logger logger = Logger.getLogger(AdminUIInventoryList.class);

	@Autowired
	private InventoryService service;
	private List<InventoryAttribute> list;
	private String name;

	public static AdminUIInventoryList newInstance(){
		return new AdminUIInventoryList();
	}
	public AdminUIInventoryList setInventoryAttributeList(InventoryService service, String item) {
		// TODO Auto-generated method stub
		int max = 3000;
		int page = 0;
		this.setName(item);
		this.setList(service.getAttributes(item, max, page ));
		return this;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
	}
	public String getName(){
		return name;
	}

	public void setList(List<InventoryAttribute> list) {
		this.list = list;
	}

	public List<InventoryAttribute> getList() {
		return list;
	}

}

package com.tmo.storefront.web.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.UserService;

public class BaseController {
	@Autowired
	private UserService userService;
	
	Logger logger = Logger.getLogger(BaseController.class);

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	
	@ModelAttribute("authz")
	protected Authentication getPrincipal() {
		Object authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		logger.debug(authentication);
		if (authentication instanceof Authentication) {
			return (Authentication) authentication;
		} else
			return null;
	}
	
	@ModelAttribute("user_id")
	protected Integer getCurrentUserId() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return null;
		}
		Object authentication = context.getAuthentication();
		if (authentication == null) {
			return null;
		}
		String name = ((Authentication) authentication).getName();
		Users user = ((Users) userService.getUsers(name));
		if (user == null) {
			return null;
		}
		logger.debug("Display Name:" +name + "User Id: " + user.getId() );
		return user.getId();
	}

	@ModelAttribute("display")
	private String getUserFullName() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return "";
		}
		Object authentication = context.getAuthentication();
		if (authentication == null) {
			return "";
		}
		String name = ((Authentication) authentication).getName();
		logger.debug("Display Name:" +name);
		Users user = ((Users) userService.getUsers(name));
		if (user == null) {
			return "";
		}
		return user.getFirstname() + " " + user.getLastname();
	}

	protected String getPrincipleUsername() {

		String username = null;
		Object principle = getPrincipal().getPrincipal();
		if (principle instanceof UserDetails) {
			UserDetails user = ((UserDetails) principle);
			username = user.getUsername();
		}
		return username;
	}
	
	protected void doAutoLogin(String username, String password,
			HttpServletRequest request) {

		try {
			// Must be called from request filtered by Spring Security,
			// otherwise SecurityContextHolder is not updated
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					username, password);
			token.setDetails(new WebAuthenticationDetails(request));
			Authentication authentication = authenticationManager
					.authenticate(token);
			logger.info("Logging in with [{}]" + authentication.getPrincipal());
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (Exception e) {
			SecurityContextHolder.getContext().setAuthentication(null);
			logger.error("Failure in autoLogin", e);
		}

	}
}

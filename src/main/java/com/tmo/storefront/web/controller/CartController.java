package com.tmo.storefront.web.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.CartInfo;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Item;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Member;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.OrderDetails;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.CartService;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.MeasurementService;
import com.tmo.storefront.web.services.OrderService;
import com.tmo.storefront.web.services.PricingService;
import com.tmo.storefront.web.services.PromotionService;
import com.tmo.storefront.web.services.ReferenceDataService;
import com.tmo.storefront.web.services.SecurityService;
import com.tmo.storefront.web.services.ShirtServices;

@Controller
public class CartController extends BaseController {
	@Autowired
	// @Qualifier(Constant.Development)
	CartService service;

	@Autowired
	MeasurementService measureService;
	@Autowired
	private ReferenceDataService refService;

	@Autowired
	PromotionService promotionService;

	@Autowired
	PricingService pservice;
	@Autowired
	ShirtServices shirtservice;

	@Autowired
	SecurityService securityService;

	@Autowired
	InvoiceService invoiceservice;

	Logger logger = Logger.getLogger(CartController.class);

	@Autowired
	private OrderService orderservice;

	@RequestMapping(value = "/cart/info", method = RequestMethod.GET)
	@ResponseBody
	public CartInfo getCartInfo(
			@RequestParam(required = false) String temp_username, Model model,
			HttpServletRequest request) throws JsonParseException,
			JsonMappingException, IOException {
		try {
			logger.info("Temp user get info: " + temp_username);
			if (temp_username.equals("default")) {
				return new CartInfo();
			}
			String username = super.getPrincipleUsername();

			if (username == null) {
				username = temp_username;
			}
			CartInfo cart = service.cloneCartInfo(service.getCart(username));
			logger.info("Cart Size: " + cart.getTotalItems());
			return cart;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}

	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	@ResponseBody
	public Cart getCart(@RequestParam(required = false) String temp_username,
			Model model, HttpServletRequest request) {

		if (temp_username.equals("default")) {
			return new Cart();
		}
		String username = super.getPrincipleUsername();

		if (username == null) {
			username = temp_username;
		}
		Cart cart = service.getCart(username);
		logger.info("Cart Size: " + cart.getTotalItems());
		return cart;
	}

	@RequestMapping(value = "/cart/carryover", method = RequestMethod.GET)
	@ResponseBody
	public Message carryCartToUser(
			@RequestParam(required = false) String temp_username, Model model,
			HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		if (username == null) {
		} else {
			Cart cart = service.getCart(temp_username);
			cart.setUsername(username);
			service.updateCart(username, cart);
		}
		return new Message();
	}

	@RequestMapping(value = "/cart", method = RequestMethod.POST)
	@ResponseBody
	public Cart updateCart(@RequestBody Cart cart,
			@RequestParam(required = false) String username, Model model,
			HttpServletRequest request) throws JsonParseException,
			JsonMappingException, IOException {
		if (!(super.getPrincipal().getPrincipal() != null)) {
			return null;
		}
		if (username == null) {
			username = super.getPrincipleUsername();
		}
		if (username == null && cart.getUsername() == null) {
			username = "UUID" + UUID.randomUUID().toString();
			cart.setUsername(username);
		} else if (username != null) {
			cart.setUsername(username);
		} else if (cart.getUsername() != null) {
			username = cart.getUsername();
		}
		for (Item item : cart.getItem()) {
			if (item.getShirt().getReference_key() != null) {
				// ready to wear
				Shirt tempShirt = shirtservice.getShirtByReferenceKey(item
						.getShirt().getReference_key());
				if (!tempShirt.getTitle().contains("Special")) {
					item.setName("MEN's SHIRT - " + tempShirt.getTitle());
				} else {
					item.setName("Special - " + tempShirt.getTitle());
				}
				item.setPrice(tempShirt.getPrice());
				if (item.getShirt().getShirtId() == null
						|| item.getShirt().getShirtId() <= 0) {
					// TODO: we should get it from database TMOA-275
					logger.debug("Shirt Id:" + item.getShirt().getShirtId());
					item.getShirt().setShirtId(cart.getNextShirtId());
				}
			} else {
				HashMap<String, Integer> option = new ObjectMapper().readValue(
						item.getShirt().getPattern_json(), HashMap.class);
				item.setName("MEN's SHIRT - "
						+ shirtservice.getFabricName(option));
				item.setPrice(pservice.getShirtPrice(option));
				item.getShirt().getExtras();
				if (item.getShirt().getShirtId() == null
						|| item.getShirt().getShirtId() <= 0) {
					// TODO: we should get it from database TMOA-275
					logger.debug("Shirt Id:" + item.getShirt().getShirtId());
					item.getShirt().setShirtId(cart.getNextShirtId());
				}
			}
			if (item.getId() == null) {
				item.setId(refService.getNextItemId());
			}
		}
		logger.info("Updating cart for user" + username);
		service.updateCart(username, cart);
		return cart;
	}

	@RequestMapping(value = "/issueInvoice/{invoice_number}", method = RequestMethod.POST)
	@ResponseBody
	public Message issueInvoice(@PathVariable String invoice_number,
			Model model, HttpServletRequest request) throws JsonParseException,
			JsonMappingException, IOException, MessagingException {
		String username = super.getPrincipleUsername();
		Message message = new Message();

		// TODO: issues customer invoice
		message = invoiceservice
				.issueInvoice(invoice_number, username, message);
		return message;
	}

	@RequestMapping(value = "/payment/submit", method = RequestMethod.POST)
	@ResponseBody
	public Message submitPayment(@RequestBody OrderDetails order, Model model,
			HttpServletRequest request) throws Exception {
		Message message = new Message();
		String username = super.getPrincipleUsername();
		if (order.getPromotion().length() <= 0) {
			message.setValid(false);
			message.setDescription("Promotion Purchase require a Promotion code.");
			return message;
		}
		message = promotionService.processCart(order.getCart(),
				order.getPromotion(), username, message);
		if (!message.isValid()) {
			return message;
		}

		Invoice invoice = service.createInvoice(order.getCart(),
				order.getPromotion(), order.getAddress());
		invoice.setStatus("Pending");
		invoice = invoiceservice.saveInvoice(invoice);
		if (order.getCart().getTotalCost() <= 0) {

			invoice.setNumber(invoice.getId().toString());
			measureService.saveMeasureSelection(username,
					order.getMeasurement());
			invoice.setMeasurement_json(measureService.convertToJSON(username,
					order.getMeasurement()));
			invoice = invoiceservice.saveInvoice(invoice);

			logger.info("Invoice number " + invoice.getNumber() + "saved");

			String fullURL = request.getContextPath();
			String returnURL = fullURL + "/payment?invoice_number="
					+ invoice.getNumber();

			// TODO: save to database
			logger.debug(order.getAddress());
			message = invoiceservice.issueInvoice(invoice.getNumber(),
					username, message);
			message.addMessage("returnUrl", returnURL);
			message.setContent(returnURL);
			Cart purchased_cart = service.getCart(username);
			purchased_cart.setItem(new ArrayList<Item>());
			service.updateCart(username, purchased_cart);
			return message;
		} else {
			orderservice.setOrder(order, username);
			message.setValid(true);
			message.setContent(String.valueOf(order.getCart().getDiscount()));
			message.setDescription("Proceed to Paypal");
			return message;
		}
	}

	@RequestMapping(value = "/payment/order", method = RequestMethod.POST)
	@ResponseBody
	public Message submitSaveOrder(@RequestBody OrderDetails order,
			Model model, HttpServletRequest request) throws Exception {

		Message message = new Message();
		String username = super.getPrincipleUsername();

		if (order.getPromotion().length() > 0) {
			message = promotionService.processCart(order.getCart(),
					order.getPromotion(), username, message);
			if (!message.isValid()) {
				return message;
			}
		}

		// orderservice.setOrder(order, username);
		Invoice invoice = service.createInvoice(order.getCart(),
				order.getPromotion(), order.getAddress());
		invoice.setStatus("Pending");

		measureService.saveMeasureSelection(username, order.getMeasurement());
		invoice.setMeasurement_json(measureService.convertToJSON(username,
				order.getMeasurement()));
		invoice = invoiceservice.saveInvoice(invoice);
		invoice.setUsername(username);
		String invoice_number = invoice.getId().toString();
		invoice.setNumber(invoice_number);
		invoice = invoiceservice.saveInvoice(invoice);

		message = invoiceservice.issueOperatorEmail(invoice.getNumber(),
				username, message);
		// TODO: util class to create JSON message
		String contentJSON = "{ \"invoice_number\": " + invoice_number + ","
				+ " \"discount\":"
				+ String.valueOf(order.getCart().getDiscount()) + "}";

		message.setContent(contentJSON);
		// TODO: save to database
		logger.debug(order.getAddress());
		return message;
	}

	@RequestMapping(value = "/payment/paypalsubmit", method = RequestMethod.GET)
	public String submitPaypalPayment(@RequestParam String invoice_number,
			Model model, HttpServletRequest request) throws Exception {
		Message message = new Message();
		String username = super.getPrincipleUsername();
		Invoice invoice = invoiceservice.getInvoiceByNumber(invoice_number);
		invoice.setStatus("Payment Received");
		invoice = invoiceservice.saveInvoice(invoice);

		logger.info("Invoice number " + invoice.getNumber() + "saved");

		String fullURL = request.getContextPath();
		String returnURL = "/payment?invoice_number=" + invoice.getNumber();

		message = invoiceservice.issueOperatorEmail(invoice.getNumber(),
				username, message);
		message.addMessage("returnUrl", fullURL + returnURL);
		message.addMessage("cancelUrl", fullURL + "/cartsummary");
		message.setContent(returnURL);
		Cart purchased_cart = service.getCart(username);
		purchased_cart.setItem(new ArrayList<Item>());
		service.updateCart(username, purchased_cart);
		return "redirect:" + returnURL;
	}

	@RequestMapping(value = "/cart", method = RequestMethod.DELETE)
	public @ResponseBody
	boolean clearCart(Model model, HttpServletRequest request) {
		try {
			String username = super.getPrincipleUsername();
			Cart purchased_cart = service.getCart(username);
			purchased_cart.setItem(new ArrayList<Item>());
			service.updateCart(username, purchased_cart);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@RequestMapping(value = "/payment/confirm", method = RequestMethod.POST)
	@ResponseBody
	public Message confirmPayment(@RequestParam String invoice_number,
			@RequestParam(required = false) String send_invoice, Model model,
			HttpServletRequest request) throws JsonParseException,
			JsonMappingException, IOException {
		Message message = new Message();
		Invoice invoice = invoiceservice.getInvoiceByNumber(invoice_number);
		String username = invoice.getUsername();
		invoice.setStatus("Payment Confirmed");
		invoice = invoiceservice.saveInvoice(invoice);
		logger.info("Invoice confirmed by operator - Invoice number "
				+ invoice.getNumber());
		try {
			if (send_invoice.equals("true")) {
				message = invoiceservice.issueInvoice(invoice_number, username,
						message);
			}
		} catch (Exception e) {
			logger.error(e);
			logger.error(e);
			message.setValid(false);
			message.setDescription(e.getMessage());

		}
		return message;
	}

	@RequestMapping(value = "/test_invoice", method = RequestMethod.GET)
	@ResponseBody
	public void testInvoice(@RequestParam String temp_username, Model model,
			HttpServletRequest request) throws Exception {
		InvoiceService invoice = new InvoiceService();
		// invoice.generateInvoice(new Cart());
	}

	@RequestMapping(value = "/payment", method = RequestMethod.GET)
	public String showPayment(@RequestParam String invoice_number, Model model,
			HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		Util util = new Util();
		Users user = securityService.getUserByUsername(username);
		// Get Invoice from Database
		Invoice invoice = invoiceservice.getInvoiceByNumber(invoice_number);
		model.addAttribute("user", user);
		model.addAttribute("invoice", invoice);
		model.addAttribute("util", new Util());
		Object shipping_days = refService
				.getReferenceValue(Constant.ShippingDays);
		model.addAttribute("shipping_date",
				util.addDays(Integer.parseInt((String) shipping_days)));
		model.addAttribute("util", util);
		return "payment";
	}

	@RequestMapping(value = "/cart/returnUrl", method = RequestMethod.GET)
	public @ResponseBody
	String cartReturnUrl(@RequestParam String invoice_number, Model model,
			HttpServletRequest request) {
		String fullURL = request.getContextPath();
		String returnURL = fullURL + "payment?invoice_number=";
		return returnURL;
	}

	@RequestMapping(value = "/cart/item", method = RequestMethod.DELETE)
	@ResponseBody
	public Cart removeItem(@RequestParam(required = false) Integer shirt_id,
			Model model, HttpServletRequest request) {

		String username = super.getPrincipleUsername();

		if (username == null) {
			Cart cart = service.getCart(username);
			return cart;
		} else {
			service.removeItemByShirtId(service.getCart(username), shirt_id);
			return service.getCart(username);
		}
	}

	@RequestMapping(value = "/cart/item", method = RequestMethod.POST)
	@ResponseBody
	public Cart addItem(@RequestParam(required = false) Integer shirt_id,
			Model model, HttpServletRequest request) {

		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {
			Cart cart = service.getCart(username);
			return cart;
		} else {
			service.addItemByShirtId(service.getCart(username), shirt_id);
			return service.getCart(username);
		}
	}

	@RequestMapping(value = "/cart/item", method = RequestMethod.GET)
	@ResponseBody
	public Cart getItem(@RequestParam(required = false) Integer shirt_id,
			Model model, HttpServletRequest request) {

		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {
			Cart cart = service.getCart(username);
			return cart;
		} else {
			return service.getCart(username);
		}
	}

	@RequestMapping(value = "/cart/shipping", method = RequestMethod.PUT)
	@ResponseBody
	public Cart updateShipping(@RequestParam String method, Model model,
			HttpServletRequest request) {

		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {

		} else {
			service.updateShipping(service.getCart(username), method);
		}
		return service.getCart(username);
	}

	@RequestMapping(value = "/cart/promotion", method = RequestMethod.PUT)
	@ResponseBody
	public Cart updatePromotion(@RequestParam String promotion_code,
			Model model, HttpServletRequest request) {

		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {

		} else {
			service.updatePromotionCode(service.getCart(username),
					promotion_code, username);
		}
		return service.getCart(username);
	}

	@RequestMapping(value = "/cart/item/{item_id}/comments", method = RequestMethod.POST)
	@ResponseBody
	public Cart addOrUpdateComments(@PathVariable Integer item_id,
			@RequestBody String comments) {
		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {
			return null;
		} else {
			Cart result = service.getCart(username);
			result = service.updateItemComments(result, item_id, comments);
			service.updateCart(username, result);
			return result;
		}
	}

	@RequestMapping(value = "/cart/item/{item_id}/remarks", method = RequestMethod.POST)
	@ResponseBody
	public Cart addOrUpdateRemarks(@PathVariable Integer item_id,
			@RequestBody String remarks) {
		String username = super.getPrincipleUsername();
		logger.debug("Username:" + username);
		if (username == null) {
			return null;
		} else {
			Cart result = service.getCart(username);
			result = service.updateItemRemarks(result, item_id, remarks);
			service.updateCart(username, result);
			return result;

		}
	}
}

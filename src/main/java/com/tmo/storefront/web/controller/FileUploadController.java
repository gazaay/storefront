package com.tmo.storefront.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUpload;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import org.springframework.web.portlet.ModelAndView;

import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.FileInfo;
import com.tmo.storefront.domain.FileUploadForm;

@Controller
public class FileUploadController {

	Logger logger = Logger.getLogger(FileUploadController.class);

	@RequestMapping(value = "show", method = RequestMethod.GET)
	public String displayForm() {
		return "file_upload_form";
	}

	/**
	 * @param userdefineFileName
	 * @param model
	 * @param request
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody FileInfo save(@RequestParam(required=false) String userdefineFileName, Model model, DefaultMultipartHttpServletRequest request) throws IllegalStateException, IOException {
		String fileName=Util.generateRandomString();
		if (userdefineFileName!= null) {
			fileName = userdefineFileName;
		}
		String path = System.getProperty("user.home");
		path += "/apps/tmo/upload";
		Util.createDirectory(path);
//		String path = "/Users/user/storefront/upload/";
		FileInfo result = new FileInfo();
		for (MultipartFile file : request.getFiles("files[]")) {
			String fileType = Util.getFileExtention(file);
			logger.debug("Get File Type from file: " +  fileType);
			fileName += "." +fileType;
			String targetPath = path + "/"+ fileName;
			logger.info("Saving File to : " + targetPath);
			File target = new File(targetPath);
			target.createNewFile();
			logger.info("Receieved file with name: " + file.getOriginalFilename());
			file.transferTo(target);
			result.setFilename(fileName);
			result.setFileurl("resources/upload/" + fileName);
		}
		return result ;
	}
}

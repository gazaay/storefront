package com.tmo.storefront.web.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.Alter;
import com.tmo.storefront.domain.Category;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.web.services.InventoryAttributeComparator;
import com.tmo.storefront.web.services.InventoryService;
import com.tmo.storefront.web.services.ShirtServices;

@Controller
@RequestMapping(value = "inv")
public class InventoryController {

	Logger logger = Logger.getLogger(InventoryController.class);

	@Autowired
	private InventoryService service;

	@RequestMapping(value = "/category_name/{category_name}", method = RequestMethod.GET)
	public @ResponseBody
	List<InventoryAttribute> getFabric(@PathVariable String category_name,
			@RequestParam int max, @RequestParam int page) throws Exception {
		String type = "";
		List<InventoryAttribute> list = service.getAttributes(category_name,
				max, page);
		InventoryAttributeComparator compare = new InventoryAttributeComparator();
		java.util.Collections.sort(list, compare);
		return list;
	}

	@RequestMapping(value = "/category/{Id}", method = RequestMethod.GET)
	public @ResponseBody
	Category getCategory(@PathVariable Integer Id) throws Exception {
		String type = "";

		return service.getCategoryById(Id);
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public @ResponseBody
	List<Category> getCategories(@RequestParam int max) throws Exception {
		String type = "";

		return service.getCategories(max);
	}

	@RequestMapping(value = "/alter/{Id}", method = RequestMethod.GET)
	public @ResponseBody
	Alter getAlter(@PathVariable Integer Id) throws Exception {
		String type = "";

		return service.getAlterBy(Id);
	}

	@RequestMapping(value = "/{Id}", method = RequestMethod.PUT)
	public @ResponseBody
	Message updateMessage(@PathVariable Integer Id,
			@RequestBody InventoryAttribute inventory) throws Exception {
		Message msg = new Message();
		service.updateInventoryFromAdmin(Id, inventory);

		msg.setDescription("Inventory updated successfully!");
		return msg;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody
	Message createMessage(
			@RequestBody InventoryAttribute inventory) throws Exception {
		Message msg = new Message();
		service.createInventory(inventory);
		
		msg.setDescription("Inventory updated successfully!");
		return msg;
	}

	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
	public @ResponseBody
	InventoryAttribute getInventoryAttribute(@PathVariable Integer Id)
			throws Exception {
		return service.getInventoryAttributeById(Id);
	}

}

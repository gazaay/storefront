package com.tmo.storefront.web.controller;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Address;
import com.tmo.storefront.domain.AddressInfo;
import com.tmo.storefront.domain.GridResult;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.web.services.InventoryService;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.LocaleService;
import com.tmo.storefront.web.services.ShirtServices;
import com.tmo.storefront.web.services.UserService;

@Controller
public class InvoiceController extends BaseController {
	@Autowired
	InvoiceService service;
	@Autowired
	InventoryService inventoryService;

	@Autowired
	UserService userService;

	@Autowired
	LocaleService localeService;

	@Autowired
	ShirtServices shirtService;

	@RequestMapping(value = "/invoices", method = RequestMethod.GET)
	public @ResponseBody
	GridResult<Invoice> getInvoices(Model model, HttpServletRequest request) {
		List<Invoice> list = service.getInvoicesInfo();
		GridResult<Invoice> results = new GridResult<Invoice>();
		results.setRows(list);
		results.setPage("1");
		results.setRecords(String.valueOf(list.size()));
		results.setTotal(Integer.valueOf(1));

		return results;
	}

	@RequestMapping(value = "/invoice/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Invoice getInvoiceDetail(@PathVariable String id, Model model,
			HttpServletRequest request) {
		return service.getInvoice(id);
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.GET)
	public @ResponseBody
	Invoice getInvoiceDetailByInvoiceNumber(
			@RequestParam String invoice_number, Model model,
			HttpServletRequest request) {
		return service.getInvoiceByNumber(invoice_number);
	}

	@RequestMapping(value = "/invoice/{id}", method = RequestMethod.PUT)
	public @ResponseBody
	Message updateInvoice(@PathVariable String id,
			@RequestBody Invoice invoice, Model model,
			HttpServletRequest request) {
		return service.saveOrUpdate(id, invoice);
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	public @ResponseBody
	Message createNewInvoice(@RequestBody Invoice invoice, Model model,
			HttpServletRequest request) {
		if (invoice.getId() < 0) {
			invoice.setId(null);
		}
		return service.saveOrUpdate(invoice);

	}

	@RequestMapping(value = "/invoice/reissue", method = RequestMethod.POST)
	public @ResponseBody
	Message reIssueInvoice(@RequestParam String invoice_number, Model model,
			HttpServletRequest request) {
		Message message = new Message();
		Invoice invoice = service.getInvoiceByNumber(invoice_number);
		String username = invoice.getUsername();
		logger.info("Reissuing Invoice - Invoice number " + invoice.getNumber());
		try {
			message = service.issueInvoice(invoice_number, username, message);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
			message.setValid(false);
			message.setDescription(e.getMessage());

		}
		return message;
	}

	@RequestMapping(value = "/invoice/reassign", method = RequestMethod.POST)
	public @ResponseBody
	Message reAssignInvoice(@RequestParam String invoice_number,
			@RequestParam String username, Model model,
			HttpServletRequest request) {
		Message message = new Message();
		Invoice invoice = service.getInvoiceByNumber(invoice_number);
		invoice.setUsername(username);
		service.saveInvoice(invoice);
		logger.info("Reassign Invoice - Invoice number " + invoice.getNumber()
				+ " to username: " + username);
		return message;
	}

	@RequestMapping(value = "/admininvoice_view", method = RequestMethod.GET)
	public String showAdminViewInvoiceSummary(
			@RequestParam String invoice_number, Model model,
			HttpServletRequest request) {

		// Get Invoice from Database
		Invoice invoice = service.getInvoiceByNumber(invoice_number);

		model.addAttribute("user", userService.getUsers(invoice.getUsername()));
		model.addAttribute("users", userService.getUsers());
		model.addAttribute("invoice", invoice);
		model.addAttribute("util", new Util());
		model.addAttribute("service", inventoryService);
		model.addAttribute("shirtService", shirtService);
		model.addAttribute("localeService", localeService);
		model.addAttribute("date", new DateTool());
		model.addAttribute("num", new NumberTool());

		return "admin/invoice_view";

	}
	
	@RequestMapping(value = "/invoice/{invoice_number}/item/{item_id}/remarks", method = RequestMethod.POST)
	public @ResponseBody
	Message updateRemarks(
			@PathVariable Integer item_id,
			@PathVariable Integer invoice_number,
			@RequestBody String remarks, Model model,
			HttpServletRequest request) {
		Message message = new Message();
		Invoice invoice = service.getInvoiceByNumber( invoice_number.toString());
        service.updateItemRemarks(invoice, item_id, remarks);
		return message;
	}

	@RequestMapping(value = "/invoice/address", method = RequestMethod.GET)
	public @ResponseBody AddressInfo getPreviousInvoiceAddress() {
		String username = super.getPrincipleUsername();
		AddressInfo address = service.getAddressFromLastInvoice(username);
		return address;
	}
	// @RequestMapping(value = "/invoice/{id}", method = RequestMethod.DELETE)
	// public @ResponseBody
	// Message deleteInvoice(@PathVariable String id, Model model,
	// HttpServletRequest request) {
	// logger.info("Deleting Id:" + id);
	// if (id != null) {
	// return service.delete(Integer.valueOf(id));
	// } else {
	// return null;
	// }
	// }
}

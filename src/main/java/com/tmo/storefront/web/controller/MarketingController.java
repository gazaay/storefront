package com.tmo.storefront.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.services.CartService;
import com.tmo.storefront.web.services.PricingService;

@Controller
public class MarketingController {
	Logger logger = Logger.getLogger(MarketingController.class);

	@Autowired
	// @Qualifier(Constant.Development)
	CartService service;

	@Autowired
	PricingService pservice;

	@RequestMapping(value = "/maillist", method = RequestMethod.POST)
	@ResponseBody Message joinMailingList(@RequestBody String email){
		Message msg = new Message();
		logger.info("Email subscriber: "+ email);
		msg.setDescription("Email address has been successfully saved.");
		return msg;
	}
	
}

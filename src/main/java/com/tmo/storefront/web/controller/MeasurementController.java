package com.tmo.storefront.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.services.MeasurementService;

@Controller
public class MeasurementController extends BaseController {

	@Autowired
	MeasurementService service;

	Logger logger = Logger.getLogger(MeasurementController.class);

	@RequestMapping(value = "/measurement/user/{uid:.+}", method = RequestMethod.GET)
	public @ResponseBody
	Measurement getMeasurementByUID(@PathVariable String uid,
			@RequestParam String measure_type,
			@RequestParam(required = false) String measure_name, Model model,
			HttpServletRequest request) {
		logger.debug("Getting Measurement for uid:" + uid);
		return service.getMeasurement(uid, measure_type);
	}

	@RequestMapping(value = "/measurement/user/{uid:.+}", method = RequestMethod.POST)
	public @ResponseBody
	Message createMeasurementByUID(@PathVariable String uid,
			@RequestParam String measure_type,
			@RequestParam(required = false) String measure_name,
			@RequestBody Measurement measurement, Model model,
			HttpServletRequest request) {
		logger.debug("Getting Measurement for uid:" + uid + "with name:"
				+ measure_name);
		if (measure_name == null) {
			measure_name = Constant.DefaultMeasureName;
		}
		service.saveMeasurementWithType(uid, measurement, measure_type,
				measure_name);
		return new Message();
	}

	@RequestMapping(value = "/measurement/user/{uid:.+}", method = RequestMethod.PUT)
	public @ResponseBody
	String updateMeasurementByUID(@PathVariable String uid,
			@RequestParam String measure_type,
			@RequestParam(required = false) String measure_name,
			@RequestBody Measurement measurement, Model model,
			HttpServletRequest request) {
		logger.debug("Getting Measurement for uid:" + uid);
		if (measure_name == null) {
			measure_name = Constant.DefaultMeasureName;
		}
		service.saveMeasurementWithType(uid, measurement, measure_type,
				measure_name);
		return "";
	}

	@RequestMapping(value = "/measurement/check/user/{uid:.+}", method = RequestMethod.GET)
	public @ResponseBody
	Boolean checkMeasurementProfileExists(@PathVariable String uid,
			@RequestParam String measurement, Model model,
			HttpServletRequest request) {
		logger.debug("Getting Measurement for uid:" + uid);
		return (service.getMeasurement(uid, measurement) != null);
	}
}

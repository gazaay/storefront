package com.tmo.storefront.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.services.NewsLetterServices;

@Controller
public class NewsLetterController {
	Logger logger = Logger.getLogger(NewsLetterController.class);
	@Autowired
	private NewsLetterServices newsletterService;

	@RequestMapping(value = "/newsletter/send", method = RequestMethod.POST)
	@ResponseBody
	public Message sendNewsLetters(@RequestParam String template,
			@RequestParam(required = false) String promotion_code,
			@RequestParam(required = false) String title,
			@RequestBody String content, @RequestParam List<String> usernames,
			Model model, HttpServletRequest request) {
		Map<String, String> attribute_map = new HashMap<String, String>();
		attribute_map.put("promotion_code", promotion_code);
		return newsletterService.sendNewsLetter(content, template, attribute_map, usernames, title);
	}
}

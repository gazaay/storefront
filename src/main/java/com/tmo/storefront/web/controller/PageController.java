package com.tmo.storefront.web.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.HashedMap;
import org.apache.derby.jdbc.ReferenceableDataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Member;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.Stock;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.CartService;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.PricingService;
import com.tmo.storefront.web.services.ReferenceDataService;
import com.tmo.storefront.web.services.ShirtServices;
import com.tmo.storefront.web.services.UserService;

@Controller
public class PageController extends BaseController {

	Logger logger = Logger.getLogger(PageController.class);

	@Autowired
	private CartService cartService;
	@Autowired
	private ShirtServices shirtService;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private ReferenceDataService refService;

	// / Header

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showHome(Model model, HttpServletRequest request) {
		List<Shirt> shirts = shirtService.getShirtsByReferenceKey("TMO");
		model.addAttribute("shirts", shirts);
		return "home";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String showHome2(Model model, HttpServletRequest request) {
		List<Shirt> shirts = shirtService.getShirtsByReferenceKey("TMO");
		model.addAttribute("shirts", shirts);
		return "home";
	}

	@RequestMapping(value = "/m/home", method = RequestMethod.GET)
	public String showHomeM(Model model, HttpServletRequest request) {
		List<Shirt> shirts = shirtService.getShirtsByReferenceKey("TMO");
		model.addAttribute("shirts", shirts);
		return "mobile/home";
	}

	@RequestMapping(value = "/s/home", method = RequestMethod.GET)
	public String showHomeStore(Model model, HttpServletRequest request) {
		List<Shirt> shirts = shirtService.getShirtsByReferenceKey("TMO");
		model.addAttribute("shirts", shirts);
		return "store/home";
	}

	@RequestMapping(value = "/custom", method = RequestMethod.GET)
	public String showCustomShirt(Model model, HttpServletRequest request) {
		Object tiefix_value = refService.getReferenceValue(Constant.TieFix);
		Object fonts_value = refService.getReferenceValue(Constant.Embroidery);
		Object handkerchief_value = refService
				.getReferenceValue(Constant.Handkerchief);
		model.addAttribute(Constant.TieFix + "_value", tiefix_value);
		model.addAttribute(Constant.Handkerchief + "_value", handkerchief_value);
		model.addAttribute(Constant.Embroidery + "_value", fonts_value);
		return "custom_shirt";
	}

	@RequestMapping(value = "/gallery", method = RequestMethod.GET)
	public String showGallery(Model model, HttpServletRequest request) {
		return "gallery";
	}

	@RequestMapping(value = "/business_to_business", method = RequestMethod.GET)
	public String showB2B(Model model, HttpServletRequest request) {
		return "b2b";
	}

	@RequestMapping(value = "/contact_us", method = RequestMethod.GET)
	public String showContact(Model model, HttpServletRequest request) {
		return "contact";
	}

	// / In Page

	@RequestMapping(value = "/readytowear", method = RequestMethod.GET)
	public String showReadyToWear(Model model, HttpServletRequest request) {
		return "home";
	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String showLogin(Model model, HttpServletRequest request) {
		return "cart";
	}

	@RequestMapping(value = "/cartview", method = RequestMethod.GET)
	public String viewCart(Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		model.addAttribute("cart", cartService.getCart(username));

		return "cart";
	}

	@RequestMapping(value = "/cartsummary", method = RequestMethod.GET)
	public String viewCartSummary(Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		model.addAttribute("cart", cartService.getCart(username));
		Measurement measurement = new Measurement();
		Util util = new Util();
		Object shipping_days = refService
				.getReferenceValue(Constant.ShippingDays);

		model.addAttribute("shipping_date",
				util.addDays(Integer.parseInt((String) shipping_days)));
		model.addAttribute("measurement", measurement);
		model.addAttribute("paypal_account",
				(String) refService.getReferenceValue(Constant.PaypalAccount));
		model.addAttribute("currency",
				(String) refService.getReferenceValue(Constant.Currency));
		model.addAttribute("returnUrl", "www.tailormeonline.com");
		model.addAttribute("addressInfo",
				invoiceService.getAddressFromLastInvoice(username));
		List<String> countries = refService.getCountries();
		model.addAttribute("countries", countries);
		model.addAttribute("util", util);

		return "cartsummary";
	}

	// / Footer
	@RequestMapping(value = "/terms_condition", method = RequestMethod.GET)
	public String showTerms(Model model, HttpServletRequest request) {
		return "terms";
	}

	@RequestMapping(value = "/delivery_shipping", method = RequestMethod.GET)
	public String showDelivery(Model model, HttpServletRequest request) {
		return "shipping";
	}

	@RequestMapping(value = "/gifts", method = RequestMethod.GET)
	public String showGifts(Model model, HttpServletRequest request) {
		return "gifts";
	}

	@RequestMapping(value = "/faq", method = RequestMethod.GET)
	public String showFAQ(Model model, HttpServletRequest request) {
		return "faq";
	}

	@RequestMapping(value = "/sitemap", method = RequestMethod.GET)
	public String showSiteMap(Model model, HttpServletRequest request) {
		return "sitemap";
	}

	@RequestMapping(value = "/about_us", method = RequestMethod.GET)
	public String showAboutUs(Model model, HttpServletRequest request) {
		return "about_us";
	}

	@RequestMapping(value = "/newsletter", method = RequestMethod.GET)
	public String showNewsLetter(Model model, HttpServletRequest request) {
		return "newsletter";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegister(Model model, HttpServletRequest request) {
		model.addAttribute("user", new Users());
		return "register";
	}

	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	public String showLoginPage(Model model, HttpServletRequest request) {
		return "loginPage";
	}

	@RequestMapping(value = "/logoutSuccess", method = RequestMethod.GET)
	public String showLogoutPage(Model model, HttpServletRequest request) {
		model.addAttribute("cookie_clean", "true");

		return showHome(model, request);
	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String showLoginFailed(Model model, HttpServletRequest request) {
		model.addAttribute("cookie_clean", "true");
		return "loginfailed";
	}

	@RequestMapping(value = "/forget_password", method = RequestMethod.GET)
	public String showForgetPassword(Model model, HttpServletRequest request) {
		return "forget_password";
	}

	@RequestMapping(value = "/readyList", method = RequestMethod.GET)
	public String showReadyList(Model model, HttpServletRequest request) {
		List<Shirt> shirts = shirtService.getShirtsByReferenceKey("TMO");
		model.addAttribute("shirts", shirts);
		return "readylist";
	}
	
	@RequestMapping(value = "/readyitem", method = RequestMethod.GET)
	public String showReadyItem(@RequestParam String key,  Model model, HttpServletRequest request) {
		Shirt shirt = shirtService.getShirtByReferenceKey(key);
		model.addAttribute("shirt", shirt);
		return "readyitem";
	}
	@RequestMapping(value = "/channel", method = RequestMethod.GET)
	public String showChannel( Model model, HttpServletRequest request) {
		return "channel";
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String showError(Model model, HttpServletRequest request) {
		return "error";
	}

}
package com.tmo.storefront.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.UserService;

@Controller
public class ProfileController extends BaseController {

	Logger logger = Logger.getLogger(ProfileController.class);

	@Autowired
	private UserService userService;

//	@RequestMapping(value = "/users", method = RequestMethod.POST)
//	public @ResponseBody
//	Message createNewUser(@RequestBody Users users) {
//		userService.createOrUpdate(users);
//		return new Message();
//	}
//
//	@RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
//	public @ResponseBody
//	Users getUser(@PathVariable String username) {
//		return userService.getUsers(username);
//
//	}
//
//	@RequestMapping(value = "/users/{username}", method = RequestMethod.PUT)
//	public @ResponseBody
//	Message updateUser(@PathVariable String username, @RequestBody Users users) {
//		userService.createOrUpdate(username, users);
//		return new Message();
//	}
//
//	@RequestMapping(value = "/users", method = RequestMethod.GET)
//	public @ResponseBody
//	List<Users> getAllUsers() {
//		return userService.getUsers();
//	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String viewProfilePage(Model model, HttpServletRequest request){
		String username = super.getPrincipleUsername();
		model.addAttribute("user", userService.getUsers(username));
		
		return "profile";
	}
	@RequestMapping(value = "/adminprofile", method = RequestMethod.GET)
	public String viewAdminProfilePage(@RequestParam String username, Model model, HttpServletRequest request){
		model.addAttribute("user", userService.getUsers(username));
		
		return "profile";
	}
}

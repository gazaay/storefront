package com.tmo.storefront.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tmo.storefront.domain.Promotion;

import com.tmo.storefront.domain.GridResult;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.services.PromotionService;

@Controller
public class PromotionController extends BaseController {
	@Autowired
	PromotionService service;

	@RequestMapping(value = "/promotions", method = RequestMethod.GET)
	public @ResponseBody
	GridResult<Promotion> getPromotion(Model model, HttpServletRequest request) {
		List<Promotion> list = service.getPromotions();
		GridResult<Promotion> results = new GridResult<Promotion>();
		results.setRows(list);
		results.setPage("1");
		results.setRecords(String.valueOf(list.size()));
		results.setTotal(Integer.valueOf(1));

		return results;
	}

	@RequestMapping(value = "/promotion/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Promotion getPromotionDetail(@PathVariable String id, Model model,
			HttpServletRequest request) {
		return service.getPromotion(id);
	}

	@RequestMapping(value = "/promotion/{id}", method = RequestMethod.PUT)
	public @ResponseBody
	Message updatePromotion(@PathVariable String id,
			@RequestBody Promotion promotion, Model model,
			HttpServletRequest request) {
		return service.saveOrUpdate(id, promotion);
	}

	@RequestMapping(value = "/promotion", method = RequestMethod.POST)
	public @ResponseBody
	Message createNewPromotion(@RequestBody Promotion promotion, Model model,
			HttpServletRequest request) {
		if (promotion.getId() < 0) {
			promotion.setId(null);
		}
		return service.saveOrUpdate(promotion);

	}

	@RequestMapping(value = "/promotion/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	Message deletePromotion(@PathVariable String id, Model model,
			HttpServletRequest request) {
		logger.info("Deleting Id:" + id);
		if (id != null) {
			return service.delete(Integer.valueOf(id));
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/promotion/checkvalid", method = RequestMethod.GET)
	public @ResponseBody
	Boolean checkIfPromotionValid(@RequestParam String promotion_code,
			Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		Message message = service.validatePromotionCode(username,
				promotion_code, new Message());
		return message.isValid();
	}

}

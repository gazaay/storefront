package com.tmo.storefront.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.GridResult;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Config;
import com.tmo.storefront.web.services.ReferenceDataService;

@Controller
public class ReferenceDataController extends BaseController{
	@Autowired
	ReferenceDataService refService;
	
	@RequestMapping(value = "/configs", method = RequestMethod.GET)
	public @ResponseBody
	GridResult<Config> getConfig(Model model, HttpServletRequest request) {
		List<Config> list = refService.getConfigs();
		GridResult<Config> results = new GridResult<Config>();
		results.setRows(list);
		results.setPage("1");
		results.setRecords(String.valueOf(list.size()));
		results.setTotal(Integer.valueOf(1));

		return results;
	}

	@RequestMapping(value = "/config/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Config getConfigDetail(@PathVariable String id, Model model,
			HttpServletRequest request) {
		return refService.getConfig(id);
	}

	@RequestMapping(value = "/config/{id}", method = RequestMethod.PUT)
	public @ResponseBody
	Message updateConfig(@PathVariable String id,
			@RequestBody Config config, Model model,
			HttpServletRequest request) {
		return refService.saveOrUpdate(id, config);
	}

	@RequestMapping(value = "/config", method = RequestMethod.POST)
	public @ResponseBody
	Message createNewConfig(@RequestBody Config config, Model model,
			HttpServletRequest request) {
		if (config.getId() < 0) {
			config.setId(null);
		}
		return refService.saveOrUpdate(config);

	}

	@RequestMapping(value = "/config/{id}", method = RequestMethod.DELETE)
	public @ResponseBody
	Message deleteConfig(@PathVariable String id, Model model,
			HttpServletRequest request) {
		logger.info("Deleting Id:" + id);
		if (id != null) {
			return refService.delete(Integer.valueOf(id));
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/config/checkvalid", method = RequestMethod.GET)
	public @ResponseBody
	Boolean checkIfConfigValid(@RequestParam String config_code,
			Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		Message message = refService.validateConfigCode(username,
				config_code, new Message());
		return message.isValid();
	}

	
	
}

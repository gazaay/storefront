package com.tmo.storefront.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.InventoryService;
import com.tmo.storefront.web.services.RegistrationService;
import com.tmo.storefront.web.services.SecurityService;



@Controller
public class RegistrationController extends BaseController {
	
	Logger logger = Logger.getLogger(RegistrationController.class);
	
	@Autowired
	private RegistrationService service;
	
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	RegistrationService registration_service;
	
//	@RequestMapping(value="/signup2", method = RequestMethod.POST)
//	public @ResponseBody Message signUpNewUser(@ModelAttribute("userProfile") UserProfile profile) throws Exception {
//		logger.info("User Profile Name" + profile.getFirstname());
//		logger.info("User Profile country" + profile.getCountry());
//		logger.info("User Profile email" + profile.getEmail());
//		logger.info("User Profile lastname" + profile.getLastname());
//		logger.info("User Profile phone" + profile.getPhone());
//		logger.info("User Profile salutation" + profile.getSalutation());
//		Message message = new Message();
//		message.setDescription("Testing message only");
//		message.setValid(true);
//		return message;
//	}
	@RequestMapping(value="/validate/{input}/{value}", method = RequestMethod.GET)
	public @ResponseBody boolean validateOnRegistration(@PathVariable String input, @PathVariable String value) throws Exception {
		String type = "";
		
		return true;
	}
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	@ResponseBody
	public Message registerUser(@RequestBody Users user, Model model,
			HttpServletRequest request) throws Exception {
		// TODO: configure it by database later
		Message msg = new Message();
		try {
			securityService.saveUser(user);
			super.doAutoLogin(user.getUsername(), user.getPassword(), request);
			logger.info("User Signup and login automatically for first time. user:" + user.getUsername());
			registration_service.sendWelcomeRegistrationEmail(user.getUsername());
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			msg.setDescription("Error occur while saving user. Please contact us for more information.");
			return msg;
		}
		msg.setDescription("Registration successfully done !");
		return msg;
	}

	
}

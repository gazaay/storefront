package com.tmo.storefront.web.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.tmo.storefront.domain.Authorities;
import com.tmo.storefront.domain.Member;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.PasswordService;
import com.tmo.storefront.web.services.RegistrationService;
import com.tmo.storefront.web.services.SecurityService;
import com.tmo.storefront.web.services.UserService;

@Controller
@SessionAttributes("login")
public class SecurityController extends BaseController {
	Logger logger = Logger.getLogger(SecurityController.class);

	
	@Autowired
	private SecurityService service;
	
	
	@Autowired
	private PasswordService password_service;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticate(@ModelAttribute("login") Member member,
			Model model, HttpServletRequest request) {
		logger.info("Name: " + member.getName());
		logger.info("Username: " + member.getUsername());
		member.setName(member.getUsername());
		SecurityService service = new SecurityService();
		member = service.authenticate(member);
		return "home";
	}

	@RequestMapping(value = "/authenticateCheck", method = RequestMethod.POST)
	@ResponseBody
	public boolean authenticateCheck(@ModelAttribute("login") Member member,
			Model model, HttpServletRequest request) {
		logger.info("Name" + member.getName());
		logger.info("Username" + member.getUsername());
		member.setName(member.getUsername());
		SecurityService service = new SecurityService();
		member = service.authenticate(member);
		return member.isAuthenticate();
	}

	


	@RequestMapping(value = "/password_retrieve", method = RequestMethod.POST)
	@ResponseBody
	public Message retrievePassword(Model model, HttpServletRequest request,
			@RequestBody String username) throws Exception {
		Message msg = new Message();
		try {
			password_service.SendPasswordRecoveryEmail(username);
			msg.setValid(true);
			msg.setDescription("Password has been sent to registered email address.");
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			msg.setValid(false);
			msg.setDescription("Error occur while retrieving user password. Please contact us for more information.");
			return msg;
		}
		return msg;
	}

}

package com.tmo.storefront.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.Message;

@Controller
public class SessionController {
	@RequestMapping(value = "/session", method = RequestMethod.POST)
	@ResponseBody
	public Message showAdmin(@RequestParam String attribute,
			@RequestBody String session_value, Model model,
			HttpServletRequest request) {
		request.getSession().setAttribute(attribute, session_value);
		return new Message();
	}
}

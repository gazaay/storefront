package com.tmo.storefront.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmo.storefront.domain.GridResult;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Promotion;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.domain.ShirtUIModel;
import com.tmo.storefront.domain.ShirtZoom;
import com.tmo.storefront.domain.UIShirtSelection;
import com.tmo.storefront.web.services.PricingService;
import com.tmo.storefront.web.services.ShirtServices;
import com.tmo.storefront.web.services.WishService;

@Controller
@RequestMapping(value = "shirt")
public class ShirtController extends BaseController {

	Logger logger = Logger.getLogger(ShirtController.class);

	@Autowired
	private ShirtServices services;

	@Autowired
	private PricingService price_services;

	@Autowired
	private WishService wishServices;

	@RequestMapping(value = "shirtAttributes", method = RequestMethod.GET)
	public @ResponseBody
	List<ShirtAttribute> getShirtAttributes() throws Exception {
		String type = "";

		return services.getAttributes(type);
	}

	@RequestMapping(value = "options", method = RequestMethod.GET)
	public @ResponseBody
	Shirt getShirtAttributes(@RequestParam String options,
			@RequestParam String menu, Model model, HttpServletRequest request)
			throws Exception {
		HashMap<String, Object> result = new ObjectMapper().readValue(options,
				HashMap.class);
		String username = super.getPrincipleUsername();
		logger.info("User: " + username + " has selected menu " + menu
				+ " with options: " + options);
		Shirt shirt = services.getShirtAttributes(result, menu);
		shirt.setPattern_json(options);
		// Save shirt to wish list
		wishServices.saveWishListWithShirt(shirt, username, request
				.getSession().getId());
		return shirt;
	}

	@RequestMapping(value = "price", method = RequestMethod.GET)
	public @ResponseBody
	Float getShirtPrice(@RequestParam String options) throws Exception {
		HashMap<String, Integer> result = new ObjectMapper().readValue(options,
				HashMap.class);
		logger.info("Shirt Price with options: " + options);
		return price_services.getShirtPrice(result);
	}

	@RequestMapping(value = "reference/{key}", method = RequestMethod.GET)
	public @ResponseBody
	Shirt getShirtByKey(@PathVariable("key") String key) throws Exception {
		return services.getShirtByReferenceKey(key);
	}

	@RequestMapping(value = "reference/{key}", method = RequestMethod.PUT)
	public @ResponseBody
	String getShirtByKey(@PathVariable("key") String key,
			@RequestParam String pattern_json) throws Exception {
		services.updateShirtByReferenceKey(key, pattern_json);
		return "success";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody
	Message createShirt(@RequestBody Shirt shirt) throws Exception {
		services.createShirtWithSelectedAttributes(shirt);
		return new Message();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody
	Message updateShirt(@PathVariable Integer id, @RequestBody Shirt shirt)
			throws Exception {
		shirt.setShirtId(id);
		services.saveOrUpdate(shirt);
		return new Message();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody
	GridResult<Shirt> getShirts(Model model, HttpServletRequest request) {
		List<Shirt> list = services.getShirts();
		GridResult<Shirt> results = new GridResult<Shirt>();
		results.setRows(list);
		results.setPage("1");
		results.setRecords(String.valueOf(list.size()));
		results.setTotal(Integer.valueOf(1));

		return results;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Shirt getShirt(@PathVariable Integer id, Model model,
			HttpServletRequest request) {
		Shirt result = services.getShirtById(id);
		return result;
	}

	@RequestMapping(value = "/readytowear", method = RequestMethod.GET)
	public @ResponseBody
	GridResult<Shirt> getReadytoWearShirts(
			Model model, HttpServletRequest request) {
		List<Shirt> shirts = services.getShirtsByReferenceKey("TMO");
		GridResult<Shirt> results = new GridResult<Shirt>();
		results.setRows(shirts);
		results.setPage("1");
		results.setRecords(String.valueOf(shirts.size()));
		results.setTotal(Integer.valueOf(1));
		return results;
	}
	
	
	// @RequestMapping(value = "attributes", method = RequestMethod.GET)
	// public @ResponseBody
	// Shirt getShirtAttributes(@RequestParam String style) throws Exception {
	// logger.info("*********** Getting attributes search with sytle:" + style);
	// Shirt shirt = services.getShirtAttributes(style);
	// List<ShirtAttribute> list = shirt.getAttributes();
	// String name = list.get(0).getName();
	// return shirt;
	// }

}

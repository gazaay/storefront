package com.tmo.storefront.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tmo.storefront.domain.Stock;
 
@Controller
public class StockController extends BaseRestfulController{
 
	@RequestMapping(value="{name}", method = RequestMethod.GET)
	public @ResponseBody Stock getShopInJSON(@PathVariable String name) {
 
		Stock stock = new Stock();
		stock.setName(name);
		stock.setAttributes(new String[]{"mkyong1", "mkyong2"});
 
		return stock;
 
	}
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
    public String showPing(Model model, HttpServletRequest request) {
        return "test";
    }
 
}
package com.tmo.storefront.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tmo.storefront.web.admin.AdminController;

@Controller
public class TemplateController {
	Logger logger = Logger.getLogger(TemplateController.class);

	@RequestMapping(value = "/template{template_pages}", method = RequestMethod.GET)
	public String showTemplate(@PathVariable String template_pages,
			@RequestParam(required = false) String promotion_code, Model model,
			HttpServletRequest request) {
		Object email_content = request.getSession().getAttribute(
				"email_content");
		logger.debug("Email Content Retrieve: " + email_content);
		model.addAttribute("content", email_content);
		model.addAttribute("promotion_code", promotion_code);
		return "template/" + template_pages;
	}

}

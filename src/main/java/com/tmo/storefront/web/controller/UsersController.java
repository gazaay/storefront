package com.tmo.storefront.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.mvc.AbstractController;

import com.tmo.storefront.domain.Member;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;
import com.tmo.storefront.web.services.SecurityService;
import com.tmo.storefront.web.services.UserService;

@Controller
public class UsersController extends AbstractController {

	Logger logger = Logger.getLogger(UsersController.class);
	@Autowired
	private UserService service;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public  @ResponseBody List<Users> getUsers() {

		return service.getUsers();
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public  @ResponseBody Message createUser(@RequestBody Users users) {

		service.createOrUpdate(users);
		return new Message();
	}

	@RequestMapping(value = "/users/{username:.+}", method = RequestMethod.PUT)
	public  @ResponseBody Message updateUser(@PathVariable String username,
			@RequestBody Users users) {

		service.updateProfile(username, users);
		return new Message();
	}

	@RequestMapping(value = "/users/{username:.+}", method = RequestMethod.GET)
	public  @ResponseBody Users getUsers(@PathVariable String username) {

		return service.getUsers(username);
	}

	@RequestMapping(value = "/check_email", method = RequestMethod.GET)
	public  @ResponseBody boolean checkEmailDuplication(@RequestParam String email) {
		Users user = service.getUsers(email);
		return user == null;
	}
}

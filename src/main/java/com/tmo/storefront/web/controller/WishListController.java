package com.tmo.storefront.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tmo.storefront.domain.WishCustomerView;
import com.tmo.storefront.web.services.CartService;
import com.tmo.storefront.web.services.MeasurementService;
import com.tmo.storefront.web.services.PricingService;
import com.tmo.storefront.web.services.PromotionService;
import com.tmo.storefront.web.services.ReferenceDataService;
import com.tmo.storefront.web.services.ShirtServices;
import com.tmo.storefront.web.services.WishService;

@Controller
public class WishListController extends BaseController {
	@Autowired
	// @Qualifier(Constant.Development)
	CartService service;

	@Autowired
	MeasurementService measureService;
	@Autowired
	ReferenceDataService refService;

	@Autowired
	WishService wishService;

	@Autowired
	PromotionService promotionService;

	@Autowired
	PricingService pservice;
	@Autowired
	ShirtServices shirtservice;

	@RequestMapping(value = "/wishes", method = RequestMethod.GET)
	public void getWishes(String username) {

	}

	@RequestMapping(value = "/wishes/user/{uid:.+}", method = RequestMethod.GET)
	public List<WishCustomerView> getWishesCustomerView(@PathVariable String username) {
		return wishService.getWishesCustomerView(username);
	}

	@RequestMapping(value = "/wish_view", method = RequestMethod.GET)
	public void displayWishes(String username) {

	}

	@RequestMapping(value = "/wish", method = RequestMethod.POST)
	public void createWish(String username) {

	}

	@RequestMapping(value = "/wish/{id}", method = RequestMethod.PUT)
	public void updateWish(String username) {

	}

	@RequestMapping(value = "/wish/{id}", method = RequestMethod.DELETE)
	public void deleteWish(String username) {

	}

}

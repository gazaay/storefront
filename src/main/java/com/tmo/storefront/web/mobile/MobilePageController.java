package com.tmo.storefront.web.mobile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.web.controller.BaseController;
import com.tmo.storefront.web.services.UserService;

@Controller
public class MobilePageController extends MobileController {

	Logger logger = Logger.getLogger(MobilePageController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/mhome", method = RequestMethod.GET)
	public String showHome(Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		if (username != null) {
			model.addAttribute("user", userService.getUsers(username));
		}
		model.addAttribute("users", userService.getUsers());

		return "store/home";
	}
	@RequestMapping(value = "/m{page}", method = RequestMethod.GET)
	public String showPages(@PathVariable String page,  Model model, HttpServletRequest request) {
		String username = super.getPrincipleUsername();
		if (username != null) {
			model.addAttribute("user", userService.getUsers(username));
		}
		model.addAttribute("users", userService.getUsers());
		
		return "store/" + page;
	}

	@RequestMapping(value = "/mready", method = RequestMethod.GET)
	public String showReadyToWear(Model model, HttpServletRequest request) {

		return "store/readytowear_flip";
	}

}

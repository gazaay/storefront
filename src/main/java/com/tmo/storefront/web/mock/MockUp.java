//package com.tmo.storefront.web.mock;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.tmo.storefront.domain.Shirt;
//import com.tmo.storefront.domain.ShirtAttribute;
//import com.tmo.storefront.domain.ShirtUIModel;
//
//public class MockUp {
//
//	public static ShirtUIModel CreateShirtUIModel() {
//
//		ShirtUIModel model = new ShirtUIModel();
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		model.setShirtAttributes(shirtAttributes);
//		return model;
//	}
//
//	public static Shirt CreateFitSlimShirt() {
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		Shirt shirt = new Shirt();
//		shirt.setStyle("Slim");
//		shirt.setAttributes(shirtAttributes);
//		return shirt;
//	}
//	
//	public static Shirt CreateRegularShirt() {
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		Shirt shirt = new Shirt();
//		shirt.setStyle("Regular");
//		shirt.setAttributes(shirtAttributes);
//		return shirt;
//	}
//	
//	public static Shirt CreateShortShirt() {
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		Shirt shirt = new Shirt();
//		shirt.setStyle("Short");
//		shirt.setAttributes(shirtAttributes);
//		return shirt;
//	}
//	
//	public static Shirt CreateLongShirt() {
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		Shirt shirt = new Shirt();
//		shirt.setStyle("Long");
//		shirt.setAttributes(shirtAttributes);
//		return shirt;
//	}
//	
//	public static Shirt CreateShortWithCuffShirt() {
//		List<ShirtAttribute> shirtAttributes = getMockAttributes();
//		Shirt shirt = new Shirt();
//		shirt.setStyle("Short with normal cuff");
//		shirt.setAttributes(shirtAttributes);
//		return shirt;
//	}
//
//	private static List<ShirtAttribute> getMockAttributes() {
//		List<ShirtAttribute> shirtAttributes = new ArrayList<ShirtAttribute>();
//		shirtAttributes
//				.add(new ShirtAttribute("0",
//						"resources//img//Shirt//Shirt-Wide-Lyr01-Torso-NormClassicBot.0001.png"));
//		shirtAttributes.add(new ShirtAttribute("1", "resources//img//Shirt//"));
//		shirtAttributes
//				.add(new ShirtAttribute("2",
//						"resources//img//Shirt//Shirt-Wide-Lyr03-NormPlacket.0001.png"));
//		shirtAttributes.add(new ShirtAttribute("3",
//				"resources//img//Shirt//Shirt-Wide-Lyr04-LeftPocket.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("4",
//						"resources//img//Shirt//Shirt-Wide-Lyr05-NormLongSleeve.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("5",
//						"resources//img//Shirt//Shirt-Wide-Lyr06-PlacketThread.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("6",
//						"resources//img//Shirt//Shirt-Wide-Lyr07-PlacketButton.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("7",
//						"resources//img//Shirt//Shirt-Wide-Lyr08-Collar-NormClassic-Inner.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("8",
//						"resources//img//Shirt//Shirt-Wide-Lyr09-Collar-NormClassic-Outer.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("9",
//						"resources//img//Shirt//Shirt-Wide-Lyr10-Collar-NormClassic-Thread.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("10",
//						"resources//img//Shirt//Shirt-Wide-Lyr11-Collar-NormClassic-Button.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("11",
//						"resources//img//Shirt//Shirt-Wide-Lyr12-Cuff-StrSingButton-Outer.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("12",
//						"resources//img//Shirt//Shirt-Wide-Lyr12-Cuff-StrSingButton-Outer.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("13",
//						"resources//img//Shirt//Shirt-Wide-Lyr14-Cuff-StrSingButton-Thread.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("14",
//						"resources//img//Shirt//Shirt-Wide-Lyr15-Cuff-StrSingButton-Button.0001.png"));
//		shirtAttributes
//				.add(new ShirtAttribute("15",
//						"resources//img//Shirt//Shirt-Wide-Lyr16-Cuff-NormRight.0001.png"));
//		return shirtAttributes;
//	}
//	
//	
//}

package com.tmo.storefront.web.services;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import com.tmo.storefront.domain.Address;
import com.tmo.storefront.domain.AddressInfo;

@Component
public class AddressHelper {

	public AddressInfo parseAddress(String address_json) throws JsonParseException, JsonMappingException, IOException {
		//2. Convert JSON to Java object
		// {"mailing_name":"CHoco","mailing_phone":"62335535","mailing_address1":"ABC","mailing_address2":"c","mailing_city":"Fo Tan",
		// "mailing_state":"NT","mailing_postcode":"0","mailing_country":"HongKong"} 
		//
		ObjectMapper mapper = new ObjectMapper();
		AddressInfo result = mapper.readValue(address_json, AddressInfo.class);
		
		return result;
		
	}

}

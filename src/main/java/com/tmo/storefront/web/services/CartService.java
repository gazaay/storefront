package com.tmo.storefront.web.services;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.CartInfo;
import com.tmo.storefront.domain.Invoice;

@Service
@Transactional
public interface CartService {

	Cart getCart(String username);

	void updateCart(String username, Cart cart);

	float getCartTotalCost(Cart cart) throws JsonParseException, JsonMappingException, IOException;
	Invoice createInvoice(Cart cart, String promotion, String address) throws JsonParseException, JsonMappingException, IOException;
	void removeItemByShirtId(Cart cart, Integer id);
	void addItemByShirtId(Cart cart, Integer id);

	void updateShipping(Cart cart, String method);

	void updatePromotionCode(Cart cart, String promotion_code, String username);

	CartInfo cloneCartInfo(Cart cart)throws JsonParseException, JsonMappingException, IOException;

	Cart updateItemComments(Cart result, Integer item_id, String comments);

	Cart updateItemRemarks(Cart result, Integer item_id, String remarks);

}

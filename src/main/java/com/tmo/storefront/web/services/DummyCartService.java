package com.tmo.storefront.web.services;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.tmo.storefront.dao.PromotionDao;
import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.CartInfo;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Item;
import com.tmo.storefront.domain.Promotion;
import com.tmo.storefront.common.Constant;

@Component
// @Qualifier(Constant.Development)
public class DummyCartService implements CartService {
	private Map<String, Cart> carts;
	Logger logger = Logger.getLogger(DummyCartService.class);
	@Autowired
	PromotionDao promoDao;
	@Autowired
	private ReferenceDataService refService;

	public DummyCartService() {
		carts = new HashMap<String, Cart>();
	}

	public Cart getCart(String username) {
		// TODO Auto-generated method stub
		if (username == null) {
			return null;
		}
		if (carts.containsKey(username)) {
			return carts.get(username);
		}
		Cart result = new Cart();
		result.setUsername(username);
		carts.put(username, result);
		return result;
	}

	public void updateCart(String username, Cart cart) {
		// TODO Auto-generated method stub
		if (carts.containsKey(username)) {
			carts.put(username, cart);
		} else {
			carts.put(username, cart);
		}
	}

	@Override
	public float getCartTotalCost(Cart cart) throws JsonParseException,
			JsonMappingException, IOException {
		// TODO Auto-generated method stub plus shipping cost
		float total = 0;
		for (Item item : cart.getItem()) {
			total += item.getPrice() * item.getQuantity();
		}
		return total;
	}

	public Invoice createInvoice(Cart cart, String promotion, String address)
			throws JsonParseException, JsonMappingException, IOException {
		Invoice result = new Invoice();
		result.setDue(new Date());
		result.setGSTRate(refService.getGST());
		result.setIssue(new Date());
		result.setItems(cart.getItem());
		String items_json = new ObjectMapper().writeValueAsString(cart
				.getItem());
		result.setItems_json(items_json);
		result.setShipping(cart.getShippingCost());
		result.setPromotion_code(promotion);
		result.setDiscount(cart.getDiscount());
		result.setTotal(cart.getTotalCost());
		result.setAddress_json(address);
		return result;
	}

	@Override
	public void removeItemByShirtId(Cart cart, Integer id) {
		int index = 0;
		for (Item cartItem : cart.getItem()) {
			if (cartItem.getShirt().getShirtId().equals(id)) {
				cart.removeItem(index);
			}
			index++;

		}
	}

	@Override
	public void addItemByShirtId(Cart cart, Integer id) {
		int index = 0;
		for (Item cartItem : cart.getItem()) {
			logger.info("searching for id: " + id + " with type:"
					+ id.getClass().toString() + " current Shirt id "
					+ cartItem.getShirt().getShirtId() + " with type "
					+ cartItem.getShirt().getShirtId().getClass().toString());

			if (cartItem.getShirt().getShirtId().equals(id)) {
				logger.info("found shirt with index " + index);
				cart.addItem(index);
			}
			index++;

		}
	}

	@Override
	public void updateShipping(Cart cart, String method) {
		cart.setShippingMethod(method);
	}

	@Override
	public void updatePromotionCode(Cart cart, String promotion_code,
			String username) {
		Promotion promo = promoDao.getPromotionByCode(promotion_code);
		if (promo == null) {
		} else {
			Boolean isPromoValid = (promo.getTarget().equals("*") || promo
					.getTarget().toLowerCase().equals(username))
					&& promo.getClaims() + cart.getTotalItems() <= promo
							.getLimits();
			if (isPromoValid) {
				cart.setDiscount(promo.getDiscount());
			}
		}
	}

	@Override
	public CartInfo cloneCartInfo(Cart cart) throws JsonParseException,
			JsonMappingException, IOException {
		CartInfo result = new CartInfo();
		result.setDiscount(cart.getDiscount());
		result.setShippingCost(cart.getShippingCost());
		result.setShippingMethod(cart.getShippingMethod());
		result.setTotalCost(cart.getTotalCost());
		result.setTotalCostWithoutShipping(cart.getTotalCostWithoutShipping());
		result.setTotalItems(cart.getTotalItems());
		result.setTotalShirtCost(cart.getTotalShirtCost());
		return result;
	}

	@Override
	public Cart updateItemComments(Cart result, Integer item_id, String comments) {
		Item item = result.getItem(item_id);
		if (item != null) {
			item.setCustomer_comments(comments);
		}
		return result;
	}

	@Override
	public Cart updateItemRemarks(Cart result, Integer item_id, String remarks) {
		Item item = result.getItem(item_id);
		if (item != null) {
			item.setOperator_remarks(remarks);
		}
		return result;
	}
}

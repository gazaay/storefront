package com.tmo.storefront.web.services;

import javax.mail.MessagingException;

public interface EmailService {

	public abstract void send(String username, String subject, String emailBody)
			throws MessagingException;

	/**
	 * Send a single email.
	 * 
	 * @throws MessagingException
	 */
	public abstract void sendEmail(String aFromEmailAddr, String aToEmailAddr,
			String aSubject, String aBody) throws MessagingException;

}
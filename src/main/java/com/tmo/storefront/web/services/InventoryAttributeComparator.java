package com.tmo.storefront.web.services;

import java.util.Comparator;

import com.tmo.storefront.domain.InventoryAttribute;

public class InventoryAttributeComparator implements
		Comparator<InventoryAttribute> {

	@Override
	public int compare(InventoryAttribute o1, InventoryAttribute o2) {
		String datarate1 = o1.getUri();
		String datarate2 = o2.getUri();
		return datarate1.compareTo(datarate2);
	}
}

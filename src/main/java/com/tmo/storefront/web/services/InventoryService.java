package com.tmo.storefront.web.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.CategoryDao;
import com.tmo.storefront.dao.InventoryDao;
import com.tmo.storefront.dao.ShirtDao;
import com.tmo.storefront.domain.Alter;
import com.tmo.storefront.domain.Category;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.InvoiceAttributes;

@Service
@Transactional
public class InventoryService {
	@Autowired
	private InventoryDao invDao;
	@Autowired
	private CategoryDao catDao;

	Logger logger = Logger.getLogger(InventoryService.class);

	public List<InventoryAttribute> getAttributes(String componentName,
			int maxnumber, int pagenumber) {
		// TODO Auto-generated method stub
		return invDao.getAttributesByComponentName(componentName, maxnumber,
				pagenumber);
	}

	public Category getCategoryById(Integer id) {
		// TODO Auto-generated method stub
		return invDao.getCategoryById(id);
	}

	public List<Category> getCategories(int maxnumber) {
		return invDao.getCategories(maxnumber);
	}

	public Alter getAlterBy(Integer id) {
		return invDao.getAlterBy(id);
	}

	public void updateInventoryFromAdmin(Integer Id,
			InventoryAttribute inventory) {
		// TODO Why do we need to set value one by one?
		InventoryAttribute inv = invDao.getInventoryAttributeById(Id);
		inv.setColour(inventory.getColour());
		inv.setComposition(inventory.getComposition());
		inv.setFabric_name(inventory.getFabric_name());
		inv.setNew_code(inventory.getNew_code());
		inv.setPrice(inventory.getPrice());
		inv.setPrice_collar_contrast(inventory.getPrice_collar_contrast());
		inv.setPrice_cuff_contrast(inventory.getPrice_cuff_contrast());
		inv.setPrice_placket(inventory.getPrice_placket());
		inv.setTreatment(inventory.getTreatment());
		inv.setType(inventory.getType());
		inv.setWeaving(inventory.getWeaving());
		inv.setActive(inventory.isActive());
		inv.setYarn(inventory.getYarn());
		invDao.saveOrUpdateInventoryAttribute(inv);
	}

	public InventoryAttribute getInventoryAttributeById(Integer id) {
		// TODO Auto-generated method stub

		return invDao.getInventoryAttributeById(id);
	}

	public List<InventoryAttribute> getInventoryList(String pattern_json)
			throws JsonParseException, JsonMappingException, IOException {
		try {
			logger.debug("Getting Attributes from Pattern String:"
					+ pattern_json);
			if (pattern_json == null) {
				return new ArrayList<InventoryAttribute>();
			}
			HashMap<String, Integer> selection = new ObjectMapper().readValue(
					pattern_json, HashMap.class);
			List<InventoryAttribute> result = new ArrayList<InventoryAttribute>();
			for (Entry<String, Integer> pattern : selection.entrySet()) {
				logger.debug("found adding:" + pattern);
				logger.debug("found adding:" + pattern.getValue());
				logger.debug("found adding:" + pattern.getKey());
				try {
					InventoryAttribute invatt = invDao
							.getAttributesById(pattern.getValue());
					if (invatt != null) {
						result.add(new InvoiceAttributes(pattern.getKey(),
								invatt));
					}
				} catch (Exception ex) {
					logger.error(ex);
				}
			}

			return result;
		} catch (Exception ex) {
			logger.error(ex);
			logger.error(ex.getStackTrace());
			return new ArrayList<InventoryAttribute>();
		}

	}

	public Map<String, InventoryAttribute> getInventoryKeyValue(
			String pattern_json) {
		try {
			logger.debug("Getting Attributes from Pattern String:"
					+ pattern_json);
			if (pattern_json == null) {
				return new HashMap<String, InventoryAttribute>();
			}
			HashMap<String, Object> selection = new ObjectMapper().readValue(
					pattern_json, HashMap.class);
			HashMap<String, InventoryAttribute> result = new HashMap<String, InventoryAttribute>();
			for (Entry<String, Object> pattern : selection.entrySet()) {
				logger.debug("found adding:" + pattern);
				logger.debug("found adding:" + pattern.getValue());
				logger.debug("found adding:" + pattern.getKey());
				try {
					if (pattern.getValue() instanceof Integer) {
						InventoryAttribute invatt = invDao
								.getAttributesById((Integer) pattern.getValue());
						if (invatt != null) {
							result.put(pattern.getKey(), new InvoiceAttributes(
									pattern.getKey(), invatt));
						}
					} else {
						InventoryAttribute tematt = new InventoryAttribute();
						tematt.setFabric_name((String) pattern.getValue());
						result.put(pattern.getKey(), new InvoiceAttributes(
								pattern.getKey(), tematt));
					}
				} catch (Exception ex) {
					logger.error(ex);
				}
			}
			return result;
		} catch (Exception ex) {
			logger.error(ex);
			logger.error(ex.getStackTrace());
			return new HashMap<String, InventoryAttribute>();
		}

	}

	public List<Category> getInvoiceCategories() {
		List<Category> result = new ArrayList<Category>();
		result.add(new Category("fabrics"));
		result.add(new Category("shirt_sleeve"));
		result.add(new Category("collar"));
		result.add(new Category("cuff"));
		result.add(new Category("front"));
		result.add(new Category("back"));
		result.add(new Category("buttons"));
		result.add(new Category("embroidery"));
		result.add(new Category("additional"));
		return result;

	}

	public List<InventoryAttribute> getInventoryListByCategory(
			Map<String, InventoryAttribute> inventory_hash, Category category)
			throws JsonParseException, JsonMappingException, IOException {
		Map<String, List<String>> categoryInvoiceGrouping = new HashMap<String, List<String>>();
		List<String> fabrics_list = new ArrayList<String>();
		List<String> sleeve_list = new ArrayList<String>();
		List<String> collar_list = new ArrayList<String>();
		List<String> cuff_list = new ArrayList<String>();
		List<String> front_list = new ArrayList<String>();
		List<String> back_list = new ArrayList<String>();
		List<String> buttons_list = new ArrayList<String>();
		List<String> embroidery_list = new ArrayList<String>();
		List<String> additional_list = new ArrayList<String>();

		fabrics_list.add("fabrics");
		sleeve_list.add("shirt_type");
		sleeve_list.add("shirt_sleeve");
		collar_list.add("collar");
		collar_list.add("collar_inner");
		collar_list.add("collar_outer");
		cuff_list.add("cuff");
		cuff_list.add("cuff_outer");
		cuff_list.add("cuff_inner");
		cuff_list.add("cuff_short_outer");
		cuff_list.add("cuff_short_inner");
		cuff_list.add("cuff_style");
		cuff_list.add("placket");
		front_list.add("fastening");
		front_list.add("bottom_cuts");
		front_list.add("pocket");
		back_list.add("back");
		back_list.add("yoke");
		buttons_list.add("buttons");
		buttons_list.add("button_thread");
		embroidery_list.add("cuff_personalize_text");
		embroidery_list.add("cuff_personalize_font");
		embroidery_list.add("cuff_personalize_thread");
		additional_list.add("tie_fix");
		additional_list.add("handkerchief");

		List<InventoryAttribute> listOfInventory = new ArrayList<InventoryAttribute>();
		categoryInvoiceGrouping.put("fabrics", fabrics_list);
		categoryInvoiceGrouping.put("shirt_sleeve", sleeve_list);
		categoryInvoiceGrouping.put("collar", collar_list);
		categoryInvoiceGrouping.put("cuff", cuff_list);
		categoryInvoiceGrouping.put("front", front_list);
		categoryInvoiceGrouping.put("back", back_list);
		categoryInvoiceGrouping.put("buttons", buttons_list);
		categoryInvoiceGrouping.put("embroidery", embroidery_list);
		categoryInvoiceGrouping.put("additional", additional_list);

		for (String catString : categoryInvoiceGrouping.get(category.getName())) {
			listOfInventory.add(inventory_hash.get(catString));
		}

		return listOfInventory;
	}

	public void createInventory(InventoryAttribute inventory) {
		Category storeCategory = catDao.getCategoryById(30);
		List<Category> list = new ArrayList<Category>();
		List<InventoryAttribute> inventoryAttributes = storeCategory.getInventoryAttributes();
		list.add(storeCategory);
		if (inventoryAttributes== null || inventoryAttributes.size() <=0) {
			inventoryAttributes = new ArrayList<InventoryAttribute>();
		}
		inventoryAttributes.add(inventory);
		storeCategory.setInventoryAttributes(inventoryAttributes);
		invDao.saveOrUpdateInventoryAttribute(inventory);
	}

}

package com.tmo.storefront.web.services;

import java.io.IOException;
import java.io.StringWriter;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.ConfigDao;
import com.tmo.storefront.dao.InvoiceDao;
import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.domain.Address;
import com.tmo.storefront.domain.AddressInfo;
import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Item;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.Users;

@Service
public class InvoiceService {
	private Logger logger = Logger.getLogger(InvoiceService.class);
	@Autowired
	MeasurementService measureService;
	@Autowired
	ShirtServices shirtService;

	@Autowired
	private InvoiceDao invoiceDao;

	@Autowired
	private AddressHelper addressHelper;

	@Autowired
	@Qualifier(Constant.Production)
	private EmailService emailService;

	@Autowired
	private InvoiceTemplateService templateService;
	@Autowired
	private SecurityDao securityDao;

	@Autowired
	private ConfigDao configDao;

	@Autowired
	private OperatorTemplateService operatorTemplateService;

	public Message issueInvoice(String invoice_number, String username,
			Message message) throws MessagingException,
			JsonGenerationException, JsonMappingException, IOException {
		try {
			Users user = securityDao.getUserByUsername(username);
			sendCustomerInvoice(invoice_number, user);
			sendOperatorInvoice(invoice_number, user);
		} catch (Exception ex) {
			logger.error("Error Send Email: " + ex);
			message.setDescription("Error sending invoice to customer. Please contact us for resolution.");
			message.setValid(true);
		}
		return message;
	}

	private void sendOperatorInvoice(String invoice_number, Users user)
			throws JsonParseException, JsonMappingException, IOException {
		Invoice invoice = invoiceDao.getInvoiceByNumber(invoice_number);
		String opsemail = configDao.getValueStringByKey(Constant.OPsEmail);
		invoice.loadAttributes();
		// Create Operator Invoice from template
		String operatorInvoiceTemplate = operatorTemplateService
				.generateInvoiceTemplate(invoice, user);
		logger.info("Sending invoice to operator with email: " + opsemail);
		logger.info("Invoice number" + invoice.getNumber());
		logger.info(operatorInvoiceTemplate);
		// Send Invoice to customer
		try {
			emailService.send(opsemail, "An order #" + invoice.getNumber()
					+ "  status (" + invoice.getStatus() + ") from User ("
					+ user.getUsername()
					+ ") has been changed from TailorMeOnline",
					operatorInvoiceTemplate);
		} catch (Exception ex) {
			logger.error("" + ex.getMessage());
		}
	}

	private void sendCustomerInvoice(String invoice_number, Users user) {
		// Get Invoice from Database
		// invoice.setAddress_json(address);
		// invoice.setMeasurement_json(measureService.convertToJSON(username,
		// measurement));
		// Create Invoice from template
		Invoice invoice = invoiceDao.getInvoiceByNumber(invoice_number);
		String invoiceTemplate = templateService.generateInvoiceTemplate(
				invoice, user);
		logger.info("Sending invoice to " + user.getUsername());
		logger.info("Invoice number" + invoice.getNumber());
		logger.info(invoiceTemplate);
		// Send Invoice to customer
		try {
			emailService.send(user.getUsername(),
					"Thank you for purchase from Tailor Me Online Invoice:"
							+ invoice.getNumber(), invoiceTemplate);
		} catch (Exception ex) {
			logger.error("" + ex.getMessage());
		}
	}

	public Invoice saveInvoice(Invoice invoice) {
		return invoiceDao.saveInvoice(invoice);
	}

	public Invoice getInvoiceByNumber(String invoice_number) {
		Invoice invoice = invoiceDao.getInvoiceByNumber(invoice_number);
		return invoice;
	}

	public Message issueOperatorEmail(String invoice_number, String username,
			Message message) {
		try {
			Users user = securityDao.getUserByUsername(username);
			sendOperatorInvoice(invoice_number, user);
		} catch (Exception ex) {
			message.setDescription("Error sending operator mail. Please contact us for resolution.");
			logger.error("Error Send Email: " + ex);
			message.setValid(true);
		}
		return message;
	}

	public List<Invoice> getInvoicesInfo() {
		List<Invoice> invoices = invoiceDao.getInvoicesInfo();
		return invoices;
	}

	public List<Invoice> getInvoices() {
		List<Invoice> invoices = invoiceDao.getInvoices();
		return invoices;
	}

	public Message saveOrUpdate(String id, Invoice invoice) {
		invoice.setId(Integer.valueOf(id));
		invoiceDao.saveInvoice(invoice);
		return new Message();

	}

	public Message saveOrUpdate(Invoice invoice) {
		invoiceDao.saveInvoice(invoice);
		return new Message();

	}

	public Message delete(Integer valueOf) {
		// TODO Auto-generated method stub
		return null;
	}

	public Invoice getInvoice(String id) {
		return invoiceDao.getInvoiceById(Integer.valueOf(id));
	}

	public void updateItemRemarks(Invoice invoice, Integer item_id,
			String remarks) {
		try {
			Item item_select = null;
			List<Item> item_list = invoice.getItems();
			for (Item item : item_list) {
				if (item.getId().equals(item_id)) {
					item_select = item;
				}
			}
			if (item_select != null) {
				item_select.setOperator_remarks(remarks);
			}
			invoice.setItems(item_list);
		} catch (JsonParseException e) {
			logger.error(e);
		} catch (JsonMappingException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		invoiceDao.saveInvoice(invoice);
	}

	public void batchUpdateInvoiceItems(Integer shirtId)
			throws JsonParseException, JsonMappingException, IOException {
		// List<Invoice> invoices = invoiceDao.getInvoices();
		Invoice invoice = invoiceDao.getInvoiceByNumber(shirtId.toString());
		// for (Invoice invoice : invoices) {
		List<Item> items = invoice.getItems();
		for (Item item : items) {
			Shirt shirt = item.getShirt();
			logger.debug("Updating Shirt Details from :"
					+ shirt.getPattern_json());
			List<String> extras = shirt.getExtras();
			for (String extra : extras) {
				if (extra == null || extra.isEmpty()) {
					continue;
				}
				String[] extraList = extra.split("\\s");
				String key = extra.replace(extraList[extraList.length - 1], "");
				String value = extraList[extraList.length - 1];
				if (key.contains("Plack")) {
					key = "placket";
				}
				if (key.contains("Stiff")) {
					key = "stiff";
				}
				if (key.contains("Cuff Style")) {
					key = "cuff_style";
				}
				if (extra.contains("Cuff Personalize Text")) {
					key = "cuff_personalize_text";
					value = extra.replace("Cuff Personalize Text", "");
				}
				if (extra.contains("Cuff Personalize Fonts")) {
					key = "cuff_personalize_font";
					value = extra.replace("Cuff Personalize Fonts", "");
				}
				if (key.contains("Shirt")) {
					key = "shirt";
				}
				if (key.contains("tieFix")) {
					key = "tie_fix";
				}
				if (key.contains("handkerchief")) {
					key = "handkerchief";
				}
				logger.debug("Processing Extra :" + extra);
				shirtService.addAttributeToShirtPatternJSON(shirt, key, value);
			}
			logger.debug("Shirt Details has been changed to :"
					+ shirt.getPattern_json());
		}
		invoice.setItems(items);
		invoiceDao.saveInvoice(invoice);
		// }
	}

	public AddressInfo getAddressFromLastInvoice(String username) {
		Invoice invoice = invoiceDao.getLastInvoice(username);
		AddressInfo result = new AddressInfo();
		try {
			if (invoice != null) {
				result = addressHelper.parseAddress(invoice.getAddress_json());
			}
		} catch (JsonParseException e) {
			logger.error("Username: "+ username, e);
		} catch (JsonMappingException e) {
			logger.error("Username: "+ username, e);
		} catch (IOException e) {
			logger.error("Username: "+ username, e);
		} catch (Exception e) {
			logger.error("Username: "+ username, e);
		}
		return result;
	}
}

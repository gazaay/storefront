package com.tmo.storefront.web.services;

import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Users;

@Service
public class InvoiceTemplateService {
	private Logger logger = Logger.getLogger(InvoiceTemplateService.class);
	@Autowired
	private ReferenceDataService refService;

	public String generateInvoiceTemplate(Invoice invoice, Users user) {
		/*
		 * first, get and initialize an engine
		 */

		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class",
				ClasspathResourceLoader.class.getName());
		ve.init();

		/*
		 * organize our data
		 */

		/*
		 * add that list to a VelocityContext
		 */

		VelocityContext context = new VelocityContext();
		Object shipping_days = refService
		.getReferenceValue(Constant.ShippingDays);
		Util util = new Util();
		context.put("user", user);
		context.put("invoice", invoice);
		context.put("date", new DateTool());
		context.put("num", new NumberTool());
		context.put("util", util);
		context.put("shipping_date", util.addDays(Integer.parseInt((String) shipping_days)));
		/*
		 * get the Template
		 */

		Template t = ve.getTemplate("template/customer_invoice.vm");

		/*
		 * now render the template into a Writer, here a StringWriter
		 */

		StringWriter writer = new StringWriter();

		t.merge(context, writer);

		/*
		 * use the output in the body of your emails
		 */

		logger.info(writer.toString());
		return writer.toString();
	}
}

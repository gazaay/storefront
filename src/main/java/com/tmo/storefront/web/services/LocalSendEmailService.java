package com.tmo.storefront.web.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;

@Service
@Qualifier(Constant.Development)
public class LocalSendEmailService implements EmailService {

	static Logger logger = Logger.getLogger(SendEmailService.class);

	/* (non-Javadoc)
	 * @see com.tmo.storefront.web.services.EmailService#send(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void send(String username, String subject, String emailBody)
			throws MessagingException {
		// TODO Auto-generated method stub
		sendEmail("TMO Administrator", username, subject, emailBody);
	}

	/* (non-Javadoc)
	 * @see com.tmo.storefront.web.services.EmailService#sendEmail(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void sendEmail(String aFromEmailAddr, String aToEmailAddr,
			String aSubject, String aBody) throws MessagingException {
		logger.info("Local Send email service with title:"+ aSubject +" From: " + aFromEmailAddr + " To :" +aToEmailAddr + " Body: "+ aBody);
	}

	/**
	 * Allows the config to be refreshed at runtime, instead of requiring a
	 * restart.
	 */
	public static void refreshConfig() {
		fMailServerConfig.clear();
		fetchConfig();
	}

	// PRIVATE //

	private static Properties fMailServerConfig = new Properties();

	static {
		fetchConfig();
	}

	/**
	 * Open a specific text file containing mail server parameters, and populate
	 * a corresponding Properties object.
	 */
	private static void fetchConfig() {
		InputStream input = null;
		try {
			// If possible, one should try to avoid hard-coding a path in this
			// manner; in a web application, one should place such a file in
			// WEB-INF, and access it using ServletContext.getResourceAsStream.
			// Another alternative is Class.getResourceAsStream.
			// This file contains the javax.mail config properties mentioned
			// above.
			// input = new FileInputStream("C:\\Temp\\MyMailServer.txt");
			// fMailServerConfig.load(input);
			// mail.host=mail.blah.com
			//
			// # Return address to appear on emails
			// # (Default value : username@host)
			// mail.from=webmaster@blah.net
			//
			// # Other possible items include:
			// # mail.user=
			// # mail.store.protocol=
			// # mail.transport.protocol=
			// # mail.smtp.host=
			// # mail.smtp.user=
			// # mail.debug=
			//
			fMailServerConfig.put("mail.host", "mail.tailormeonline.com");
			fMailServerConfig.put("mail.smtp.host", "mail.tailormeonline.com");
			fMailServerConfig.put("mail.from", "jira.gen@tailormeonline.com");
			fMailServerConfig.put("mail.smtp.user",
					"jira.gen@tailormeonline.com");
			fMailServerConfig.put("mail.smtp.auth", "true");
			fMailServerConfig.put("mail.smtp.password", "168168");
			logger.info("Setup completed!");
			// fMailServerConfig.put("mail.store.protocol", value);
			// fMailServerConfig.put("mail.transport.protocol", );
			// fMailServerConfig.put("mail.smtp.host", value);
			// fMailServerConfig.put("mail.smtp.user", value);
			// fMailServerConfig.put("mail.debug", "false");
		} finally {
			try {
				if (input != null)
					input.close();
			} catch (IOException ex) {
				logger.error("Cannot close mail server properties file.");
			}
		}
	}
}

package com.tmo.storefront.web.services;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

@Service
public class LocaleService {
	Logger logger = Logger.getLogger(LocaleService.class);
	@Autowired
	ReloadableResourceBundleMessageSource msgSrc;

	public String getValue(String key, String locale) {
		try {
			if (key == null) {
				logger.error("Input decode messages was Null!");
				return "";
			}
			String message = msgSrc.getMessage(key, null, new Locale(locale));
			logger.debug("Locale: "+ locale+" translating key " + key + " to ->" + message);
			return message;
		} catch (NoSuchMessageException ex) {
			logger.error(ex.getMessage());
			return "missing:" + key;
		}
	}
}

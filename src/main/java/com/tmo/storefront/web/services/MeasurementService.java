package com.tmo.storefront.web.services;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Constant.MeasurementTypes;
import com.tmo.storefront.dao.MeasurementDao;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.web.controller.MeasurementController;

@Service
public class MeasurementService {

	@Autowired
	MeasurementDao dao;

	Logger logger = Logger.getLogger(MeasurementService.class);

	public Measurement getMeasurement(String uid, String measure_type) {

		return dao.getMeasurement(uid, measure_type,
				Constant.DefaultMeasureName);
	}

	public void saveMeasurementWithType(String uid, Measurement measurement,
			String measure_type, String measureName) {
		logger.info("Measurement printer:");
		logger.info("Uid:" + uid);
		logger.info("Measure_type:" + measure_type);
		logger.info("measurement JSON:" + measurement.getJson());
		logger.info("measurement Chest :" + measurement.getOwn_chest());
		Measurement database_measurement = dao.getMeasurement(uid, "all",
				measureName);
		if (database_measurement == null) {
			Measurement newMeasure = createNewMeasure(uid, measurement,
					measure_type);
			newMeasure.setMeasure_name(measureName);
			dao.createNew(newMeasure);
		} else {
			// addMeasurements(database_measurement, measurement, measure_type);
			// database_measurement.setUser_id(uid);
			// database_measurement.setMeasure_type(measure_type);
			// database_measurement.setSelection(measure_type);

			measurement.setId(database_measurement.getId());
			measurement.setUser_id(uid);
			dao.saveOrUpdate(measurement);
		}
	}

	private void addMeasurements(Measurement database_measurement,
			Measurement measurement, String measure_type) {
		MeasurementTypes types = MeasurementTypes.valueOf(measure_type
				.toUpperCase());
		switch (types) {
		case BESTMEASURE:
			database_measurement.setBest_chest(measurement.getBest_chest());
			database_measurement.setBest_collar(measurement.getBest_collar());
			database_measurement.setBest_cuff(measurement.getBest_cuff());
			database_measurement.setBest_length_to_waist(measurement
					.getBest_length_to_waist());
			database_measurement.setBest_long_lowlen(measurement
					.getBest_long_lowlen());
			database_measurement.setBest_long_uplen(measurement
					.getBest_long_uplen());
			database_measurement.setBest_shirt_length(measurement
					.getBest_shirt_length());
			database_measurement.setBest_short_lowlen(measurement
					.getBest_short_lowlen());
			database_measurement.setBest_short_opening(measurement
					.getBest_short_opening());
			database_measurement.setBest_short_uplen(measurement
					.getBest_short_uplen());
			database_measurement.setBest_shoulder_width(measurement
					.getBest_shoulder_width());
			database_measurement.setBest_sleeve_width(measurement
					.getBest_sleeve_width());
			database_measurement.setBest_waist(measurement.getBest_waist());
			database_measurement.setBest_width(measurement.getBest_width());
			break;
		case OWNMEASURE:
			database_measurement.setOwn_arm_len(measurement.getOwn_arm_len());
			database_measurement.setOwn_armpit(measurement.getOwn_armpit());
			database_measurement.setOwn_biceps(measurement.getOwn_biceps());
			database_measurement.setOwn_chest(measurement.getOwn_chest());
			database_measurement.setOwn_len_chest(measurement
					.getOwn_len_chest());
			database_measurement.setOwn_len_seat(measurement.getOwn_len_seat());
			database_measurement.setOwn_len_waist(measurement
					.getOwn_len_waist());
			database_measurement.setOwn_neck(measurement.getOwn_neck());
			database_measurement.setOwn_seat(measurement.getOwn_seat());
			database_measurement.setOwn_shirt_len(measurement
					.getOwn_shirt_len());
			database_measurement.setOwn_short_len(measurement
					.getOwn_short_len());
			database_measurement.setOwn_shoulder_width(measurement
					.getOwn_shoulder_width());
			database_measurement.setOwn_waist(measurement.getOwn_waist());
			database_measurement.setOwn_wrist(measurement.getOwn_wrist());
			break;
		case STANDARDMEASURE:
			database_measurement.setStandard_size(measurement
					.getStandard_size());
			break;
		}
	}

	private Measurement createNewMeasure(String uid, Measurement measurement,
			String measure_type) {
		measurement.setUser_id(uid);
		measurement.setMeasure_type(measure_type);
		return measurement;
	}

	public String convertToJSON(String uid, String measure_type)
			throws JsonGenerationException, JsonMappingException, IOException {
		Measurement measurement = getMeasurement(uid, measure_type);
		ObjectMapper mapper = new ObjectMapper();
		String result = mapper.writeValueAsString(measurement);
		logger.debug("JSON STRING For Measurement:" + result);
		return result;
	}

	public void saveMeasureSelection(String username, String selection) {
		// TODO Auto-generated method stub
		Measurement database_measurement = dao.getMeasurement(username, "all",
				Constant.DefaultMeasureName);
		database_measurement.setSelection(selection);
		dao.saveOrUpdate(database_measurement);
	}

	public String getAdminMeasurementJSON(String username, String invoice_id) {
		Measurement adminMeasurement = dao.getMeasurement(username, null,
				Constant.AdminMeasureName + "_" + invoice_id);
		String result = adminMeasurement.getJson();
		logger.info("Measurement JSON: " + result);
		return result;
	}

	public void saveAdminMeasureSelection(String username, String invoice_id,
			Measurement measurement) {
		Measurement database_measurement = dao.getMeasurement(username, "all",
				Constant.AdminMeasureName + "_" + invoice_id);
		if (database_measurement == null) {
			// TODO: get the last admin measurement
			// if all null do below.
			database_measurement = new Measurement();
			database_measurement.setUser_id(username);
			database_measurement.setMeasure_name(Constant.AdminMeasureName
					+ "_" + invoice_id);
		}
		addMeasurements(database_measurement, measurement, "BESTMEASURE");
		addMeasurements(database_measurement, measurement, "OWNMEASURE");
		addMeasurements(database_measurement, measurement, "STANDARDMEASURE");

		dao.saveOrUpdate(database_measurement);
	}
}

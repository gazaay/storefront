package com.tmo.storefront.web.services;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.AuditDao;
import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;

@Service
public class NewsLetterServices {
	private Logger logger = Logger.getLogger(NewsLetterServices.class);

	@Autowired
	private AuditDao auditDao;
	@Autowired
	private SecurityDao securityDao;
	@Autowired
	private NewsLetterTemplateService newsLetterTemplateService;
	@Autowired
	@Qualifier(Constant.Production)
	private EmailService emailService;

	public Message sendNewsLetter(String content, String template,
			Map<String, String> attributes, List<String> usernames, String title) {
		Users user;
		for (String username : usernames) {
			String action = "send newsletter";
			String action_content = content;
			String action_template = template;
			try {
				user = securityDao.getUserByUsername(username);
			} catch (Exception ex) {
				user = new Users();
				user.setUsername(username);
				user.setFirstname("Valued clients");
			}
			if (title == null || title.isEmpty()) {
				title = "Newsletter";
			}
			String body = newsLetterTemplateService.generateTemplate(template,
					user, content, attributes);
			try {
				logger.info("Sending Newsletter(" + title + ") to :" + username);
				emailService.send(username, title, body);
				auditDao.addTrails(username, action, action_content,
						action_template);
			} catch (Exception ex) {
				logger.error("" + ex.getMessage());
			}
		}
		return new Message();
	}

}

package com.tmo.storefront.web.services;

import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Users;

@Service
public class NewsLetterTemplateService {
	private Logger logger = Logger.getLogger(NewsLetterTemplateService.class);
	@Autowired
	private ReferenceDataService refService;

	public String generateTemplate(String template, Users user, String content, Map<String, String> attribute) {
		/*
		 * first, get and initialize an engine
		 */

		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class",
				ClasspathResourceLoader.class.getName());
		ve.init();

		/*
		 * organize our data
		 */

		/*
		 * add that list to a VelocityContext
		 */
		Util util = new Util();
		Object shipping_days = refService
				.getReferenceValue(Constant.ShippingDays);

			VelocityContext context = new VelocityContext();
		context.put("user", user);
		context.put("content", content);
		context.put("date", new DateTool());
		context.put("num", new NumberTool());
		context.put("util", new Util());
		context.put("shipping_date",
				util.addDays(Integer.parseInt((String) shipping_days)));
		for( Entry<String, String> entry :attribute.entrySet()){
			String value = entry.getValue() ;
			if (value != null &&  !value.isEmpty() ) {
				context.put(entry.getKey(), value);
			}
		}
		/*
		 * get the Template
		 */

		Template t = ve.getTemplate("template/"+template+".vm");

		/*
		 * now render the template into a Writer, here a StringWriter
		 */

		StringWriter writer = new StringWriter();

		t.merge(context, writer);

		/*
		 * use the output in the body of your emails
		 */

		logger.debug(writer.toString());
		return writer.toString();
	}

}

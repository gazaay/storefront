package com.tmo.storefront.web.services;

import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Util;
import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Users;

@Service
public class OperatorTemplateService {
	private Logger logger = Logger.getLogger(OperatorTemplateService.class);
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private LocaleService localeService;
	
	public String generateInvoiceTemplate(Invoice invoice, Users user) {
		/*
		 * first, get and initialize an engine
		 */

		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();

		/*
		 * organize our data
		 */

		/*
		 * add that list to a VelocityContext
		 */

		VelocityContext context = new VelocityContext();
		context.put("user", user);
		context.put("invoice", invoice);
		
		context.put("service", inventoryService);
		context.put("localeService", localeService);
		context.put("date", new DateTool());
		context.put("num", new NumberTool());
		context.put("util", new Util());
		/*
		 * get the Template
		 */

		Template t = ve
				.getTemplate("template/account_invoice.vm");

		/*
		 * now render the template into a Writer, here a StringWriter
		 */

		StringWriter writer = new StringWriter();

		t.merge(context, writer);

		/*
		 * use the output in the body of your emails
		 */

		logger.info(writer.toString());
		return writer.toString();
	}

}

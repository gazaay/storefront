package com.tmo.storefront.web.services;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.tmo.storefront.domain.OrderDetails;

@Service
public class OrderService {

	public HashMap<String, OrderDetails> orderlist;

	public OrderService() {
		orderlist = new HashMap<String, OrderDetails>();
	}

	public OrderDetails getOrder(String username) {
		OrderDetails order = orderlist.get(username);
		orderlist.put(username, null);
		return order;
	}

	public void setOrder(OrderDetails order, String username) {
		this.orderlist.put(username, order);
	}

}

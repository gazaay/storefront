package com.tmo.storefront.web.services;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Users;

@Service
@Transactional
public class PasswordService {
	Logger logger = Logger.getLogger(PasswordService.class);

	@Autowired
	private SecurityDao securityDao;

	@Autowired
	private SecurityService service;

	@Autowired
	private SendEmailService emailService;

	
	@Autowired
	private EmailTemplateService templateService;
	
		

	  @Transactional(readOnly = false, rollbackFor=Exception.class)
	  public Message SendPasswordRecoveryEmail(String username)
			throws Exception {
		Users user = securityDao.getUserByUsername(username);
		user.setPassword(SecurityService.generateRandomPassword());
		String emailBody = createPasswordRecoveryEmail(user);
		emailService.send(user.getUsername(), "Tailor Me Online Password Recovery", emailBody);
		logger.info(user.getUsername() + " password " + user.getPassword());
		securityDao.saveUser(user);
		return new Message();
	}

	private String createPasswordRecoveryEmail(Users user) {
		return templateService.generatePasswordRecovery(user);
	}
}

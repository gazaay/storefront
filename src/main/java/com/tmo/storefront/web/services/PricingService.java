package com.tmo.storefront.web.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.InventoryDao;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Shirt;

@Service
@Transactional
public class PricingService {

	@Autowired
	InventoryDao inv_dao;

	public float getShirtPrice(HashMap<String, Integer> option)
			throws JsonParseException, JsonMappingException, IOException {

		Integer fabric_Id = option.get(Constant.Fabrics);
		Integer tiefix_Id = option.get(Constant.TieFix);
		Integer handkerchief_Id = option.get(Constant.Handkerchief);
		Integer collari_Id = option.get(Constant.CollarInner);
		Integer collaro_Id = option.get(Constant.CollarOuter);
		Integer cuffi_Id = option.get(Constant.CuffInner);
		Integer cuffo_Id = option.get(Constant.CuffOuter);
		float result_price = 0;
		// tiefix and handkerchief price
		result_price += getPriceFromInventory(tiefix_Id);
		result_price += getPriceFromInventory(handkerchief_Id);

		// fabric price
		result_price += getPriceFromInventory(fabric_Id);
		if (!collari_Id.equals(fabric_Id)) {
			// collar in price if different fabric
			result_price += getCollarPriceFromInventory(collari_Id);
		}
		if (!collaro_Id.equals(fabric_Id)) {
			// collar in price if different fabric
			result_price += getCollarPriceFromInventory(collaro_Id);
		}
		if (!cuffi_Id.equals(fabric_Id)) {
			// cuff in price if different fabric
			result_price += getCuffPriceFromInventory(cuffi_Id);
		}
		if (!cuffo_Id.equals(fabric_Id)) {
			// cuff out price if different fabric
			result_price += getCuffPriceFromInventory(cuffo_Id);
		}
		return Float.parseFloat(String.format("%.2f", result_price));
	}

	// public float calculatePricingForCart(){}

	private float getPriceFromInventory(Integer id) {
		if (id != null  && id > 0) {
			InventoryAttribute inv = inv_dao.getAttributesById(id);
			return inv.getPrice();
		}
		return 0.0f;
	}
	
	private float getCollarPriceFromInventory(Integer id) {
		if (id != null  && id > 0) {
			InventoryAttribute inv = inv_dao.getAttributesById(id);
			return inv.getPrice_collar_contrast();
		}
		return 0.0f;
	}
	private float getCuffPriceFromInventory(Integer id) {
		if (id != null  && id > 0) {
			InventoryAttribute inv = inv_dao.getAttributesById(id);
			return inv.getPrice_cuff_contrast();
		}
		return 0.0f;
	}
	private float getPlacketPriceFromInventory(Integer id) {
		if (id != null  && id > 0) {
			InventoryAttribute inv = inv_dao.getAttributesById(id);
			return inv.getPrice_placket();
		}
		return 0.0f;
	}
	public PricingService getInstance() {
		// TODO Auto-generated method stub
		if (this == null) {
			return new PricingService();
		}
		return this;
	}
}

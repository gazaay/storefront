package com.tmo.storefront.web.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.dao.PromotionDao;
import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.Measurement;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.domain.Promotion;

@Service
public class PromotionService {
	Logger logger = Logger.getLogger(PromotionService.class);
	@Autowired
	PromotionDao dao;

	@Autowired
	CartService cartservice;
	public List<Promotion> getPromotions() {
		// TODO Auto-generated method stub
		return dao.getPromotions();
	}

	public Promotion getPromotion(String id) {
		// TODO Auto-generated method stub
		return dao.getPromotionById(id);
	}

	public Message saveOrUpdate(String id, Promotion promotion) {
		if (id == null) {
		} else {
			promotion.setId(Integer.parseInt(id));
		}
		dao.saveOrUpdate(promotion);
		return new Message();
	}

	public Message saveOrUpdate(Promotion promotion) {
		saveOrUpdate(null, promotion);
		return null;
	}

	public Message processCart(Cart cart, String promotion, String username,
			Message message) throws Exception {
		
		if (promotion.isEmpty()){
			return message;
		}
		try {
			Promotion promo = dao.getPromotionByCode(promotion);
			if (promo == null) {
				message.setDescription("The promotion code is not exisits: "
						+ promotion);
				message.setValid(false);
				return message;
			}
			Boolean isPromoValid = (promo.getTarget().equals("*") || promo
					.getTarget().toLowerCase().equals(username))
					&& promo.getClaims() + cart.getTotalItems() <= promo.getLimits();
			if (isPromoValid) {
				cart.setDiscount(promo.getDiscount());
				promo.setClaims(promo.getClaims() + cart.getTotalItems() );
				dao.saveOrUpdate(promo);
			} else {
				logger.info("Promotion Error: with Promotion Code:" + promo.getCode() + " with limit:" + promo.getLimits() + " with username:" + username);
				message.setDescription("The promotion code is invalid: "
						+ promotion);
				message.setValid(false);
				return message;
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw ex;
		}
		return message;
	}

	public Message validatePromotionCode(String username,
			String promotion_code, Message message) {
		try {
			Cart cart = cartservice.getCart(username);
			
			Promotion promo = dao.getPromotionByCode(promotion_code);
			if (!promo.getTarget().toLowerCase().equals(username.toLowerCase()) && !promo.getTarget().equals("*")) {
				message.setValid(false);
				message.setDescription("Promotion code is invalid.");
			} else if (promo.getClaims() >= promo.getLimits()) {
				message.setValid(false);
				message.setDescription("Promotion code is no longer avaiable.");
			} else if (cart.getTotalItems()+promo.getClaims()  > promo.getLimits()) {
				message.setValid(false);
				message.setDescription("This promotion code is only valid for "+ promo.getLimits() +" Shirt(s). Please reduce your cart and try again.");
			}

		} catch (Exception ex) {
			message.setValid(false);
			message.setDescription(ex.getMessage());
		}
		return message;
	}

	public Message delete(Promotion promotion) {
		dao.delete(promotion);
		return null;
	}

	public Message delete(Integer id) {
		logger.info("Deleting Id:" + id);
		dao.delete(id);
		return null;
	}

}

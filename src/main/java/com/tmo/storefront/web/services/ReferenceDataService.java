package com.tmo.storefront.web.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.ConfigDao;
import com.tmo.storefront.domain.Config;
import com.tmo.storefront.domain.Message;

@Service
@Transactional
public class ReferenceDataService {

	@Autowired
	private ConfigDao configDao;

	public float getGST() {
		String GSTRate = configDao.getValueStringByKey(Constant.DefaultGST);
		return Float.parseFloat(GSTRate);
	}

	public Object getReferenceValue(String referenceKey) {
		String referenceValue = configDao.getValueStringByKey(referenceKey);
		return referenceValue;
	}

	public List<Config> getConfigs() {
		// TODO Auto-generated method stub
		return configDao.getList();
	}

	public Config getConfig(String id) {
		return configDao.getConfigById(id);
	}

	public Message saveOrUpdate(String id, Config config) {
		configDao.saveConfig(config);
		return new Message();
	}

	public Message saveOrUpdate(Config config) {
		 configDao.saveConfig(config);
		 return new Message();
	}

	public Message delete(Integer valueOf) {
		// TODO Auto-generated method stub
		return null;
	}

	public Message validateConfigCode(String username, String config_code,
			Message message) {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getNextItemId() {
		// TODO Auto-generated method stub
		Integer itemId = Integer.parseInt((String) getReferenceValue(Constant.CartItemKey));
		Config config = configDao.getConfigByKey(Constant.CartItemKey);
		Integer result = itemId + 1;
		String config_value =  result.toString();
		config.setConfig_value(config_value);
		configDao.saveConfig(config);
		return result;
	}
	
	public Integer getUserId() {
		// TODO Auto-generated method stub
		Integer userId = Integer.parseInt((String) getReferenceValue(Constant.UserIdKey));
		Config config = configDao.getConfigByKey(Constant.UserIdKey);
		Integer result = userId + 1;
		String config_value =  result.toString();
		config.setConfig_value(config_value);
		configDao.saveConfig(config);
		return result;
	}

	public List<String> getCountries() {
		List<String> result = new ArrayList<String>();
		result.add("Singapore");
		result.add("Hong Kong");
		result.add("Australia");
		result.add("Japan");
		result.add("US");
		return result;
	}
}

package com.tmo.storefront.web.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.domain.Users;

@Service
public class RegistrationService {
	private Logger logger = Logger.getLogger(RegistrationService.class);
	
	@Autowired
	@Qualifier(Constant.Production)
	private EmailService emailService;
	
	@Autowired
	private WelcomeTemplateService welcomeTemplateService;
	
	@Autowired
	private SecurityDao securityDao;
	
	public boolean sendWelcomeRegistrationEmail(String username){
		Users user = securityDao.getUserByUsername(username);
		
		String welcomeEmailTemplate = welcomeTemplateService.generateWelcomeTemplate(user);
		try {
			emailService.send(user.getUsername(),
					"Welcome to TailorMeOnline", welcomeEmailTemplate);
		} catch (Exception ex) {
			logger.error("" + ex.getMessage());
			return false;
		}
		return true;
	}
}

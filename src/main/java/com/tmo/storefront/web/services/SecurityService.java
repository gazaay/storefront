package com.tmo.storefront.web.services;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.domain.Authorities;
import com.tmo.storefront.domain.Member;
import com.tmo.storefront.domain.Users;
@Service
@Transactional
public class SecurityService {

	private Map<String, String> authToken;
	@Autowired
	private SecurityDao securityDao;
	
	public SecurityService(){
		authToken = new HashMap<String, String>();
		authToken.put("eric", "eric");
		authToken.put("marcus", "marcus");
		authToken.put("ken", "ken");
		authToken.put("gary", "gary");
	}
	
	public void saveUser(Users user) throws Exception{
		user.setEnabled(true);
		Authorities auth = new Authorities();
		auth.setUsers(user);
		auth.setAuthority("ROLE_USER");
		auth.setUsername(user.getUsername());
		user.setAuthorities(auth);
		securityDao.saveUser(user);
	}
	
	public Member authenticate(Member member) {
		try {
			boolean isAuthenticated = authToken.get(member.getUsername()).equals(member.getPassword());
			member.setAuthenticate(isAuthenticated);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			member.setAuthenticate(false);
			return member;
		}
		
		return member;
	}

	public Member logout(Member member) {
		member.setAuthenticate(false);
		member.setUsername("");
		member.setPassword("");
		return member;
	}

	public static String generateRandomPassword() {

		SecureRandom random = new SecureRandom();
		return new BigInteger(50, random).toString(32);
	}

	public Users getUserByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}

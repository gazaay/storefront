package com.tmo.storefront.web.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tmo.storefront.common.Constant;

@Service
@Qualifier(Constant.Production)
public class SendEmailService implements EmailService {

	static Logger logger = Logger.getLogger(SendEmailService.class);

	public void send(String username, String subject, String emailBody)
			throws MessagingException {
		// TODO Auto-generated method stub
		sendEmail("TMO Administrator", username, subject, emailBody);
	}

	/**
	 * Send a single email.
	 * 
	 * @throws MessagingException
	 */
	public void sendEmail(String aFromEmailAddr, String aToEmailAddr,
			String aSubject, String aBody) throws MessagingException {
		// Here, no Authenticator argument is used (it is null).
		// Authenticators are used to prompt the user for user
		// name and password.
		String smtp_host = "mail.server.tailormeonline.com";
		String user = "donotreply@tailormeonline.com";
		String password = "tmo168168";
		Session session = Session.getDefaultInstance(fMailServerConfig, null);
		MimeMessage message = new MimeMessage(session);
		Transport tr = session.getTransport("smtp");
		try {
			// the "from" address may be set in code, or set in the
			// config file under "mail.from" ; here, the latter style is used
			// message.setFrom( new InternetAddress(aFromEmailAddr) );
			InternetAddress[] TheAddresses = InternetAddress.parse(
					aToEmailAddr, false);
			message.addRecipients(Message.RecipientType.TO, TheAddresses);
			message.setFrom(new InternetAddress(user, "TailorMeOnline"));
			message.setReplyTo(InternetAddress.parse(
					user, false));
			// .addRecipient(Message.RecipientType.TO, new InternetAddress(
			// aToEmailAddr));
			// message.setSubject(aSubject);
			// Setting the Subject and Content Type
			message.setSubject(aSubject, "utf-8");
			// message.setHeader("Content-Type", "text/html");
			message.setContent(aBody, "text/html; charset=utf-8");
			// message.setText(aBody,"UTF-8");

			tr.connect(smtp_host, user, password);
			message.saveChanges();
			tr.sendMessage(message, message.getAllRecipients());
		} catch (MessagingException ex) {
			logger.error("Cannot send email. " + ex + "to email: "+aToEmailAddr + " with Email Body:" + aBody);
			throw ex;
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot send email. " + e);
			e.printStackTrace();
		} finally {
			tr.close();
			logger.info("Send email process done with title:"+ aSubject +"  with " + aToEmailAddr);
		}
	}

	/**
	 * Allows the config to be refreshed at runtime, instead of requiring a
	 * restart.
	 */
	public static void refreshConfig() {
		fMailServerConfig.clear();
		fetchConfig();
	}

	// PRIVATE //

	private static Properties fMailServerConfig = new Properties();

	static {
		fetchConfig();
	}

	/**
	 * Open a specific text file containing mail server parameters, and populate
	 * a corresponding Properties object.
	 */
	private static void fetchConfig() {
		InputStream input = null;
		try {
			// If possible, one should try to avoid hard-coding a path in this
			// manner; in a web application, one should place such a file in
			// WEB-INF, and access it using ServletContext.getResourceAsStream.
			// Another alternative is Class.getResourceAsStream.
			// This file contains the javax.mail config properties mentioned
			// above.
			// input = new FileInputStream("C:\\Temp\\MyMailServer.txt");
			// fMailServerConfig.load(input);
			// mail.host=mail.blah.com
			//
			// # Return address to appear on emails
			// # (Default value : username@host)
			// mail.from=webmaster@blah.net
			//
			// # Other possible items include:
			// # mail.user=
			// # mail.store.protocol=
			// # mail.transport.protocol=
			// # mail.smtp.host=
			// # mail.smtp.user=
			// # mail.debug=
			//
			fMailServerConfig.put("mail.host", "mail.tailormeonline.com");
			fMailServerConfig.put("mail.smtp.host", "mail.tailormeonline.com");
			fMailServerConfig.put("mail.from", "donotreply@tailormeonline.com");
			fMailServerConfig.put("mail.smtp.user",
					"donotreply@tailormeonline.com");
			fMailServerConfig.put("mail.smtp.auth", "true");
			fMailServerConfig.put("mail.smtp.password", "tmo168168");
			logger.info("Setup completed!");
			// fMailServerConfig.put("mail.store.protocol", value);
			// fMailServerConfig.put("mail.transport.protocol", );
			// fMailServerConfig.put("mail.smtp.host", value);
			// fMailServerConfig.put("mail.smtp.user", value);
			// fMailServerConfig.put("mail.debug", "false");
		} finally {
			try {
				if (input != null)
					input.close();
			} catch (IOException ex) {
				logger.error("Cannot close mail server properties file.");
			}
		}
	}
}

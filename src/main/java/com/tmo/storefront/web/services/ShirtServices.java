package com.tmo.storefront.web.services;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.common.Constant;
import com.tmo.storefront.common.Util;
import com.tmo.storefront.dao.InventoryDao;
import com.tmo.storefront.dao.ShirtDao;
import com.tmo.storefront.domain.InventoryAttribute;
import com.tmo.storefront.domain.Promotion;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.domain.UIShirtSelection;
import com.tmo.storefront.domain.Zoom;

@Service
@Transactional
public class ShirtServices {
	@Autowired
	private ShirtDao dao;

	@Autowired
	private InventoryDao invdao;

	Logger logger = Logger.getLogger(ShirtServices.class);

	@Autowired
	private PricingService price_services;

	public List<ShirtAttribute> getAttributes(String type) throws Exception {
		// TODO Auto-generated method stub
		List<ShirtAttribute> list = new ArrayList<ShirtAttribute>();
		list.add(new ShirtAttribute("Layer0", "This is a sample"));
		dao.addAttribute("Layer 1", "abc");
		list.add(dao.getAttributeByName("Layer 1"));

		return list;
	}

	public void createShirtWithSelectedAttributes(Shirt shirt) {
		// TODO Auto-generated method stub
		dao.createShirt(shirt);

	}

	public void saveOrUpdate(Shirt shirt) {
		// TODO Auto-generated method stub
		dao.saveOrUpdate(shirt);

	}

	public Shirt getShirtAttributes(HashMap<String, Object> options,
			String menu_selection) {
		List<ShirtAttribute> attrs = getShirtAttributesFromOptions(options);

		Shirt result = new Shirt();
		result.setAttributes(attrs);

		HashMap<String, Integer>  integerOptions = Util.getIntegerOptions(options);
		
		List<Zoom> zooms = dao.getZoomsBy(integerOptions, menu_selection);
		logger.info("Rows return:" + zooms.size());
		result.setZooms(zooms);
		return result;
	}

	private List<ShirtAttribute> getShirtAttributesFromOptions(
			HashMap<String, Object> options) {
		
		HashMap<String, Integer>  integerOptions = Util.getIntegerOptions(options);
		if (dao.isOptionsContainsShortSleeves(integerOptions)) {
			logger.debug("It is a SHORT SLEEVE!!!!!!!!!!!!!!!");
			for (Entry<String, Integer> attr : integerOptions.entrySet()) {
				if (attr.getKey().matches("cuff")) {
					attr.setValue(1);
				}
			}
		}

		List<ShirtAttribute> attrs = dao.getAttributesBy(integerOptions);
		return attrs;
	}

	// public String renderShirtPng(List<ShirtAttribute> shirts) throws
	// IOException {
	// ShirtAttribute attr = shirts.get(0);
	// // load source images
	// String path =System.getProperty("user.dir") + "/src/main/webapp/" +
	// attr.getPath();
	// logger.info("root" + path);
	// BufferedImage image = ImageIO.read(new File(path));
	// BufferedImage overlay = ImageIO.read(new File(path));
	//
	// // create the new image, canvas size is the max. of both image sizes
	// int w = Math.max(image.getWidth(), overlay.getWidth());
	// int h = Math.max(image.getHeight(), overlay.getHeight());
	// BufferedImage combined = new BufferedImage(w, h,
	// BufferedImage.TYPE_INT_ARGB);
	//
	// // paint both images, preserving the alpha channels
	// Graphics g = combined.getGraphics();
	// g.drawImage(image, 0, 0, null);
	// g.drawImage(overlay, 0, 0, null);
	//
	// ImageOutputStream output = ImageIO.createImageOutputStream(combined);
	// String result = "";
	// // Save as new image
	// ImageIO.write(combined, "PNG", output);
	// output.writeUTF(result);
	// return result;
	// }

	public List<ShirtAttribute> getShirtAttributesFromJSON(String json) {
		try {
			HashMap<String, Object> options = new ObjectMapper().readValue(
					json, HashMap.class);
			List<ShirtAttribute> attrs = getShirtAttributesFromOptions(options);

			List<ShirtAttribute> results = new ArrayList<ShirtAttribute>();
			Map<String, ShirtAttribute> uniqueMap = new HashMap<String, ShirtAttribute>();
			for (ShirtAttribute attr : attrs) {
				uniqueMap.put(attr.getLayer(), attr);
			}
			for (ShirtAttribute item : uniqueMap.values()) {
				results.add(item);
			}
			Collections.sort(results, new ShirtAttributeComparator());
			return results;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Error when trying to parse the Shirt JSON:" + e);
		}
		return null;
	}

	public static class ShirtAttributeComparator implements
			Comparator<ShirtAttribute> {
		@Override
		public int compare(ShirtAttribute x, ShirtAttribute y) {
			// TODO: Handle null x or y values
			int startComparison = compare(Integer.parseInt(x.getLayer()),
					Integer.parseInt(y.getLayer()));
			return startComparison != 0 ? startComparison : compare(
					Integer.parseInt(x.getLayer()),
					Integer.parseInt(y.getLayer()));
		}

		// I don't know why this isn't in Long...
		private static int compare(Integer a, Integer b) {
			return a < b ? -1 : a > b ? 1 : 0;
		}
	}

	public Shirt getShirtAttributes(UIShirtSelection selections) {
		List<ShirtAttribute> attrs = dao.getAttributesBySelections(selections
				.getInventoryAttributesIds());
		Shirt result = new Shirt();
		result.setAttributes(attrs);
		return result;
	}

	public Shirt getShirtByReferenceKey(String key) {
		return dao.getShirtByReferenceKey(key);
	}

	public List<Shirt> getShirtsByReferenceKey(String key) {
		return dao.getShirtsByReferenceCriteria(key);
	}

	public void updateShirtByReferenceKey(String key, String pattern_json)
			throws JsonParseException, JsonMappingException, IOException {
		Shirt shirt = dao.getShirtByReferenceKey(key);
		if (shirt == null) {
			shirt = new Shirt();
			shirt.setReference_key(key);
		}
		try {
			if (pattern_json != null) {
				HashMap<String, Integer> result = new ObjectMapper().readValue(
						pattern_json, HashMap.class);
				logger.info("Shirt Price with options: " + pattern_json);
				Float price = price_services.getShirtPrice(result);
				shirt.setPrice(price);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		shirt.setPattern_json(pattern_json);
		dao.saveOrUpdate(shirt);
	}

	public String getFabricName(HashMap<String, Integer> option) {
		// TODO Auto-generated method stub
		Integer fabricKey = option.get(Constant.Fabrics);
		InventoryAttribute fabric = invdao.getAttributesById(fabricKey);
		return fabric.getFabric_name();
	}

	public List<Shirt> getShirts() {
		return dao.getShirts();
	}

	public void addAttributeToShirtPatternJSON(Shirt shirt, String key,
			String value) throws JsonParseException, JsonMappingException,
			IOException {
		String json = shirt.getPattern_json();
		HashMap<String, Object> options = new ObjectMapper().readValue(json,
				HashMap.class);
		Integer valueInt = 0;
		if (value.contains("true")) {
			valueInt = 301;
		}
		if (value.contains("Yes")) {
			valueInt = 301;
		}
		if (value.contains("No")) {
			valueInt = 301;
		}
		if (value.contains("false")) {
			valueInt = 302;
		}
		if (value.contains("Slim")) {
			valueInt = 246;
		}
		if (value.contains("Regular")) {
			valueInt = 230;
		}
		// if (value.contains("Beveled")){
		// valueInt = 301;
		// }
		// if (value.contains("Round")){
		// valueInt = 301;
		// }
		// if (value.contains("Straight")){
		// valueInt = 301;
		// }
		if (valueInt > 0) {
			options.put(key, valueInt);
		} else {
			options.put(key, value);
		}
		String result = new ObjectMapper().writeValueAsString(options);
		shirt.setPattern_json(result);
	}

	public Shirt getShirtById(Integer id) {
		return dao.getShirtById(id);
	}

}

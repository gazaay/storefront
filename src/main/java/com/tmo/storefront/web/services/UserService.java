package com.tmo.storefront.web.services;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmo.storefront.dao.SecurityDao;
import com.tmo.storefront.dao.UserDao;
import com.tmo.storefront.domain.Authorities;
import com.tmo.storefront.domain.Users;

@Service
@Transactional
public class UserService {
	@Autowired
	private UserDao dao;

	public List<Users> getUsers() {
		return dao.getUsers();
	}

	public Users getUsers(String username) {
		if (username.isEmpty()) {
			return null;
		}
		return dao.getUserByUsername(username);
	}

	public boolean createOrUpdate(String username, Users users) {
		users.setUsername(username);
		return dao.createOrUpdate(users);
	}
	
	public boolean updateProfile(String username, Users users) {
		Users resultUser = dao.getUserByUsername(username);
		resultUser.setCountry(users.getCountry());
		resultUser.setFirstname(users.getFirstname());
		resultUser.setLastname(users.getLastname());
		resultUser.setPhone(users.getPhone());
		return dao.createOrUpdate(resultUser);
	}

	public boolean createOrUpdate(Users users) {
		return dao.createOrUpdate(users);
	}
}

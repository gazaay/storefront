package com.tmo.storefront.web.services;

import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Service;

import com.tmo.storefront.domain.Users;

@Service
public class WelcomeTemplateService {
	private Logger logger = Logger.getLogger(WelcomeTemplateService.class);

	public String generateWelcomeTemplate(Users user) {
		/*
		 * first, get and initialize an engine
		 */

		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class",
				ClasspathResourceLoader.class.getName());
		ve.init();

		/*
		 * organize our data
		 */

		/*
		 * add that list to a VelocityContext
		 */

		VelocityContext context = new VelocityContext();
		context.put("user", user);
		
		Template t = ve.getTemplate("template/welcome_email.vm");

		/*
		 * now render the template into a Writer, here a StringWriter
		 */

		StringWriter writer = new StringWriter();

		t.merge(context, writer);

		/*
		 * use the output in the body of your emails
		 */

		logger.info(writer.toString());
		return writer.toString();
	}

}

package com.tmo.storefront.web.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.dao.WishDao;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.Wish;
import com.tmo.storefront.domain.WishCustomerView;

@Service
public class WishService {

	@Autowired
	WishDao wishDao;
	@Autowired
	ShirtServices shirtService;
	Logger logger = Logger.getLogger(WishService.class);

	public void saveWishListWithShirt(Shirt shirt, String username,
			String session_id) {
		Date currentDate = new Date();
		// Get the latest session wishes
		Wish wish = wishDao.getLatestWish(username, session_id);
		// if not create new wish
		if (wish == null) {
			logger.debug(" New Wish ");
			wish = new Wish();
		}
		// Check if Shirt Exists
		if (wish.getShirt() == null) {
			// create new Shirt
			logger.debug("Create New Shirt");
			shirt.setShirtId(null);
			wish.setCreated_Date(currentDate);
		} else {
			// update Shirt
			logger.debug("Update Shirt");
			shirt.setShirtId(wish.getId());
		}
		// Save Wishes
		wish.setShirt(shirt);
		shirt.setWish(wish);
		shirtService.saveOrUpdate(shirt);

		wish.setSession_Id(session_id);
		wish.setUsername(username);
		wish.setModified_Date(currentDate);
		wishDao.saveOrUpdate(wish);
	}

	public Wish getWishBy(String username, String session_id) {
		return wishDao.getWishBy(username, session_id);
	}

	public List<WishCustomerView> getWishesCustomerView(String username) {
		List<Wish> wishes = wishDao.getWishesBy(username, null);
		
		List<WishCustomerView> result = new ArrayList<WishCustomerView>();
		for(Wish wish : wishes){
			result.add(new WishCustomerView(wish));
		}
		return result;
	}

}

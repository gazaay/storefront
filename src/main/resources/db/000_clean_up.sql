CREATE SCHEMA IF NOT EXISTS tmo;
GRANT USAGE ON *.* TO 'tmo_app'@'tmo';
GRANT USAGE ON *.* TO 'tmo_app'@'%';
DROP USER 'tmo_app'@'tmo';
DROP USER 'tmo_app'@'%';
DROP SCHEMA  IF EXISTS tmo ;
CREATE SCHEMA tmo;


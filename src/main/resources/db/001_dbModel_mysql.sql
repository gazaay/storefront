CREATE TABLE tmo.Shirt (
       Id INT NOT NULL AUTO_INCREMENT
     , Title VARCHAR(255)
     , Invoice VARCHAR(50)
     , Thumb TEXT
     , Pattern_JSON VARCHAR(1000)
     , Reference_Key CHAR(10)
     , Code VARCHAR(50)
     , Price FLOAT(2)
     , Fit VARCHAR(55)
     , Sleeve VARCHAR(55)
     , Thumb_URL VARCHAR(255)
     , Main_Image_URL VARCHAR(255)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Image_storage (
       Id INT NOT NULL AUTO_INCREMENT
     , layer INT
     , URL VARCHAR(255)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Inventory_Attribute (
       Id INT NOT NULL AUTO_INCREMENT
     , URI VARCHAR(255)
     , URI_Selected VARCHAR(255)
     , URI_CloseUp VARCHAR(255)
     , Description VARCHAR(255)
     , New_Code VARCHAR(255)
     , Fabric_Name VARCHAR(255)
     , Price FLOAT(20)
     , Price_Collar_Contrast FLOAT(20)
     , Price_Cuff_Contrast FLOAT(20)
     , Price_Placket FLOAT(20)
     , Composition VARCHAR(255)
     , Yarn VARCHAR(255)
     , Colour VARCHAR(255)
     , Weaving VARCHAR(255)
     , Treatment VARCHAR(255)
     , Type VARCHAR(255)
     , Old_Fabric_Code VARCHAR(255)
     , Active BOOLEAN
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.AlterShirt (
       Id INT NOT NULL AUTO_INCREMENT
     , AlterShirt_Group_Name CHAR(10)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Toolbox_Layout (
       Id INT NOT NULL AUTO_INCREMENT
     , Name CHAR(50)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Zoom (
       Id INT NOT NULL AUTO_INCREMENT
     , Image_storage_Id INT
     , Description VARCHAR(255)
     , Layer INT
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.System_Config (
       Id INT NOT NULL AUTO_INCREMENT
     , Config_Name CHAR(10)
     , Config_Usage CHAR(10)
     , Config_Value CHAR(255)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Stock (
       Id INT NOT NULL AUTO_INCREMENT
     , Title VARCHAR(255)
     , PRIMARY KEY (Id)
);

CREATE TABLE tmo.Shirt_Attribute (
       Id INT NOT NULL AUTO_INCREMENT
     , Attribute_name VARCHAR(50)
     , Attribute_description VARCHAR(255)
     , Image_storage_Id INT
     , Layer INT
     , PRIMARY KEY (Id)
     , INDEX (Image_storage_Id)
     , CONSTRAINT FK_Stock_Attribute_1 FOREIGN KEY (Image_storage_Id)
                  REFERENCES tmo.Image_storage (Id)
);

CREATE TABLE tmo.Category (
       Id INT NOT NULL AUTO_INCREMENT
     , Name CHAR(50)
     , AlterShirt_Id INT
     , Toolbox_Layout_Id INT
     , Header VARCHAR(255)
     , PRIMARY KEY (Id)
     , INDEX (Toolbox_Layout_Id)
     , CONSTRAINT FK_Category_2 FOREIGN KEY (Toolbox_Layout_Id)
                  REFERENCES tmo.Toolbox_Layout (Id)
     , INDEX (AlterShirt_Id)
     , CONSTRAINT FK_Category_3 FOREIGN KEY (AlterShirt_Id)
                  REFERENCES tmo.AlterShirt (Id)
);

CREATE TABLE tmo.Shirt_Attribute_Inventory_Attribute (
       Shirt_Attribute_Id INT NOT NULL
     , Inventory_Attribute_Id INT NOT NULL
     , PRIMARY KEY (Shirt_Attribute_Id, Inventory_Attribute_Id)
     , INDEX (Shirt_Attribute_Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_2 FOREIGN KEY (Shirt_Attribute_Id)
                  REFERENCES tmo.Shirt_Attribute (Id)
     , INDEX (Inventory_Attribute_Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES tmo.Inventory_Attribute (Id)
);

CREATE TABLE tmo.Inventory_Attribute_Category (
       Category_Id INT
     , Inventory_Attribute_Id INT
     , INDEX (Category_Id)
     , CONSTRAINT FK_Inventory_Attribute_Category_1 FOREIGN KEY (Category_Id)
                  REFERENCES tmo.Category (Id)
     , INDEX (Inventory_Attribute_Id)
     , CONSTRAINT FK_Inventory_Attribute_Category_2 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES tmo.Inventory_Attribute (Id)
);

CREATE TABLE tmo.Zoom_Inventory_Attribute (
       Zoom_Id INT
     , Inventory_Attribute_Id INT
     , INDEX (Zoom_Id)
     , CONSTRAINT FK_Zoom_Inventory_Attribute_2 FOREIGN KEY (Zoom_Id)
                  REFERENCES tmo.Zoom (Id)
     , INDEX (Inventory_Attribute_Id)
     , CONSTRAINT FK_Zoom_Inventory_Attribute_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES tmo.Inventory_Attribute (Id)
);

CREATE TABLE tmo.Category_Zoom (
       Category_Id INT
     , Zoom_Id INT
     , INDEX (Category_Id)
     , CONSTRAINT FK_Category_Zoom_1 FOREIGN KEY (Category_Id)
                  REFERENCES tmo.Category (Id)
     , INDEX (Zoom_Id)
     , CONSTRAINT FK_Category_Zoom_2 FOREIGN KEY (Zoom_Id)
                  REFERENCES tmo.Zoom (Id)
);

CREATE TABLE tmo.Inventory_Attribute_Filter (
       Id INT NOT NULL
     , Red INT
     , Green INT
     , Blue INT
     , Pattern CHAR(50)
     , Inventory_Attribute_Id INT
     , PRIMARY KEY (Id)
     , INDEX (Inventory_Attribute_Id)
     , CONSTRAINT FK_InventoryAttributeFilter_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES tmo.Inventory_Attribute (Id)
);


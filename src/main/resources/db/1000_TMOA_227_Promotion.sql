CREATE TABLE tmo.Promotion (
       Id INT NOT NULL AUTO_INCREMENT
     , Promotion_Code VARCHAR(8)
     , Has_Distributed BOOLEAN
     , Distribution_To VARCHAR(50)
     , No_Of_Allowance INT
     , Expiry_Date DATETIME
     , PRIMARY KEY (Id)
);
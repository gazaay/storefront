use tmo;
CREATE TABLE tmo.Invoice (
       Id INTEGER
     , Number CHAR(20)
     , Due DATETIME
     , Shipping FLOAT
     , GSTRate FLOAT
     , Items_JSON LongText
     , Total FLOAT
     , Issue DATETIME
);

insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('web_master_email', 'This is used for the web user to contact our web admin with this email address.', 'garylamj@gmail.com');
insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('order_email', 'This is the receipient of the order', 'garylamj@gmail.com');

insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('SG_GST', 'This is the rate of GST', '0.07');
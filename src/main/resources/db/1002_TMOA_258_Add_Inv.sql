use tmo;
-- comments not sure why I need to drop all the constrains and re add constrains afterwards.
-- TMOA 258
insert into Category (Name) values ('tie_fix');
insert into Category (Name) values ('handkerchief');
Alter table System_Config MODIFY Config_Name CHAR(255);
Alter table System_Config MODIFY Config_Usage CHAR(255);


insert into Inventory_Attribute (  URI ,
URI_Selected , URI_CloseUp , Description , 
New_Code , Fabric_Name , Price , Price_Collar_Contrast ,
Price_Cuff_Contrast , Price_Placket , Composition , Yarn , 
Colour , Weaving , Treatment , Type , Old_Fabric_Code, 
Active ) values ('nil',
'nil', 'nil', 'tie_fix and handkerchief','nil','1', 8.0,0.0,0.0,0.0,
'nil','nil','nil','nil','nil',
'nil','nil', true);

SET @true_index = last_insert_id();
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) select @true_index, Id From Category where Name='tie_fix';


insert into Inventory_Attribute (  URI ,
URI_Selected , URI_CloseUp , Description , 
New_Code , Fabric_Name , Price , Price_Collar_Contrast ,
Price_Cuff_Contrast , Price_Placket , Composition , Yarn , 
Colour , Weaving , Treatment , Type , Old_Fabric_Code, 
Active ) values ('nil',
'nil', 'nil', 'tie_fix and handkerchief','nil','0', 0.0,0.0,0.0,0.0,
'nil','nil','nil','nil','nil',
'nil','nil', false);

SET @false_index = last_insert_id();

insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) select @false_index, Id From Category where Name='tie_fix';
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) select @true_index, Id From Category where Name='handkerchief';
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) select @false_index, Id From Category where Name='handkerchief';

insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('tie_fix', 'selection of default Tie fix inventory item for selection', @true_index);
insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('handkerchief', 'selection of default handkerchief inventory item for selection', @true_index);

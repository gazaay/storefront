use tmo;

CREATE TABLE tmo.Measurement (
       Id INT NOT NULL AUTO_INCREMENT
     , json VARCHAR(500)
     , Selection VARCHAR(50)
     , user_id VARCHAR(50)
     , Measure_Type CHAR(50)
     , Standard_Size CHAR(10)
     , Best_Collar FLOAT
     , Best_Chest FLOAT
     , Best_Waist FLOAT
     , Best_Length_To_Waist FLOAT
     , Best_Width FLOAT
     , Best_Shirt_Length FLOAT
     , Best_Shoulder_Width FLOAT
     , Best_Long_UpLen FLOAT
     , Best_Long_LowLen FLOAT
     , Best_Short_UpLen FLOAT
     , Best_Short_LowLen FLOAT
     , Best_Cuff FLOAT
     , Best_Sleeve_Width FLOAT
     , Best_Short_Opening FLOAT
     , Own_Neck FLOAT
     , Own_Chest FLOAT
     , Own_Len_Chest FLOAT
     , Own_Waist FLOAT
     , Own_Len_Waist FLOAT
     , Own_Shoulder_Width FLOAT
     , Own_Seat FLOAT
     , Own_Len_Seat FLOAT
     , Own_Shirt_Len FLOAT
     , Own_Arm_Len FLOAT
     , Own_Short_Len FLOAT
     , Own_Wrist FLOAT
     , Own_Biceps FLOAT
     , Own_Armpit FLOAT
     , Measure_Name VARCHAR(50)
     , PRIMARY KEY (Id)
);

ALTER TABLE tmo.Invoice ADD Address_JSON VARCHAR(1000);

ALTER TABLE tmo.Invoice ADD Measurement_JSON VARCHAR(1000);

use tmo;

CREATE TABLE tmo.Promotion (
       Id INTEGER NOT NULL AUTO_INCREMENT
     , Promo_Type CHAR(10)
     , Code CHAR(10)
     , Target VARCHAR(50)
     , Limits INTEGER
     , Claims INTEGER
     , Discount FLOAT
     , PRIMARY KEY (Id)
);

 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('POEGEAFW', '*', '999', '0', 1.0);
 
 Alter TABLE tmo.Invoice ADD Discount FLOAT;
 Alter TABLE tmo.Invoice ADD Promotion_Code CHAR(10); 
 
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('GJEKRJDN', 'jezztoh@gmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('EOWPRNGN', 'kiminc78@gmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('QLERNEKF', 'kahhing@thelaufamily.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('BNCJSKJR', 'alex@beermarket.com.sg', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('NMJFJDEA', 'hudsonwong77@gmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('AKEKWNRG', 'chailih@gmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('ALWELMRG', 'michaelmah@hotmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('BNFMFJRT', 'paul.sek@asia.ing.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('EKWLWNGD', 'spidertan@hotmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('CSDFEWQQ', 'tanchihmien@hotmail.com', '1', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('GJEJRNEN', 'eric.lau@tailormeonline.com', '30', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('YOTORPEJ', 'ken.yuen@tailormeonline.com', '30', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('QEJRJRKD', 'marcus.lio@tailormeonline.com', '30', '0', 1.0);
 Insert into Promotion(Code, Target, Limits, Claims, Discount) values ('QWERTYUI', 'garylamj@gmail.com', '30', '0', 1.0);

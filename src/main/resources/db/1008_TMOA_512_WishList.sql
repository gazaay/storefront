CREATE TABLE tmo.Wish (
       Id INT NOT NULL AUTO_INCREMENT
     , Shirt_Id INT
     , username VARCHAR(50)
     , Status VARCHAR(50)
     , Session_Id VARCHAR(120)
     , Created_Date DATETIME
     , Modified_Date DATETIME
     , PRIMARY KEY (Id)
);
use tmo;

Alter TABLE tmo.users ADD Id INTEGER;

insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('users_seq_num', 'user id sequence number', '100');

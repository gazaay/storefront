DROP SCHEMA PUBLIC CASCADE;

CREATE TABLE Shirt (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Style VARCHAR(255)
     , Invoice VARCHAR(50)
);


CREATE TABLE Image_storage (
       Id INT NOT NULL IDENTITY PRIMARY KEY
);

CREATE TABLE Stock (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Title VARCHAR(255)
);

CREATE TABLE Shirt_Attribute (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Attribute_name VARCHAR(50)
     , Attribute_describtion VARCHAR(255)
     , Image_storage_Id INT
     , CONSTRAINT FK_Stock_Attribute_1 FOREIGN KEY (Image_storage_Id)
                  REFERENCES Image_storage (Id)
);

CREATE TABLE Shirt_Shirt_Attribute (
       Shirt_Id INT
     , Shirt_Attribute_Id INT
     , CONSTRAINT FK_Shirt_Shirt_Attribute_1 FOREIGN KEY (Shirt_Attribute_Id)
                  REFERENCES Shirt_Attribute (Id)
     , CONSTRAINT FK_Shirt_Shirt_Attribute_2 FOREIGN KEY (Shirt_Id)
                  REFERENCES Shirt (Id)
);



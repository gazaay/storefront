use tmo_sample;
select 
    this_.id as id19_2_,
    this_.Attribute_name as Attribute2_19_2_,
    this_.Attribute_describtion as Attribute3_19_2_,
    inventorya3_.Shirt_Attribute_Id as Shirt1_19_,
    inventorya1_.Id as Inventory2_,
    inventorya1_.Id as Id16_0_,
    inventorya1_.Category_Id as Category5_16_0_,
    inventorya1_.URI as URI16_0_,
    inventorya1_.URI_CloseUp as URI3_16_0_,
    inventorya1_.URI_Selected as URI4_16_0_,
    category5_.Id as Id14_1_,
    category5_.name as name14_1_
from
    Shirt_Attribute this_
        inner join
    Shirt_Attribute_Inventory_Attributeinventorya3_ ON this_.id = inventorya3_.Shirt_Attribute_Id
        inner join
    Inventory_Attributeinventorya1_ ON inventorya3_.Inventory_Attribute_Id = inventorya1_.Id
        left outer join
    Categorycategory5_ ON inventorya1_.Category_Id = category5_.Id
where
    (inventorya1_.Id = 1 or inventorya1_.Id = 1);
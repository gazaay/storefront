

CREATE TABLE Shirt (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , Title VARCHAR(255)
     , Invoice VARCHAR(50)
);

CREATE TABLE Image_storage (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , layer INT
     , URL VARCHAR(255)
);

CREATE TABLE Inventory_Attribute (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , URI VARCHAR(255)
     , URI_Selected VARCHAR(255)
     , URI_CloseUp VARCHAR(255)
);

CREATE TABLE Toolbox_Layout (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , Name CHAR(50)
);

CREATE TABLE Stock (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , Title VARCHAR(255)
);

CREATE TABLE Shirt_Attribute (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , Attribute_name VARCHAR(50)
     , Attribute_describtion VARCHAR(255)
     , Image_storage_Id INT
     , CONSTRAINT FK_Stock_Attribute_1 FOREIGN KEY (Image_storage_Id)
                  REFERENCES Image_storage (Id)
);

CREATE TABLE Category (
       Id INT NOT NULL
     , Name CHAR(50)
     , AlterShirt_Id INT
     , Toolbox__Layout_Id INT
     , PRIMARY KEY (Id)
     , CONSTRAINT FK_Category_2 FOREIGN KEY (Toolbox__Layout_Id)
                  REFERENCES Toolbox_Layout (Id)
);

CREATE TABLE Inventory_Attribute_Filter (
       Id INT NOT NULL
     , Red INT
     , Green INT
     , Blue INT
     , Pattern CHAR(50)
     , Inventory_Attribute_Id INT
     , PRIMARY KEY (Id)
     , CONSTRAINT FK_InventoryAttributeFilter_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE Shirt_Attribute_Inventory_Attribute (
       Shirt_Attribute_Id INT NOT NULL
     , Inventory_Attribute_Id INT NOT NULL
     , PRIMARY KEY (Shirt_Attribute_Id, Inventory_Attribute_Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_2 FOREIGN KEY (Shirt_Attribute_Id)
                  REFERENCES Shirt_Attribute (Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE AlterShirt (
       Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
     , AlterShirt_Group_Name CHAR(10)
     , CONSTRAINT FK_AlterShirt_1 FOREIGN KEY (Id)
                  REFERENCES Category (Id)
);

CREATE TABLE Inventory_Attribute_Category (
       Category_Id INT
     , Inventory_Attribute_Id INT
     , CONSTRAINT FK_Inventory_Attribute_Category_1 FOREIGN KEY (Category_Id)
                  REFERENCES Category (Id)
     , CONSTRAINT FK_Inventory_Attribute_Category_2 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE Shirt_Shirt_Attribute (
       Shirt_Id INT
     , Shirt_Attribute_Id INT
     , CONSTRAINT FK_Shirt_Shirt_Attribute_1 FOREIGN KEY (Shirt_Id)
                  REFERENCES Shirt_Attribute (Id)
     , CONSTRAINT FK_Shirt_Shirt_Attribute_2 FOREIGN KEY (Shirt_Id)
                  REFERENCES Shirt (Id)
);


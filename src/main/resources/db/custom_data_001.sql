insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('web_master_email', 'This is used for the web user to contact our web admin with this email address.', 'garylamj@gmail.com');
insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('SG_GST', 'This is the rate of GST', '0.07');
insert into System_Config (Config_Name, Config_Usage, Config_Value) values ('order_email', 'This is the receipient of the order', 'garylamj@gmail.com');

insert into AlterShirt (Id, AlterShirt_Group_Name) values (1, 'design');
insert into AlterShirt (Id, AlterShirt_Group_Name) values (2, 'customize');
insert into AlterShirt (Id, AlterShirt_Group_Name) values (3, 'contrasts');
insert into AlterShirt (Id, AlterShirt_Group_Name) values (4, 'options');
insert into AlterShirt (Id, AlterShirt_Group_Name) values (5, 'measurements');



insert into Toolbox_Layout (Id, Name) values (1, 'split');
insert into Toolbox_Layout (Id, Name) values (2, 'with_thumb_zoom');
insert into Toolbox_Layout (Id, Name) values (3, 'with_thumb_zoom_filter');
insert into Toolbox_Layout (Id, Name) values (4, 'with_thumb');

insert into Category (Id, Name) values (13, 'button_thread');
insert into Category (Id, Name) values (14, 'collar_outer');
insert into Category (Id, Name) values (15, 'collar_inner');
insert into Category (Id, Name) values (16, 'cuff_outer');
insert into Category (Id, Name) values (17, 'cuff_inner');
insert into Category (Id, Name) values (18, 'placket_outer');
insert into Category (Id, Name) values (19, 'placket_inner');
insert into Category (Id, Name) values (20, 'placket_flip');
insert into Category (Id, Name) values (21, 'add_on');
insert into Category (Id, Name) values (22, 'standard_measure');
insert into Category (Id, Name) values (23, 'best_measure');
insert into Category (Id, Name) values (24, 'own_measure');
insert into Category (Id, Name) values (25, 'placket');
insert into Category (Id, Name) values (26, 'cuff_short_outer');
insert into Category (Id, Name) values (27, 'cuff_short_inner');


update Category set Toolbox_Layout_Id = 3;
--  shirt_types is hidden
update Category set AlterShirt_Id = 1 Where Name IN ( 'shirt_sleeve', 'fabrics', 'collar', 'cuff');
-- update Category set AlterShirt_Id = 2 Where Name IN ('buttons', 'button_thread', 'thread', 'fastening', 'pocket', 'bottom_cuts','back','yoke');
update Category set AlterShirt_Id = 2 Where Name IN ('buttons', 'button_thread', 'pocket', 'bottom_cuts', 'fastening', 'back', 'yoke');
update Category set AlterShirt_Id = 3 Where Name IN ('collar_outer', 'collar_inner', 'cuff_outer', 'cuff_inner');
-- update Category set AlterShirt_Id = 3 Where Name IN ('collar_outer', 'collar_inner', 'cuff_outer', 'cuff_inner', 'placket_outer', 'placket_inner','placket_flip');
update Category set AlterShirt_Id = 4 Where Name IN ('add_on');
update Category set AlterShirt_Id = 5 Where Name IN ('standard_measure', 'best_measure', 'own_measure');

-- insert into Shirt_Attribute_Inventory_Attribute (Shirt_Attribute_Id, Inventory_Attribute_Id) SELECT 1 , Id FROM Inventory_Attribute WHERE URI like '%cuff%';

insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) SELECT Inventory_Attribute_Id, 13 FROM Inventory_Attribute_Category WHERE Category_Id = 10;
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) SELECT Inventory_Attribute_Id, 14 FROM Inventory_Attribute_Category WHERE Category_Id = 9;
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) SELECT Inventory_Attribute_Id, 15 FROM Inventory_Attribute_Category WHERE Category_Id = 9;
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) SELECT Inventory_Attribute_Id, 16 FROM Inventory_Attribute_Category WHERE Category_Id = 9;
insert into Inventory_Attribute_Category (Inventory_Attribute_Id, Category_Id) SELECT Inventory_Attribute_Id, 17 FROM Inventory_Attribute_Category WHERE Category_Id = 9;

-- insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 13, Zoom_Id from Category_Zoom where Category=10;
insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 13, Zoom_Id from Category_Zoom where Category_Id=4;
insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 14, Zoom_Id from Category_Zoom where Category_Id=5;
insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 15, Zoom_Id from Category_Zoom where Category_Id=5;
insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 16, Zoom_Id from Category_Zoom where Category_Id=6;
insert into Category_Zoom (Category_Id, Zoom_Id) SELECT 17, Zoom_Id from Category_Zoom where Category_Id=6;

insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (1, 'Fabric 0001', '{"fabrics" : 52,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 34,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 17,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '52',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (2, 'Fabric 0001', '{"fabrics" : 53,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 35,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 18,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '53',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (3, 'Fabric 0001', '{"fabrics" : 54,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 36,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 19,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '54',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (4, 'Fabric 0001', '{"fabrics" : 55,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 37,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 20,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '55',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (5, 'Fabric 0001', '{"fabrics" : 56,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 38,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 21,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '56',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (6, 'Fabric 0001', '{"fabrics" : 57,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 39,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 22,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '57',0.00);
insert into Shirt (Id, Title, Pattern_JSON, Reference_Key, Price) values (7, 'Fabric 0001', '{"fabrics" : 58,"shirt_type" : 230,"cuff" : 49,"cuff_outer" : 52,"cuff_inner" : 52,"collar_outer" : 52,"collar_inner" : 52,"bottom_cuts" : 12,"collar" : 40,"shirt_sleeve" : 236,"pocket" : 214,"buttons" : 23,"fastening" : 209,"button_thread" : 248,"yoke":280,"long_sleeve" : 9999}', '58',0.00);

insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (8, 'Causino',  'TMO18001', 79.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.001.png', 'resources/images/readytowear.001.jpg');
insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (9, 'Arica',  'TMO18002', 89.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.002.png', 'resources/images/readytowear.002.jpg');
insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (10, 'Montt',  'TMO18003', 69.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.003.png', 'resources/images/readytowear.003.jpg');
insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (11, 'Osorno',  'TMO18004', 79.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.004.png', 'resources/images/readytowear.004.jpg');
insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (12, 'Valdivia',  'TMO18005', 79.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.005.png', 'resources/images/readytowear.005.jpg');
insert into Shirt (Id, Title,  Reference_Key, Price, Fit, Sleeve, Thumb_URL, Main_Image_URL) values (13, 'Curico',  'TMO18006', 89.90, 'Slim fit', 'Long Sleeve', 'resources/images/readytowear_thumb.006.png', 'resources/images/readytowear.006.jpg');

-- 
-- insert into Zoom (Id, Description, Layer) values (1, 'a', 1);
-- insert into Zoom (Id, Description, Layer) values (2, 'b', 2);
-- insert into Zoom (Id, Description, Layer) values (3, 'c', 3);
-- insert into Zoom (Id, Description, Layer) values (4, 'd', 4);
-- insert into Zoom (Id, Description, Layer) values (5, 'e', 5);
-- insert into Zoom (Id, Description, Layer) values (6, 'f', 6);
-- insert into Zoom (Id, Description, Layer) values (7, 'g', 7);
-- insert into Zoom (Id, Description, Layer) values (8, 'h', 8);
-- insert into Zoom (Id, Description, Layer) values (9, 'i', 9);
-- 
-- 
-- --  	{"fabrics":52,"shirt_type":230,"cuff":49,"cuff_outer":52,"cuff_inner":52,"collar_outer":52,
-- --  "collar_inner":52,"bottom_cuts":12,"collar":34,"shirt_sleeve":236,"pocket":214,"buttons":17,"fastening":209,"button_thread":248,"long_sleeve":9999}
-- 
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 1 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND cat.Name in ('fabrics', 'collar', 'shirt_type', 'bottom_cuts' ) ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 3 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND cat.Name in ('fabrics', 'collar', 'placket' ) ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 4 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ('collar', 'collar_inner') ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 5 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ('collar', 'collar_outer' ) ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 6 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ( 'collar', 'button_thread') ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 7 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ( 'collar', 'buttons' ) ;
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 8 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ('collar', 'placket_thread' ) ; // new Placket_Thread
-- insert into Zoom_Inventory_Attribute (Zoom_Id, Inventory_Attribute_Id) SELECT 9 , Inventory_Attribute_Id FROM Inventory_Attribute_Category c inner join Category cat on cat.Id = c.Category_Id where Inventory_Attribute_Id in (52, 230, 49, 12, 34, 236, 214, 17, 209, 248) AND  cat.Name in ('collar', 'placket_button' ) ; // new Placket_button
-- 
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (1, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (2, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (3, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (4, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (5, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (6, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (7, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (8, 5);
-- insert into Zoom_Category(Zoom_Id, Category_Id) values (9, 5);

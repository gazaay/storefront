CREATE TABLE Shirt (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Title VARCHAR(255)
     , Invoice VARCHAR(50)
     , Thumb LONGVARCHAR
     , Pattern_JSON VARCHAR(1000)
     , Reference_Key CHAR(10)
     , Code VARCHAR(50)
     , Price FLOAT(2)
     , Fit VARCHAR(55)
     , Sleeve VARCHAR(55)
     , Thumb_URL VARCHAR(255)
     , Main_Image_URL VARCHAR(255)
);

CREATE TABLE Image_storage (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , layer INT
     , URL VARCHAR(255)
);

CREATE TABLE Inventory_Attribute (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , URI VARCHAR(255)
     , URI_Selected VARCHAR(255)
     , URI_CloseUp VARCHAR(255)
     , Description VARCHAR(255)
     , New_Code VARCHAR(255)
     , Fabric_Name VARCHAR(255)
     , Price FLOAT(20)
     , Price_Collar_Contrast FLOAT(20)
     , Price_Cuff_Contrast FLOAT(20)
     , Price_Placket FLOAT(20)
     , Composition VARCHAR(255)
     , Yarn VARCHAR(255)
     , Colour VARCHAR(255)
     , Weaving VARCHAR(255)
     , Treatment VARCHAR(255)
     , Type VARCHAR(255)
     , Old_Fabric_Code VARCHAR(255)
     , Active BOOLEAN
);

CREATE TABLE AlterShirt (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , AlterShirt_Group_Name CHAR(10)
);

CREATE TABLE Toolbox_Layout (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Name CHAR(50)
);

CREATE TABLE Zoom (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Image_storage_Id INT
     , Description VARCHAR(255)
     , Layer INT
);

CREATE TABLE System_Config (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Config_Name CHAR(10)
     , Config_Usage CHAR(10)
     , Config_Value CHAR(255)
);

CREATE TABLE Promotion (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Promotion_Code VARCHAR(8)
     , Has_Distributed BOOLEAN
     , Distribution_To VARCHAR(50)
     , No_Of_Allowance INT
     , Expiry_Date DATETIME
);

CREATE TABLE Stock (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Title VARCHAR(255)
);

CREATE TABLE Shirt_Attribute (
       Id INT NOT NULL IDENTITY PRIMARY KEY
     , Attribute_name VARCHAR(50)
     , Attribute_description VARCHAR(255)
     , Image_storage_Id INT
     , Layer INT
     , CONSTRAINT FK_Stock_Attribute_1 FOREIGN KEY (Image_storage_Id)
                  REFERENCES Image_storage (Id)
);

CREATE TABLE Category (
       Id INT NOT NULL
     , Name CHAR(50)
     , AlterShirt_Id INT
     , Toolbox_Layout_Id INT
     , Header VARCHAR(255)
     , PRIMARY KEY (Id)
     , CONSTRAINT FK_Category_2 FOREIGN KEY (Toolbox_Layout_Id)
                  REFERENCES Toolbox_Layout (Id)
     , CONSTRAINT FK_Category_3 FOREIGN KEY (AlterShirt_Id)
                  REFERENCES AlterShirt (Id)
);

CREATE TABLE Shirt_Attribute_Inventory_Attribute (
       Shirt_Attribute_Id INT NOT NULL
     , Inventory_Attribute_Id INT NOT NULL
     , PRIMARY KEY (Shirt_Attribute_Id, Inventory_Attribute_Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_2 FOREIGN KEY (Shirt_Attribute_Id)
                  REFERENCES Shirt_Attribute (Id)
     , CONSTRAINT FK_Shirt_Attribute_Inventory_Attribute_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE Inventory_Attribute_Category (
       Category_Id INT
     , Inventory_Attribute_Id INT
     , CONSTRAINT FK_Inventory_Attribute_Category_1 FOREIGN KEY (Category_Id)
                  REFERENCES Category (Id)
     , CONSTRAINT FK_Inventory_Attribute_Category_2 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE Zoom_Inventory_Attribute (
       Zoom_Id INT
     , Inventory_Attribute_Id INT
     , CONSTRAINT FK_Zoom_Inventory_Attribute_2 FOREIGN KEY (Zoom_Id)
                  REFERENCES Zoom (Id)
     , CONSTRAINT FK_Zoom_Inventory_Attribute_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);

CREATE TABLE Category_Zoom (
       Category_Id INT
     , Zoom_Id INT
     , CONSTRAINT FK_Category_Zoom_1 FOREIGN KEY (Category_Id)
                  REFERENCES Category (Id)
     , CONSTRAINT FK_Category_Zoom_2 FOREIGN KEY (Zoom_Id)
                  REFERENCES Zoom (Id)
);

CREATE TABLE Inventory_Attribute_Filter (
       Id INT NOT NULL
     , Red INT
     , Green INT
     , Blue INT
     , Pattern CHAR(50)
     , Inventory_Attribute_Id INT
     , PRIMARY KEY (Id)
     , CONSTRAINT FK_InventoryAttributeFilter_1 FOREIGN KEY (Inventory_Attribute_Id)
                  REFERENCES Inventory_Attribute (Id)
);


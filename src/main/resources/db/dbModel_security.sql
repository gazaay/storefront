create table users(
      username varchar(50) not null primary key,
      password varchar(50) not null,
      firstname varchar(50),
      lastname varchar(50),
      country varchar(50),
      phone varchar(30),
      signup boolean not null,
      enabled boolean not null);

  create table authorities (
      username varchar(50) not null,
      authority varchar(50) not null,
      constraint fk_authorities_users foreign key(username) references users(username));
      create unique index ix_auth_username on authorities (username,authority);
      
      
INSERT INTO users ( USERNAME,PASSWORD, firstname, lastname, signup, ENABLED) VALUES ( 'gary', '123456', 'Gary', 'Lam',TRUE,  TRUE);
INSERT INTO users ( USERNAME,PASSWORD, firstname, lastname, signup, ENABLED) VALUES ( 'admin', 'admin', 'Admin', 'Test', TRUE, TRUE);
 
INSERT INTO authorities (USERNAME,AUTHORITY) VALUES ('gary', 'ROLE_USER');
INSERT INTO authorities (USERNAME,AUTHORITY) VALUES ('admin', 'ROLE_ADMIN');

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="language" content="en" />

<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/inc/screen.css" />"
	media="screen, projection">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/inc/print.css" />" media="print">
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ie.css" />" media="screen, projection" />
	<![endif]-->

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/inc/main.css" />">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/inc/form.css" />"><!-- Basic Jquery Slider styles (This is the only essential css file) -->
<link rel="stylesheet"
	href="<c:url value="/resources/inc/css/basic-jquery-slider.css" />">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" />"
type="text/javascript"></script> <script
	src="<c:url value="/resources/inc/jquery-ui-1.js" />"></script> <!-- Load jQuery and the plug-in -->
<script
	src="<c:url value="/resources/inc/js/libs/jquery-1.6.2.min.js" />"></script>
<script src="<c:url value="/resources/inc/js/basic-jquery-slider.js" />"></script>

<!--  Attach the plug-in to the slider parent element and adjust the settings as required -->
<script>
	$(document).ready(function() {

		$('#banner').bjqs({
			'animation' : 'slide',
			'width' : 971,
			'height' : 547
		});

	});
</script>
<title>TMO.COM - Default</title>
<style>
div.dotted {
	border-top: 1px dotted black;
	margin-top: 10px;
	width: 970px;
}

#content_home {
	padding: 20px;
	font-family: AvantGardeITbyBT;
}

h3 {
	font-size: 17px;
	font-family: AvantGardeITbyBT;
	margin-bottom: 10px;
}

.float_left {
	float: left;
}

#banner {
	border: 0px;
	position: relative;
}

#jumpLogo {
	position: absolute;
	top: 0;
	right: 0;
	width: 200px;
	margin-right: 393px;
}

#jumpLogo img {
	z-index: -1;
}
</style>
</head>

<body>
<div class="container" id="page"><?php include('header.php'); ?> <!-- header -->

<div class="container">

<div id="content">
<div id="banner">
<div id="jumpLogo"><a href="shirt_type.php"><img
	src="img/slider/banner-logo.png"></a></div>
<!-- start Basic Jquery Slider -->
<ul class="bjqs">
	<li><a href="shirt_type.php"><img
		src="<c:url value="/resources/img/slider/banner1.jpg" />"></a></li>
	<li><a href="shirt_type.php"><img
		src="<c:url value="/resources/img/slider/banner2.jpg" />"></a></li>
	<li><a href="shirt_type.php"><img
		src="<c:url value="/resources/img/slider/banner3.jpg" />"></a></li>
	<li><a href="shirt_type.php"><img
		src="<c:url value="/resources/img/slider/banner4.jpg" />"></a></li>
	<li><a href="shirt_type.php"><img
		src="<c:url value="/resources/img/slider/banner5.jpg" />"></a></li>
</ul>
<!-- end Basic jQuery Slider --></div>
<br />
<div class="dotted"></div>
<br />
<div id="content_home">
<h3 style="margin-left: 36px;">READY TO WEAR</h3>
<br />
<div style="margin-left: -55px;">
<div class="float_left" style="width: 36px;"><img
	src="<c:url value="/resources/images/home/left_arrow.jpg"  />" /></div>
<div class="float_left" style="width: 200px; margin-left: 0px;"><img
	src="<c:url value="/resources/images/home/shirt1.jpg" />" style="z-index: -1" /><br />
<strong style="font-size: 15px;">Busso, blue</strong><br />
Normal fit<br />
Long-sleeve
<div style="font-size: 18px; padding-top: 5px; margin-left: 0px;">
$159.90</div>
</div>
<div class="float_left" style="width: 200px;"><img
	src="<c:url value="/resources/images/home/shirt1.jpg" />" style="z-index: -1" /><br />
<strong style="font-size: 15px;">Busso, blue</strong><br />
Normal fit<br />
Long-sleeve
<div style="font-size: 18px; padding-top: 5px; margin-left: 0px;">
$159.90</div>
</div>
<div class="float_left" style="width: 200px;"><img
	src="<c:url value="/resources/images/home/shirt1.jpg" />" style="z-index: -1" /><br />
<strong style="font-size: 15px;">Busso, blue</strong><br />
Normal fit<br />
Long-sleeve
<div style="font-size: 18px; padding-top: 5px; margin-left: 0px;">
$159.90</div>
</div>
<div class="float_left" style="width: 182px;"><img
	src="<c:url value="/resources/images/home/shirt1.jpg" />" style="z-index: -1" /><br />
<strong style="font-size: 15px;">Busso, blue</strong><br />
Normal fit<br />
Long-sleeve
<div style="font-size: 18px; padding-top: 5px; margin-left: 0px;">
$159.90</div>
</div>

<div class="float_left" style="width: 36px;"><img
	src="<c:url value="/resources/images/home/right_arrow.jpg" />" /></div>
</div>
<br clear="all" />
</div>
<div class="dotted"></div>
<div id="content_home">
<table>
	<tr>
		<td><img src="<c:url value="/resources/images/home/h1.jpg" />" /></td>
		<td><img src="<c:url value="/resources/images/home/h2.jpg" />" /></td>
	</tr>
</table>
</div>
</div>
<!-- content --></div>
</div>
<!-- page -->
</body>
</html>
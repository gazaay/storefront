<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ajax Demo</title>
<script type="text/javascript"
	src="<c:url value="/resources/jquery-1.8.1.min.js" /> "></script>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		loadTextOnServer();
	});

	function loadTextOnServer() {
		var url = "./ok wok.json";
		$.getJSON(url, {
			inputMessage : 'hello'
		}, function(message) {
			alert("test");
			$('#textToChange').text(message.name);
		});
	}
</script>
<body>
<h1>The following content is loaded dynamically through Ajax</h1>
<div id="textToChange">This message should not be here any more!</div>

</body>
</html>
String.prototype.splice = function(idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

if ( typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) === str;
    };
}

if ( typeof String.prototype.isEmpty !== 'function') {
    String.prototype.isEmpty = function isEmpty(val) {
        return ( typeof (this) === 'undefined' || this === undefined || this === null || this.length <= 0) ? true : false;
        // return ( typeof (val) === 'undefined' || val === undefined || val == null || val.length <= 0) ? true : false;
    }
}

function reset() {

}

function set_value(key, value) {

    $('#loading').fadeIn('slow');

    $.ajax({
        url : 'system/modules/session.php?key=' + key + '&value=' + value,
        cache : false,
        async : false,
        success : function(html) {
            show_price();
        }
    });

}

function get_value(key) {

    var sessionvar = null;
    $.ajax({

        url : 'system/modules/getsession.php?key=' + key,
        cache : false,
        async : false,
        success : function(html) {
            sessionvar = html;
        }
    });

    return sessionvar;

}


$("#details").click(function() {

    $('.modalOverlay').remove();
    $("body").append('<div class="modalOverlay">');
    $("#detailsZoom").dialog({
        close : function(event, ui) {
            $('.modalOverlay').remove();
        },
        height : 470,
        width : 470,
        title : 'DETAILS SUMMARY',
        closeText : ' '

    });

});

function show_price() {
    //
    // $.ajax({
    // url : 'display_price.php',
    // cache : false,
    // async : false,
    // success : function(html) {
    // $('#total').html(html);
    //
    // }
    // });

}

function notify_shirt_ui() {
    $("#cuff").toggleClass("dim", !shirt_json.hasCuff);
    $("#cuff").toggleClass("clickable", shirt_json.hasCuff);
}

// Shopping cart
function AddItemsToCart(items) {
    cart.item.push(items);
}

// MVC
//TODO: remove by tmo-core
function loadServerResource(path, method, data, handler, errorHandler, blockControl) {
    if (blockControl !== null && blockControl !== undefined) {
        blockControl.block({
            message : "loading...",
            css : {
                border : 'none',
                padding : '15px',
                'font-size' : '20px',
                backgroundColor : '#9A98A3',
                '-webkit-border-radius' : '10px',
                '-moz-border-radius' : '10px',
                height : '22px',
                left : '26px',
                width : '50%',
                opacity : .5,
                color : '#FFFFFF',
                padding : '1px'
            },
            overlayCSS : {
                backgroundColor : '#fff'
            }
        });
    }
    return $.ajax({
        type : method,
        url : path,
        contentType : "application/json; charset=utf-8",
        dataType : "json",
        data : data,
        error : errorHandler,
        complete : function() {
            if (blockControl !== null && blockControl !== undefined) {
                blockControl.unblock();
            }
        }
    }).done(handler);

}

function ruler_click_handler(ruler_complex_name, menu_name) {

    var ruler_name_array = ruler_complex_name.split("_");
    var ruler_name = ruler_name_array[0];
    var ruler_index = ruler_name_array[1];

    if (!isNaN(menu_name)) {
        $("#" + menu_name).click();
    } else {
        $("#loading_box").click();
    }

    $(".ruler_menu_img").attr('src', 'resources/inc/ruler.png');

    $("#" + ruler_name + "_img").attr('src', 'resources/images/ruler-white.png');
    var path = "inv/alter/" + ruler_index;
    var method = "GET";
    var data = "";
    loadServerResource(path, method, data, function(results) {
        var data = results.categories;
        $(".menu").addClass("hide");
        $(".menu").removeClass("show");
        $.each(data, function(index, itemData) {
            $("#" + itemData.name).removeClass("hide");
            $("#" + itemData.name).addClass("show");
            if (index === 0) {
                // TODO: add default flag to database category table
                if (!(!menu_name || 0 === menu_name.length)) {
                    $("#" + menu_name).click();
                } else if (ruler_complex_name.indexOf("design_1") !== -1) {
                    $("#shirt_sleeve").click();
                } else if (ruler_complex_name.indexOf("customize_2") !== -1) {

                    $("#buttons").click();
                } else if (ruler_complex_name.indexOf("contrast_3") !== -1) {

                    $("#collar_outer").click();
                } else {

                    $("#" + itemData.name).click();
                }
            }
        });

    });

}

function show_div(id) {
    $(".measure_image").hide();
    $('.d' + id).show();

    // $('#ld1').hide();
    // $('#ld2').hide();
    // $('#ld3').hide();
    // $('#ld4').hide();
    // $('#ld5').hide();
    // $('#ld6').hide();
    // $('#ld7').hide();
    // $('#ld8').hide();
    // $('#ld9').hide();
    // $('#ld10').hide();
    // $('#ld11').hide();
    // $('#ld12').hide();
    // $('#ld13').hide();
    // $('#ld14').hide();
    $('.ld' + id).show();

}

function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}

function FilterBy(type, control_name, array) {
    var result_html = '', selectId = 'filter_' + type + '_select', option_hash = {};

    switch (control_name) {
        case "option":
            $.each(array, function(index, value) {
                var rawdata = value[type].toLowerCase(), raw_array = rawdata.split(',');
                $.each(raw_array, function(index, raw_value) {
                    if ( typeof (option_hash[$.trim(raw_value)] ) === 'undefined' || option_hash[$.trim(raw_value)] === null) {
                        option_hash[$.trim(raw_value)] = raw_value;
                        result_html += '<option >' + raw_value + '</option>';
                    }
                });

            });
            break;
        case "ul":
            break;
        default:
            break;
    }

    $('#' + selectId).html(result_html);
    return result_html;
}

function hidePageItems(min, max, control_name) {
    var count = 0;
    $(control_name).each(function(index, item) {
        if ($(item).css('display') == "block") {
            if (count >= min && count < max) {
            } else {
                $(item).hide();
            }
            count++;
        }
    });
    if (max >= count) {
        $("#next").hide();
    } else {
        $("#next").show();

    }
}

function ProcessFilter(filter) {

    if (filter == 'all') {
        $(".filter_all").show();
    } else {
        $.tmo.toolbox.filter = filter;
        $(".filter_all").hide();
        $(".filter_" + filter).show();
    }
    var min = $.tmo.design.page_number * 12;
    var max = min + 12;
    if ($('#fabrics_toolbox').is(':visible')) {
        hidePageItems(min, max, '.fabrics_toolbox_item');
    }
    if ($('#collar_outer_toolbox').is(':visible'))
        hidePageItems(min, max, '.collar_outer_toolbox_item');
    if ($('#collar_inner_toolbox').is(':visible'))
        hidePageItems(min, max, '.collar_inner_toolbox_item');
    if ($('#cuff_outer_toolbox').is(':visible'))
        hidePageItems(min, max, '.cuff_outer_toolbox_item');
    if ($('#cuff_inner_toolbox').is(':visible'))
        hidePageItems(min, max, '.cuff_inner_toolbox_item');

}

function ItemTypeFilter(filter) {
    $.tmo.design.page_number = 0;
    if (filter === "colour" || filter === "type") {
        filter = 'all';
    }
    ProcessFilter(filter);
}

function SplitByCommaInLabel(pattern, array) {
    var result = '';
    $.each(array, function(index, value) {
        result += pattern + trim11(value);
    });
    return result;
}

function trim11(str) {
    str = str.replace(/^\s+/, '');
    var i = 0;
    for ( i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function getDefaultValue(name) {
    return $.tmo.design.map_default[name];
    // switch (name) {
    // case 'collar_outer':
    // return $
    // case 'collar_inner':
    // case 'cuff_outer':
    // case 'cuff_inner':
    // case 'button_thread':
    // case 'buttons':
    // case 'cuff':
    // case 'collar':
    // break;
    // }

}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    var i = 0;
    for ( i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function popupToSaveDesign(redirect) {
    // Pop up message to ask if merge require
    try {
        window['ga'].q.push(['_addItem', '0000', // transaction ID - necessary to associate item with transaction
        'dirty', // SKU/code - required
        'Custom Shirt', // product name - necessary to associate revenue with product
        '', // category or variation
        '', // unit price - required
        '1' // quantity - required
        ]);
    } catch (err) {
    }

    $("#message-dialog").attr("title", "Do you want to keep your shirt design?");
    $("#message-dialog").dialog({
        resizable : false,
        height : '100',
        width : 555,
        modal : true,
        closeText : "",
        close : function(event, ui) {
            $("#shirt_sleeve").click();
        },
        buttons : {
            "Keep the design" : {
                "class" : 'button confirmbutton',
                text : 'Yes, add to my cart',
                click : function() {
                    addingItemToCart(function() {
                        window.location = redirect;
                    }, $("#page"));
                    $(this).dialog("close");

                    return true;
                }
            },
            "Continue" : {
                "class" : 'button confirmbutton',
                text : 'No, continue the action',
                click : function() {
                    $(this).dialog("close");
                    window.location = redirect;
                    return false;
                }
            }
        }
    });
}

function addingItemToCart(handler, control, o) {
    o = $.extend({
        "params" : ""
    }, o);

    $.tmo.cart.item = $.tmo.cart.item || [];
    var item = {};
    $("#draw").click();
    item.shirt = {};
    item.shirt.pattern_json = JSON.stringify($.tmo.design.map);
    var placket_val = $('input:radio[name=placketsbutn]:checked').val();
    var stiffness_val = $('input:radio[name=stiffness]:checked').val();
    var cuffstyle_val = $('input:radio[name=cuffstyle]:checked').val();
    var shirtstyle_val = $('input:radio[name=shirtstyle]:checked').val();
    var tiefix_checked = $('input[name=tiefix]:checked').length > 0;
    var tiefix_val = "tieFix " + tiefix_checked.toString();
    var cuff_personalize_Text_val = "Cuff Personalize Text " + $("#text_to_personalize").val();
    var cuff_personalize_Fonts_val = "Cuff Personalize Fonts " + $("#fonts_option option:selected").text();

    var handkerchief_checked = $('input[name=handkerchief]:checked').length > 0;
    var handkerchief_val = "handkerchief " + handkerchief_checked.toString();
    item.shirt.extras = [];
    item.shirt.extras.push(placket_val);
    item.shirt.extras.push(stiffness_val);
    item.shirt.extras.push(cuffstyle_val);
    item.shirt.extras.push(cuff_personalize_Text_val);
    item.shirt.extras.push(cuff_personalize_Fonts_val);
    item.shirt.extras.push(shirtstyle_val);
    item.shirt.extras.push(tiefix_val);
    item.shirt.extras.push(handkerchief_val);
    var data = "";
    if ($.browser.msie) {
    } else {

        data = document.getElementById("test_canvas").toDataURL();
    }
    var output = data.replace(/^data:image\/(png|jpg);base64,/, "");
    item.thumb = output;
    var web_method = "POST"
    if ($.tmo.design.mode === "edit") {
        //TODO: change it to PUT and update CartController.java
        web_method = "POST"
        var index_of_update = -1;
        $.each($.tmo.cart.item, function(index, cart_item) {
            if (cart_item.shirt.shirtId.toString() === $.tmo.design.shirtId) {
                index_of_update = index;
                cart_item.shirt = item.shirt;
                cart_item.thumb = item.thumb;
                cart_item.shirt.shirtId = $.tmo.design.shirtId;
            }
        });
    } else {
        $.tmo.cart.item.push(item);
    }
    $(document).trigger('overall_block');
    loadServerResource("cart" + o.params, web_method, JSON.stringify($.tmo.cart), function(cart) {
        $.tmo.cart = cart;
        $(document).trigger('cart_refresh');
        $.tmo.design.dirty = false;
        $(document).trigger('overall_block_done');
    }, null, control).done(handler);
}

function MergeCart() {
    // onload :
    // Check if temp user has non-empty cart
    var temp_cart = JSON.parse($.cookie("tmo_cart"));
    var isReload = $.cookie("reload");
    var authz = $("#uid").text();
    if (temp_cart != null && authz.length > 0 && authz != temp_cart.username && (isReload == null || isReload == "false")) {
        loadServerResource("cart/carryover?temp_username=" + temp_cart.username, "GET", "", function(cart) {
            $.cookie("reload", true);
            reloadCart();
            // location.reload();
        });
    } else {
        $.cookie("reload", false);
    }
}

function blockUI(message) {
    $(document).ajaxStart($.blockUI({
        message : message,

        onBlock : function() {
            $(".blockPage").addClass("master_blockUI");

        }
    })).ajaxStop($.unblockUI);
}

function reloadCart(requireFullCart) {
    requireFullCart = requireFullCart || true;
    $(document).trigger('block_cart');
    var cart = "cart/info";
    if (requireFullCart) {
        cart = "cart";
    }
    loadServerResource(cart + "?temp_username=" + $.tmo.cart.username, "GET", "", function(results) {
        $("#cart_summary").removeClass("hide");
        $.tmo.cart = results;
        $("#total_items_cart").text($.tmo.cart.totalItems);
        $("#total_price_cart").text($.tmo.cart.totalCostWithoutShipping.toFixed(2));

        if ($.tmo.cart.shippingMethod === "pickup_from_shop") {
            $("#pickup").prop('checked', true);
            try {
                //hideAddress();
                $(".address").hide()

            } catch (e) {
                //TODO: Clean this up.. don't call this method all the time
            }
        } else {
            $("#pickup").prop('checked', false);
        }

        if ($.tmo.cart.discount > 0) {
            $.tmo.cart.display_discount = $.tmo.cart.totalShirtCost * $.tmo.cart.discount;
        }
        if (requireFullCart) {
            if ($('#order_summary_template').length !== 0) {
                $("#order_output").pureJSTemplate({
                    id : "order_summary_template",
                    data : {
                        cart : $.tmo.cart
                    }
                });

            }
            //TODO: put CART information to Address and PromotionCode
            $($.tmo.cart.item).each(function(index, item) {
                var shirtId = item.shirt.shirtId;
                $("#paypal_shirt_number_" + shirtId).val(shirtId);
                $("#paypal_shirt_price_" + shirtId).val(item.price);
                $("#paypal_quantity_" + shirtId).val(item.quantity);
            });
            $("#paypal_shipping").val($.tmo.cart.shippingCost);

            if ($.browser.msie) {
                $(".hide_ie").hide();
                $(".ie_only").show();
            } else {
                $(".hide_ie").show();
                $(".ie_only").hide();
            }
        }
    }).done(window.setTimeout(cartUnblock, 2000));

}

function cartUnblock() {
    // $(".cart").unblock();
    // if ( typeof $.tmo.isblock !== 'undefined') {
    // $.tmo.isblock['cart'] = false;
    // }
}

// function getURLParameter(name) {
// return decodeURI(
// (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
// );
// }

function getIdFromName(name) {
    var componentArray = name.split("_");
    var array_size = componentArray.length;
    if (array_size > 0) {
        return componentArray[array_size - 1];
    } else {
        return 0;
    }
}

function handleCommentsBlur(comments_obj) {
    var comments = $(comments_obj).html();
    var item_id = getIdFromName($(comments_obj).attr('id'));
    var url = "cart/item/" + item_id + "/comments";
    var method = "POST";

    loadServerResource(url, method, comments, function(data) {
        //TODO: updated message
        // alert("Updated");
    });
}

function handleRemarksBlur(remarks_obj, invoice_number, done_handler) {
    var comments = $(remarks_obj).html();
    var item_id = getIdFromName($(remarks_obj).attr('id'));
    var url = "invoice/" + invoice_number + "/item/" + item_id + "/remarks";
    var method = "POST";

    loadServerResource(url, method, comments, function(data) {
        //TODO: updated message
        // alert("Updated");
    }).done(done_handler);
}

//TODO finish the method
function handleAdminMeasureBlur(measure_obj, invoice_number, done_handler) {
    var measurement = $(measure_obj).html();
    $(measure_obj).attr('class')
    var username = $("#customer_id").text();
    var item_id = getIdFromName($(measure_obj).attr('id'));
    $.tmo.design.measurement = {};
    $.tmo.design.measurement.selection = $.tmo.design.menu_name;
    $.tmo.design.measurement.measure_type = $.tmo.design.menu_name;
    $.tmo.design.measurement.standard_size = $('#size_measure1').val();
    $.tmo.design.measurement.best_collar = $('#best_collar').val();
    $.tmo.design.measurement.best_chest = $('#best_chest').val();
    $.tmo.design.measurement.best_waist = $('#best_waist').val();
    $.tmo.design.measurement.best_length_to_waist = $('#best_length_to_waist').val();
    $.tmo.design.measurement.best_width = $('#best_width').val();
    $.tmo.design.measurement.best_shirt_length = $('#best_shirt_length').val();
    $.tmo.design.measurement.best_shoulder_width = $('#best_shoulder_width').val();
    $.tmo.design.measurement.best_long_uplen = $('#best_long_uplen').val();
    $.tmo.design.measurement.best_long_lowlen = $('#best_long_lowlen').val();
    $.tmo.design.measurement.best_short_uplen = $('#best_short_uplen').val();
    $.tmo.design.measurement.best_short_lowlen = $('#best_short_lowlen').val();
    $.tmo.design.measurement.best_cuff = $('#best_cuff').val();
    $.tmo.design.measurement.best_sleeve_width = $('#best_sleeve_width').val();
    $.tmo.design.measurement.best_short_opening = $('#best_short_opening').val();
    $.tmo.design.measurement.own_neck = $('#own_neck').val();
    $.tmo.design.measurement.own_chest = $('#own_chest').val();
    $.tmo.design.measurement.own_len_chest = $('#own_len_chest').val();
    $.tmo.design.measurement.own_waist = $('#own_waist').val();
    $.tmo.design.measurement.own_len_waist = $('#own_len_waist').val();
    $.tmo.design.measurement.own_shoulder_width = $('#own_shoulder_width').val();
    $.tmo.design.measurement.own_seat = $('#own_seat').val();
    $.tmo.design.measurement.own_len_seat = $('#own_len_seat').val();
    $.tmo.design.measurement.own_shirt_len = $('#own_shirt_len').val();
    $.tmo.design.measurement.own_arm_len = $('#own_arm_len').val();
    $.tmo.design.measurement.own_short_len = $('#own_short_len').val();
    $.tmo.design.measurement.own_wrist = $('#own_wrist').val();
    $.tmo.design.measurement.own_biceps = $('#own_biceps').val();
    $.tmo.design.measurement.own_armpit = $('#own_armpit').val();

    var url = "measurement/user/" + username + "?measure_type=all" + "&measure_name=admin_" + invoice_number;
    var method = "POST";

    loadServerResource(url, method, JSON.stringify($.tmo.design.measurement), function(data) {
        //TODO: updated message
        // alert("Updated");
    }).done(done_handler);
}

(function() {
    $.tmoadmin = {
        message : function(content) {
            alert(content);
        }
    };
})();

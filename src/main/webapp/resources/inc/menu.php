<? if($section == 1): ?>

<? endif; ?>

<? if($section == 2): ?>
<ul class="" id="menu">
  <li class="<?=($point == 1) ? 'active':''?>"><a href="buttons.php">BUTTON</a></li>
  <li class="<?=($point == 7) ? 'active':''?>"><a href="thread.php">BUTTON THREAD</a></li>
  <li class="<?=($point == 2) ? 'active':''?>"><a href="fastening.php">FASTENING</a></li>
  <li class="<?=($point == 3) ? 'active':''?>"><a href="pocket.php">POCKET</a></li>
  <li class="<?=($point == 4) ? 'active':''?>"><a href="bottom_cut.php">BOTTOM CUT</a></li>
  <li class="<?=($point == 5) ? 'active':''?>"><a href="back_details.php">BACK DETAIL</a></li>
  <li class="<?=($point == 6) ? 'active':''?>"><a href="yoke.php">YOKE</a></li>
</ul>
<? endif; ?>

<? if($section == 3): ?>
<ul class="" id="menu">
  <li class="<?=($point == 1) ? 'active':''?>"><a href="collar_outer.php">COLLAR - Outer</a></li>
  <li class="<?=($point == 2) ? 'active':''?>"><a href="collar_inner.php">COLLAR - Inner</a></li>
  
  <?php if( strtolower($_SESSION['step1b']) == 'short' ){ ?>
  <li class="<?=($point == 3) ? 'active':''?>"><a style="color:grey;" href="#">CUFF - Outer</a></li>
  <li class="<?=($point == 4) ? 'active':''?>"><a style="color:grey;" href="#">CUFF - Inner</a></li>
  <?php }else{ ?>
  <li class="<?=($point == 3) ? 'active':''?>"><a href="cuff_outer.php">CUFF - Outer</a></li>
  <li class="<?=($point == 4) ? 'active':''?>"><a href="cuff_inner.php">CUFF - Inner</a></li>
  <?php }?>
  
  <?php if( $_SESSION['step6'] == 'hidden button (no placket)' || $_SESSION['step6'] == 'no placket' ){ ?>
  <li class="<?=($point == 5) ? 'active':''?>"><a style="color:grey;" href="#">PLACKET - Outer</a></li>
  <li class="<?=($point == 6) ? 'active':''?>"><a style="color:grey;" href="#">PLACKET - Inner</a></li>
  <li class="<?=($point == 7) ? 'active':''?>"><a style="color:grey;" href="#">PLACKET - Flip Side</a></li>
  <?php }else{ ?>
  <li class="<?=($point == 5) ? 'active':''?>"><a href="placket_outer.php">PLACKET - Outer</a></li>
  <li class="<?=($point == 6) ? 'active':''?>"><a href="placket_inner.php">PLACKET - Inner</a></li>
  <li class="<?=($point == 7) ? 'active':''?>"><a href="placket_flip.php">PLACKET - Flip Side</a></li>
  <?php }?>
  
  
  </ul>
<? endif; ?>

<? if($section == 4): ?>
<ul class="" id="menu">
  <li class="active"><a href="tie_fix.php">ADD-ON</a></li>
</ul>
<? endif; ?>

<? if($section == 5): ?>

<ul class="" id="menu">
  <li class="<?=($point == 1) ? 'active':''?>"><a href="collar_outer.php">Select Standard Measurement</a></li>
  <li class="<?=($point == 2) ? 'active':''?>"><a href="collar_inner.php">Measure Your Best Fitting Shirt</a></li>
  <li class="<?=($point == 3) ? 'active':''?>"><a style="color:grey;" href="#">Measure Your Own Body</a></li>
 
</ul>
<ul class="" id="measurements">
  <li class="<?=($point == 1) ? 'active':''?>"><a href="measure1.php">Select Standard Measurement</a></li>
  <li class="inactive"><a href="measure2.php">Measure Your Best Fitting Shirt</a></li>
  <li class="<?=($point == 3) ? 'inactive':'inactive'?>"><a href="measure3.php">Measure Your Own Body</a></li> 
</ul>
<? endif; ?>

<?php $note = 1;?>
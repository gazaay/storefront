/**
 *  Tmo Core Library
 */

(function() {
    $.tmo_admin = {
        loading : function(content) {
            $(".message").fadeOut('slow', function() {
                $(".message_container .loading_content").text(content);
                $(".message_container .loading").show();
            });
        }
    };
})();


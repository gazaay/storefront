/**
 *  Tmo Core Library
 */

/*
 * Init
 */
$(function() {
    $.tmo = $.tmo || {};
    $.extend($.tmo, {
        ajaxloading : {
            key : 0
        },
        data : {},
        toolbox : {
            filter : 'all'
        },
        design : {
            page_number : 0,
            history : {
                maps : []
            },
            mode : 'new',
            map : {},
            user_selection : {},
            menu_name : 'mobile',
            measurement : {
                measure_type : 'ownmeasure',
                metrics : 'cm',
                username : '',
                maxcm : 160
            }
        }
    });
});

/*
 * Core functions
 */
String.prototype.splice = function(idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

if ( typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) === str;
    };
}

if ( typeof String.prototype.isEmpty !== 'function') {
    String.prototype.isEmpty = function isEmpty(val) {
        return ( typeof (this) === 'undefined' || this === undefined || this === null || this.length <= 0) ? true : false;
        // return ( typeof (val) === 'undefined' || val === undefined || val == null || val.length <= 0) ? true : false;
    }
}

(function() {
    $.tmocore = {
        info : function(content) {
            $(".message").fadeOut('slow', function() {
                $(".message_container .info_content").text(content);
                $(".message_container .info").show().delay(3000).fadeOut();
            });
        },
        error : function(content) {
            alert(content);
        },
        alert : function(content) {
            alert(content);
        },
        warning : function(content) {
            alert(content);
        },
        loading : function(content) {
            $(".message").fadeOut('slow', function() {
                $(".message_container .loading_content").text(content);
                $(".message_container .loading").show();
            });
        },
        math : {
            convertToInches : function(cm) {
                return (cm * 0.39).toFixed(1);
            },
            convertToCm : function(inches) {
                return (inches * 2.54).toFixed(1);
            }
        },
        loadServerResource : function(path, method, data, handler, errorHandler, blockControl) {
            $.tmocore.loading();
            if (blockControl !== null && blockControl !== undefined) {
                blockControl.block({
                    message : "loading...",
                    css : {
                        border : 'none',
                        padding : '15px',
                        'font-size' : '20px',
                        backgroundColor : '#9A98A3',
                        '-webkit-border-radius' : '10px',
                        '-moz-border-radius' : '10px',
                        height : '22px',
                        left : '26px',
                        width : '50%',
                        opacity : .5,
                        color : '#FFFFFF',
                        padding : '1px'
                    },
                    overlayCSS : {
                        backgroundColor : '#fff'
                    }
                });
            }
            return $.ajax({
                type : method,
                url : path,
                contentType : "application/json; charset=utf-8",
                dataType : "json",
                data : data,
                error : errorHandler,
                complete : function() {
                    if (blockControl !== null && blockControl !== undefined) {
                        blockControl.unblock();
                    }
                }
            }).done(handler);
        },
        getURLParameter : function(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            var i = 0;
            for ( i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
        }
    };
})();




/**
*   NOT SURE WHAT TO DO WITH THIS MODULE, the below function is implemented in tmo-shop.js
*/
(function() {
   $.tmo_invoice = {
		   invoice : {
           save : function(handler, useSelectedUsername) {
               saveInvoice(handler, useSelectedUsername);
           },
           get : function(handler, id) {
               getInvoice(handler, id)
           },
           set_data : function(data) {
                $('#edit_fabric_name').val(data.fabric_name);
               $('#edit_id').val(data.id);
               $('#edit_price').val(data.price);
               $('#edit_price_collar_contrast').val(data.price_collar_contrast);
               $('#edit_price_cuff_contrast').val(data.price_cuff_contrast);
               $('#edit_price_placket').val(data.price_placket);
               $('#edit_composition').val(data.composition);
               $('#edit_yarn').val(data.yarn);
               $('#edit_colour').val(data.colour);
               $('#edit_weaving').val(data.weaving);
               $('#edit_treatment').val(data.treatment);
               $('#edit_type').val(data.type);
               $('#edit_new_code').val(data.new_code);
               $('#edit_active').val(data.active);
               $('#edit_image_url').val(data.uri);
               $('#edit_thumb').attr('src', data.uri);
           }
       }
   };
})();

function getInvoice(handler, id) {
   try {
       var callback = $.Callbacks();
       callback.add(handler);
       var path = "shirt/" + id;
       var method = "GET";
       var data = "";
       $.tmocore.loadServerResource(path, method, data, handler);
   } catch (err) {
       //TODO: not always working
       $.tmocore.error(err);
       callback.fire();
   }
}

function saveInvoice(handler, username) {
   try {
       var callback = $.Callbacks();
       var method = "POST";
       callback.add(handler);
       var result_inv = {};//??? WHY INVENTORY EVERY WHERE? 
       // WHAT IS THIS???
       result_inv.fabric_name = $('#edit_fabric_name').val();
       result_inv.id = $('#edit_id').val();
       result_inv.price = $('#edit_price').val();
       result_inv.price_collar_contrast = $('#edit_price_collar_contrast').val();
       result_inv.price_cuff_contrast = $('#edit_price_cuff_contrast').val();
       result_inv.price_placket = $('#edit_price_placket').val();
       result_inv.composition = $('#edit_composition').val();
       result_inv.yarn = $('#edit_yarn').val();
       result_inv.colour = $('#edit_colour').val();
       result_inv.weaving = $('#edit_weaving').val();
       result_inv.treatment = $('#edit_treatment').val();
       result_inv.type = $('#edit_type').val();
       result_inv.new_code = $('#edit_new_code').val();
       result_inv.active = $('#edit_active').val();
       result_inv.active = $('#edit_active').val();
       result_inv.uri = $('#edit_image_url').val();
       if (result_inv.id >= 1) {
           method = "PUT";
       } else {
           method = "POST";
       }
       $.tmocore.loadServerResource("shirt/" + $('#edit_id').val(), method, JSON.stringify(result_inv), function(result) {
           callback.fire();
       });
   } catch (err) {
       //TODO: not always working
       $.tmocore.error(err);
       callback.fire();
   }
}

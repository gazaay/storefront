/**
 *
 */
(function() {
    $.tmo_profile = {
        init : function(profile_form) {
            profile_form.validate({
                rules : {
                    password : {
                        required : true,
                        minlength : 5
                    },
                    conf_password : {
                        required : true,
                        minlength : 5,
                        equalTo : "#reg_password"
                    },
                    email : {
                        remote : "check_email"
                    }
                },
                messages : {
                    conf_password : {
                        required : "Repeat your password",
                        minlength : $.validator.format("Enter at least {0} characters"),
                        equalTo : "Enter the same password as above"
                    },
                    email : {
                        remote : "This email address is not avaiable for registration."
                    }
                },
                submitHandler : function(form) {
                    hasError = false;
                    //hasError = commonValidations();
                    if (!hasError) {
                        var user = {};
                        user.firstname = $('#first_name').val();
                        user.lastname = $('#last_name').val();
                        user.country = $('#country').val();
                        user.phone = $('#phone').val();
                        // user.signup = $('#newsletter').checked();
                        // var formInput = $("form#form").serialize();
                        var username = $("#uid").text();
                        var url = 'users/' + username;
                        $.tmocore.loadServerResource(url, 'PUT', JSON.stringify(user), function(data) {
                            // $.post(url, user, function(data) {
                            // $(this).delay(100, function() {
                            $('.loadingImg').css({
                                display : "none"
                            });
                            if (data) {
                                $.tmocore.info(data.description);
                            } else {
                                $.tmocore.warning(data.description);
                            }
                        }, function() {
                            $.tmocore.warning("An error occured while submit. Please try again later.");
                        });
                    }
                    return false;
                }
            });

        },
        measurement : {
            save : function(handler, useSelectedUsername) {
                saveMeasurement(handler, useSelectedUsername);
            },
            get : function(handler, username) {
                getMeasurementForUser(handler, username);
            }
        }
    };
})();

function saveMeasurement(handler, useSelectedUsername) {
    var username = "";
    if (useSelectedUsername) {
        username = $.tmo.design.measurement.username;
    } else {
        // user the current login username
        username = $("#uid").text();
    }
    $.tmo.design.measurement = {};
    $.tmo.design.measurement.selection = $.tmo.design.measurement.measure_type;
    $.tmo.design.measurement.measure_type = $.tmo.design.measurement.measure_type;
    $.tmo.design.measurement.standard_size = $('#size_measure1').val();
    $.tmo.design.measurement.best_collar = $('#best_collar').val();
    $.tmo.design.measurement.best_chest = $('#best_chest').val();
    $.tmo.design.measurement.best_waist = $('#best_waist').val();
    $.tmo.design.measurement.best_length_to_waist = $('#best_length_to_waist').val();
    $.tmo.design.measurement.best_width = $('#best_width').val();
    $.tmo.design.measurement.best_shirt_length = $('#best_shirt_length').val();
    $.tmo.design.measurement.best_shoulder_width = $('#best_shoulder_width').val();
    $.tmo.design.measurement.best_long_uplen = $('#best_long_uplen').val();
    $.tmo.design.measurement.best_long_lowlen = $('#best_long_lowlen').val();
    $.tmo.design.measurement.best_short_uplen = $('#best_short_uplen').val();
    $.tmo.design.measurement.best_short_lowlen = $('#best_short_lowlen').val();
    $.tmo.design.measurement.best_cuff = $('#best_cuff').val();
    $.tmo.design.measurement.best_sleeve_width = $('#best_sleeve_width').val();
    $.tmo.design.measurement.best_short_opening = $('#best_short_opening').val();
    $.tmo.design.measurement.own_neck = $('#own_neck').val();
    $.tmo.design.measurement.own_chest = $('#own_chest').val();
    $.tmo.design.measurement.own_len_chest = $('#own_len_chest').val();
    $.tmo.design.measurement.own_waist = $('#own_waist').val();
    $.tmo.design.measurement.own_len_waist = $('#own_len_waist').val();
    $.tmo.design.measurement.own_shoulder_width = $('#own_shoulder_width').val();
    $.tmo.design.measurement.own_seat = $('#own_seat').val();
    $.tmo.design.measurement.own_len_seat = $('#own_len_seat').val();
    $.tmo.design.measurement.own_shirt_len = $('#own_shirt_len').val();
    $.tmo.design.measurement.own_arm_len = $('#own_arm_len').val();
    $.tmo.design.measurement.own_short_len = $('#own_short_len').val();
    $.tmo.design.measurement.own_wrist = $('#own_wrist').val();
    $.tmo.design.measurement.own_biceps = $('#own_biceps').val();
    $.tmo.design.measurement.own_armpit = $('#own_armpit').val();
    $.tmo.design.measurement.size_measure_best = $("input[name=size_measure_best]:checked").val();
    $.tmo.design.measurement.size_measure_own = $("input[name=size_measure_own]:checked").val();
    //$.tmo.design.measurement.json = JSON.stringify($.tmo.design.measurement);
    var path = "measurement/user/" + username;
    var options = "?measure_type=" + $.tmo.design.measurement.measure_type + "&measure_name=default";
    path += options;
    var method = "PUT";

    $.tmocore.loadServerResource(path, method, JSON.stringify($.tmo.design.measurement), handler);
}

function getMeasurementForUser(handler, username) {
    try {
        var callback = $.Callbacks();
        callback.add(handler);
        if (username.isEmpty()) {

        } else {
            var path = "measurement/user/" + username + "?measure_type=all&measure_name=default";
            var method = "GET";
            $.tmocore.loadServerResource(path, method, '', function(result) {
                if (!result) {
                    return;
                }
                $('#size_measure1').val(result.standard_size);
                $('#best_collar').val(result.best_collar);
                $('#best_chest').val(result.best_chest);
                $('#best_waist').val(result.best_waist);
                $('#best_length_to_waist').val(result.best_length_to_waist);
                $('#best_width').val(result.best_width);
                $('#best_shirt_length').val(result.best_shirt_length);
                $('#best_shoulder_width').val(result.best_shoulder_width);
                $('#best_long_uplen').val(result.best_long_uplen);
                $('#best_long_lowlen').val(result.best_long_lowlen);
                $('#best_short_uplen').val(result.best_short_uplen);
                $('#best_short_lowlen').val(result.best_short_lowlen);
                $('#best_cuff').val(result.best_cuff);
                $('#best_sleeve_width').val(result.best_sleeve_width);
                $('#best_short_opening').val(result.best_short_opening);
                $('#own_neck').val(result.own_neck);
                $('#own_chest').val(result.own_chest);
                $('#own_len_chest').val(result.own_len_chest);
                $('#own_waist').val(result.own_waist);
                $('#own_len_waist').val(result.own_len_waist);
                $('#own_shoulder_width').val(result.own_shoulder_width);
                $('#own_seat').val(result.own_seat);
                $('#own_len_seat').val(result.own_len_seat);
                $('#own_shirt_len').val(result.own_shirt_len);
                $('#own_arm_len').val(result.own_arm_len);
                $('#own_short_len').val(result.own_short_len);
                $('#own_wrist').val(result.own_wrist);
                $('#own_biceps').val(result.own_biceps);
                $('#own_armpit').val(result.own_armpit);
                $('input:radio[name=size_measure_own]').filter('[value=' + result.size_measure_own + ']').attr('checked', true);
                $('input:radio[name=size_measure_best]').filter('[value=' + result.size_measure_best + ']').attr('checked', true);
                callback.fire();
            });
        }
    } catch (err) {
        //TODO: not always working
        $.tmocore.error(err);
        callback.fire();
    }

}
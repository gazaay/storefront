/**
 *
 */
(function() {
    $.tmo_shop = {
        inventory : {
            save : function(handler, useSelectedUsername) {
                saveInventory(handler, useSelectedUsername);
            },
            get : function(handler, id) {
                getInventory(handler, id)
            },
            set_data : function(data) {
                $('#edit_fabric_name').val(data.fabric_name);
                $('#edit_id').val(data.id);
                $('#edit_price').val(data.price);
                $('#edit_price_collar_contrast').val(data.price_collar_contrast);
                $('#edit_price_cuff_contrast').val(data.price_cuff_contrast);
                $('#edit_price_placket').val(data.price_placket);
                $('#edit_composition').val(data.composition);
                $('#edit_yarn').val(data.yarn);
                $('#edit_colour').val(data.colour);
                $('#edit_weaving').val(data.weaving);
                $('#edit_treatment').val(data.treatment);
                $('#edit_type').val(data.type);
                $('#edit_new_code').val(data.new_code);
                $('#edit_active').val(data.active);
                $('#edit_image_url').val(data.uri);
                $('#edit_thumb').attr('src', data.uri);
            }
        },
        shirt : {
            save : function(username, handler) {
                saveShirt(handler, username);
            },
            get : function(id, handler) {
                getShirt(handler, id)
            },
            fill : function(data) {
                if (data) {
                    $('#edit_shirt_id').val(data.shirtId);
                    $('#edit_shirt_Title').val(data.Title);
                    $('#edit_shirt_sub_title').val(data.sub_title);
                    $('#edit_shirt_details').val(data.details);
                    $('#edit_shirt_fit').val(data.fit);
                    $('#edit_shirt_sleeve').val(data.sleeve);
                    $('#edit_shirt_code').val(data.code);
                    $('#edit_shirt_price').val(data.price);
                    $('#edit_shirt_pattern_json').val(data.pattern_json);
                    $('#edit_shirt_reference_key').val(data.reference_key);
                    $('#edit_shirt_image_url').val(data.main_image_url);
                    $('#edit_shirt_thumb').val(data.thumb_url);
                    $('#edit_shirt_thumb2').val(data.thumb_2_url);
                    $('#edit_shirt_thumb3').val(data.thumb_3_url);
                    $('#edit_shirt_thumb4').val(data.thumb_4_url);
                }
            }
        }
    };
})();

function getInventory(handler, id) {
    try {
        var callback = $.Callbacks();
        callback.add(handler);
        var path = "inv/" + id;
        var method = "GET";
        var data = "";
        $.tmocore.loadServerResource(path, method, data, handler);
    } catch (err) {
        //TODO: not always working
        $.tmocore.error(err);
        callback.fire();
    }
}

function saveInventory(handler, username) {
    try {
        var callback = $.Callbacks();
        var method = "POST";
        callback.add(handler);
        var result_inv = {};
        result_inv.fabric_name = $('#edit_fabric_name').val();
        result_inv.id = $('#edit_id').val();
        result_inv.price = $('#edit_price').val();
        result_inv.price_collar_contrast = $('#edit_price_collar_contrast').val();
        result_inv.price_cuff_contrast = $('#edit_price_cuff_contrast').val();
        result_inv.price_placket = $('#edit_price_placket').val();
        result_inv.composition = $('#edit_composition').val();
        result_inv.yarn = $('#edit_yarn').val();
        result_inv.colour = $('#edit_colour').val();
        result_inv.weaving = $('#edit_weaving').val();
        result_inv.treatment = $('#edit_treatment').val();
        result_inv.type = $('#edit_type').val();
        result_inv.new_code = $('#edit_new_code').val();
        result_inv.active = $('#edit_active').val();
        result_inv.active = $('#edit_active').val();
        result_inv.uri = $('#edit_image_url').val();
      
        if (result_inv.id >= 1) {
            method = "PUT";
        } else {
            method = "POST";
        }
        $.tmocore.loadServerResource("inv/" + $('#edit_id').val(), method, JSON.stringify(result_inv), function(result) {
            callback.fire()
        });
    } catch (err) {
        //TODO: not always working
        $.tmocore.error(err);
        callback.fire();
    }
}

function getShirt(handler, id) {
    try {
        var callback = $.Callbacks();
        callback.add(handler);
        var path = "shirt/" + id;
        var method = "GET";
        var data = "";
        $.tmocore.loadServerResource(path, method, data, handler);
    } catch (err) {
        //TODO: not always working
        $.tmocore.error(err);
        callback.fire();
    }
}

function saveShirt(handler, username) {
    try {
        var callback = $.Callbacks();
        var method = "POST";
        callback.add(handler);
        var result_shirt = {};
        result_shirt.id = $('#edit_shirt_id').val();
        result_shirt.Title = $('#edit_shirt_Title').val();
        result_shirt.sub_title = $('#edit_shirt_sub_title').val();
        result_shirt.details = $('#edit_shirt_details').val();
        result_shirt.fit = $('#edit_shirt_fit').val();
        result_shirt.sleeve = $('#edit_shirt_sleeve').val();
        result_shirt.code = $('#edit_shirt_code').val();
        result_shirt.price = $('#edit_shirt_price').val();
        result_shirt.pattern_json = $('#edit_shirt_pattern_json').val();
        result_shirt.reference_key = $('#edit_shirt_reference_key').val();
        result_shirt.main_image_url = $('#edit_shirt_image_url').val();
        result_shirt.thumb_url  = $('#edit_shirt_thumb').val();
       result_shirt.thumb_2_url = $('#edit_shirt_thumb2').val();
        result_shirt.thumb_3_url = $('#edit_shirt_thumb3').val();
        result_shirt.thumb_4_url  = $('#edit_shirt_thumb4').val();
        if (result_shirt.id >= 1) {
            method = "PUT";
        } else {
            method = "POST";
        }
        $.tmocore.loadServerResource("shirt/" + $('#edit_shirt_id').val(), method, JSON.stringify(result_shirt), function(result) {
            callback.fire();
        });
    } catch (err) {
        //TODO: not always working
        $.tmocore.error(err);
        callback.fire();
    }
}


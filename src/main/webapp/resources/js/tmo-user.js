/**
 *
 */
(function() {
    $.tmouser = {
        registration : function() {
            var user = {};
            user.username = $('#email').val();
            user.password = $('#reg_password').val();
            user.firstname = $('#first_name').val();
            user.lastname = $('#last_name').val();
            user.country = $('#country').val();
            user.phone = $('#phone').val();
            // user.signup = $('#newsletter').checked();
            // var formInput = $("form#form").serialize();
            var url = 'signup';
            $.tmocore.loadServerResource(url, 'POST', JSON.stringify(user), function(data) {
                $('.loadingImg').css({
                    display : "none"
                });

                if (data) {
                    $('.normal_content').css({
                        display : "none"
                    });
                    $('.info_box').css({
                        display : "block"
                    });
                    $('.info_box').text(data.description);
                    $('.registered').fadeIn('fast');
                    if ($.browser.msie) {//IE, bool var, has to be defined
                        var newlocation = document.createElement('a');
                        newlocation.href = URLtoCall;
                        document.body.appendChild(newlocation);
                        newlocation.click();
                    } else {
                        window.location.href = document.referrer;
                    }

                } else {

                    $('.warning_box').css({
                        display : "block"
                    });
                    $('.warning_box').text(data.description);
                    $('.submit-btn').fadeIn('fast');
                }
                // });
            });
        }
    };
})(); 
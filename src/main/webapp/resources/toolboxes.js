function createOrRefreshToolBox(name) {
    var toolbox_id = name + "_toolbox";
    var has_zoom = false;
    if ($('#' + name).hasClass("zoom")) {
        has_zoom = true;
    }
    if ($('#' + name).hasClass("page")) {
        $('#previous').show();
        $('#next').show();
    } else {
        $('#previous').hide();
        $('#next').hide();
    }
    // $("#" + toolbox_id).append("<ul clas's=\"att_list\"></ul>");
    if (toolbox_id == "add_on_toolbox") {
        $("#" + toolbox_id).prepend($("#design_cost"));
        // $("#" + toolbox_id).append($("#cart_buttonWidget"));
    }

    if ($("#" + toolbox_id).hasClass("preset_toolbox")) {
        $(".measure_middle").removeClass("show");
        $("#" + name + "_middle").addClass("show");
        $("#" + name + "_left").addClass("show");
        if (!$("#" + toolbox_id).hasClass("options_toolbox")) {
            $("#" + toolbox_id).append($("#measure_widget"));
        }
        return;
    }
    if ($("#" + toolbox_id).length) {

        $("#" + toolbox_id).append($("#design_page"));
        if (name == 'loading_box') {
            $("#" + toolbox_id).append($("#progress_bar"));
        }
        if (name == 'cuff') {
            $("#" + toolbox_id).append($("#cuff_options"));
            $("#" + toolbox_id).append($("#personalize_options"));
        }
        if (name == 'shirt_sleeve') {
            $("#" + toolbox_id).append($("#shirt_type_options"));
        }
        $("#" + toolbox_id).append($("#cart_buttonWidget"));
        $("#" + toolbox_id).prepend($("#toolbox_filter"));
        if (has_zoom) {

            $("#" + toolbox_id).prepend($("#zoom"));
        }
        $("#" + toolbox_id).prepend($("#design_cost"));
        $(document).trigger(name + '_design_refresh');
        // TODO: highlight selected here;
        return;
    }
    $("#toolboxes").append("<div class='" + name + " toolbox' id='" + name + "_toolbox'></div>");

    $("#" + toolbox_id).append($("#design_cost"));
    if (has_zoom) {

        $("#" + toolbox_id).append($("#zoom"));
    }
    $("#" + toolbox_id).append($("#toolbox_filter"));

    $("#" + toolbox_id).append("<div id='" + toolbox_id + "_list' class='att_list'></div>");

    $("#" + toolbox_id).append($("#design_page"));
    if (name == 'cuff') {
        $("#" + toolbox_id).append($("#cuff_options"));
        $("#" + toolbox_id).append($("#personalize_options"));
    }
    if (name == 'shirt_sleeve') {
        $("#" + toolbox_id).append($("#shirt_type_options"));
    }
    $("#" + toolbox_id).append($("#cart_buttonWidget"));

    $(document).bind(name + '_design_refresh', function() {
        if (name == "loading_box") {
            return;
        }
        loadServerResource("inv/category_name/" + name, "GET", "max=300&page=" + $.tmo.design.page_number, function(results) {
            data = results;
            $.tmo.data.inventory = data;
            // var list_colour = GetList('colour',
            // $.tmo.data.inventory);
            // var list_pattern = GetList('type',
            // $.tmo.data.inventory);
            // TODO: fix this logic
            if ((name != 'shirt_types') && (name != 'shirt_sleeve') && (name != 'buttons') && (name != 'button_thread') && (name != 'pocket') && (name != 'collar') && (name != 'cuff') && (name != 'back') && (name != 'yoke') && (name != 'fastening') && (name != 'bottom_cuts')) {
                var filterControl = FilterBy('colour', 'option', $.tmo.data.inventory);
                // $('#filter_buttonWidget').html(filterControl);
                filterControl = FilterBy('type', 'option', $.tmo.data.inventory);
                // $('#filter_buttonWidget').append(filterControl);
                $('#filter_buttonWidget').show();
            } else {
                $('#filter_buttonWidget').hide();
            }

            $('#' + name + '_toolbox_list div').remove();
            $.each(data, function(index, item) {
                if (!item.active) {
                    return;
                }
                var fabric_name = item.fabric_name;
                var item_value = item.id;
                var item_url = item.uri;
                var activeclass = "";
                if (fabric_name.indexOf("default") != -1 || fabric_name.indexOf("Matching") != -1) {
                    item_value = getDefaultValue(name);
                }
                var priceTag = "";

                switch (name) {
                    case 'fabrics':
                        if (item_value == $.tmo.design.map['fabrics']) {
                            activeclass = "activeBorder";
                        }
                        priceTag = "<span class='unit_price'>" + item.price + "</span>";
                        break;
                    case 'collar':
                        if (item_value == $.tmo.design.map['collar']) {
                            activeclass = "activeBorder";
                        }
                        break;
                    case 'cuff':
                        if (item_value == $.tmo.design.map['cuff']) {
                            activeclass = "activeBorder";
                        }
                        break;
                    case 'buttons':
                        if (item_value == $.tmo.design.map['buttons']) {
                            activeclass = "activeBorder";
                        }
                        break;
                    case 'button_thread':
                        if (item_value == $.tmo.design.map['button_thread']) {
                            activeclass = "activeBorder";
                        }
                        break;
                    case 'collar_outer':
                        if (item_value == $.tmo.design.map['collar_outer']) {
                            activeclass = "activeBorder";
                        }
                        priceTag = "<span class='unit_price'>" + item.price_collar_contrast + "</span>";
                        break;
                    case 'collar_inner':
                        if (item_value == $.tmo.design.map['collar_inner']) {
                            activeclass = "activeBorder";
                        }
                        priceTag = "<span class='unit_price'>" + item.price_collar_contrast + "</span>";
                        break;
                    case 'cuff_outer':
                        if (item_value == $.tmo.design.map['cuff_outer']) {
                            activeclass = "activeBorder";
                        }
                        priceTag = "<span class='unit_price'>" + item.price_cuff_contrast + "</span>";
                        break;
                    case 'cuff_inner':
                        if (item_value == $.tmo.design.map['cuff_inner']) {
                            activeclass = "activeBorder";
                        }
                        priceTag = "<span class='unit_price'>" + item.price_cuff_contrast + "</span>";
                        break;
                    case 'fastening':
                        if (item_value == $.tmo.design.map['fastening']) {
                            activeclass = "activeBorder";
                        }
                        break;
                    case 'bottom_cuts':
                        if (item_value == $.tmo.design.map['bottom_cuts'])
                            item_url = item.uri.splice(item.uri.indexOf(".jpg"), 0, "-select");
                        break;
                    case 'pocket':
                        if (item_value == $.tmo.design.map['pocket'])
                            item_url = item.uri.splice(item.uri.indexOf(".jpg"), 0, "-select");
                        break;
                    case 'back':
                        if (item_value == $.tmo.design.map['back'])
                            item_url = item.uri.splice(item.uri.indexOf(".jpg"), 0, "-select");
                        break;
                    case 'yoke':
                        if (item_value == $.tmo.design.map['yoke'])
                            item_url = item.uri.splice(item.uri.indexOf(".jpg"), 0, "-select");
                        break;

                    case 'shirt_sleeve':
                        if (item_value == $.tmo.design.map['shirt_sleeve'])
                            item_url = item.uri.splice(item.uri.indexOf(".jpg"), 0, "-select");
                        break;
                }

                if (item.price == 0) {
                    priceTag = "";
                }
                var colourByComma = item.colour.toLowerCase().split(',');
                var span = " span-3";
                if (name == "pocket" || name == "bottom_cuts") {
                    span = " span-4";
                }
                if (item.uri.indexOf("select") > -1) {
                    return;
                }
                $("#" + name + "_toolbox_list").append("<div id='" + toolbox_id + "_" + item.id + "' value='" + item_value + "' class='" + toolbox_id + "_item filter_all clickable toolbox_selection " + span + SplitByCommaInLabel(" filter_", colourByComma) + " filter_" + item.type.toLowerCase() + "' onclick='OnItemSelect(this," + index + ");'>" + "<div style='display:none' id='value'>" + item_value + "</div><img class='clickable " + activeclass + "' src='" + item_url + "'></img>" + "<br /><span>" + fabric_name + "</span>" + "<br />" + priceTag + "</div>");

            });
            ProcessFilter($.tmo.toolbox.filter);

            // if (item.id == $.tmo.design.map.fabrics)
        });
    });

    $(document).trigger(name + '_design_refresh');
}

function OnItemSelect(obj, index) {
    $.tmo.design.dirty = true;
    if ($.tmo.design.menu_name == "fabrics") {
        var item = $.tmo.data.inventory[index];
        $('#zoom_composition').text(item.composition);
        $('#zoom_yarn').text(item.yarn);
        $('#zoom_colour').text(item.colour);
        $('#zoom_weaving').text(item.weaving);
        $('#zoom_treatment').text(item.treatment);
        $('#faTitle').text(item.fabric_name);
    }

    var name = $(obj).parent().attr('id');
    obj.value = $("#" + $(obj).attr('id') + " #value").text();
    $.tmo.design.history.maps.push(JSON.stringify($.tmo.design.map));
    $.tmo.design.map[name.replace("_toolbox_list", "")] = parseInt(obj.value);
    $.tmo.design.user_selection[name.replace("_toolbox_list", "")] = parseInt(obj.value);
    $(document).trigger($.tmo.design.menu_name + '_design_refresh');

    // $('#' + $.tmo.design.menu_name).click();
    //	if ($.tmo.design.menu_name == "back"){
    //	}
    //	if ($.tmo.design.menu_name == "yoke"){
    //		$('#yoke').click();
    //	}
    //	if ($.tmo.design.menu_name == "pocket"){
    //		$('#pocket').click();
    //	}
    //	if ($.tmo.design.menu_name == "shirt_sleeve"){
    //		$('#shirt_sleeve').click();
    //	}
    if ($.tmo.design.menu_name == "shirt_sleeve") {
        // short sleeve don't have cuffs
        if ($.tmo.design.user_selection["shirt_sleeve"] === 240 || $.tmo.design.user_selection["shirt_sleeve"] === 242) {
            $(".cuff").addClass("hide_cuff");
        } else {
            $(".cuff").removeClass("hide_cuff");
        }
    }
    if ($.tmo.design.menu_name == "fabrics") {
        var price = $("#" + $(obj).attr('id') + " .unit_price").text();
        $('#fabric_price strong').text("$" + price);
        loadServerResource("shirt/reference/" + $.tmo.design.map.fabrics, "GET", "", function(results) {
            if (results) {
                $.tmo.design.map = JSON.parse(results.pattern_json);
                $.tmo.design.map_default = JSON.parse(results.pattern_json);
            }
        }).done(function() {
            // all inner and outer should be the same color
            $.tmo.design.map["cuff_outer"] = parseInt(obj.value);
            $.tmo.design.map["cuff_inner"] = parseInt(obj.value);
            $.tmo.design.map["collar_inner"] = parseInt(obj.value);
            $.tmo.design.map["collar_outer"] = parseInt(obj.value);
            $.extend($.tmo.design.map, $.tmo.design.user_selection);
            $(document).trigger('model_refresh');
            $(document).trigger("price_refresh");
        });
    } else {
        $.extend($.tmo.design.map, $.tmo.design.user_selection);
        $(document).trigger('model_refresh');
    }
    $('#revert_item_selection').show();
}

function revertSelection() {
    if ($.tmo.design.history.maps.length > 0) {
        $.tmo.design.map = JSON.parse($.tmo.design.history.maps.pop());
        $(document).trigger('model_refresh');
    }

    if ($.tmo.design.history.maps.length == 0) {
        $('#revert_item_selection').hide();
    } else {
        $('#revert_item_selection').show();
    }
}

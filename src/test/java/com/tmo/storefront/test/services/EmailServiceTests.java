package com.tmo.storefront.test.services;

import javax.mail.MessagingException;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.web.services.SendEmailService;


@ActiveProfiles(profiles = "development")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:production_profiles.xml",
		"classpath:development_profiles.xml",
		"classpath:springmvc-servlet.xml",
		"classpath:applicationContext.xml",
		"classpath:spring-security.xml" })
public class EmailServiceTests {
	@Autowired
	SendEmailService service;

	@Test
	public void TestEmailSendService() throws MessagingException {
		String username="";
		String emailBody="";
		service.send(username,"", emailBody);
		service.sendEmail("", "garylamj@gmail.com", "test", "aBody");
		Assert.assertEquals(true, false);
	}
}

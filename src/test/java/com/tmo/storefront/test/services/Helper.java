package com.tmo.storefront.test.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.ShirtAttribute;
import com.tmo.storefront.web.services.ShirtServices;

import static org.mockito.Mockito.*;

@Service
public class Helper {
	public Helper() {
	}
	@Autowired
	ShirtServices shirtService;

	public Shirt createMockShirt() {
		List<ShirtAttribute> attributes = mock(List.class);
		ShirtAttribute attribute = mock(ShirtAttribute.class);
		attribute.setName("mocking attribute");
		attributes.add(attribute);
		Shirt shirt = new Shirt();
		shirt.setPattern_json("{\"fabrics\" : 58,\"shirt_type\" : 230,\"cuff\" : 49,\"cuff_outer\" : 52,\"cuff_inner\" : 52,\"collar_outer\" : 52,\"collar_inner\" : 52,\"bottom_cuts\" : 12,\"collar\" : 40,\"shirt_sleeve\" : 236,\"pocket\" : 214,\"buttons\" : 23,\"fastening\" : 209,\"button_thread\" : 248,\"yoke\":280,\"long_sleeve\" : 9999} ");
		shirt.setAttributes(shirtService.getShirtAttributesFromJSON(shirt.getPattern_json()));
		return shirt;
	}

}

package com.tmo.storefront.test.services;

import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.ShirtServices;

@ActiveProfiles(profiles = "production")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:production_profiles.xml",
		"classpath:development_profiles.xml",
		"classpath:springmvc-servlet.xml", "classpath:applicationContext.xml" })
public class InvoiceServiceTests {
	@Autowired
	InvoiceService service;

	Logger logger = Logger.getLogger(InvoiceServiceTests.class);
	
	@Test
	public void getListOfInvoices() {
		List<Invoice> invoices = service.getInvoicesInfo();
		Assert.assertTrue(invoices.size() > 0);
		Assert.assertTrue(invoices.get(3) != null);
		logger.info(invoices.get(3).getId());
		
	}
}
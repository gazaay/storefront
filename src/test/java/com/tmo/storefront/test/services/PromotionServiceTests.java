package com.tmo.storefront.test.services;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.domain.Cart;
import com.tmo.storefront.domain.Message;
import com.tmo.storefront.web.services.PromotionService;
import com.tmo.storefront.web.services.SecurityService;

@ActiveProfiles(profiles = "local")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:production_profiles.xml",
		"classpath:development_profiles.xml", "classpath:local_profiles.xml",
		"classpath:springmvc-servlet.xml", "classpath:applicationContext.xml",
		"classpath:spring-security.xml" })
public class PromotionServiceTests {
	@Autowired
	PromotionService service;
	private Logger logger = Logger.getLogger(PromotionServiceTests.class);

	@Test
	public void promotionProcessCart() throws Exception {

		Cart cart = new Cart();
		String promotion = "POEGEAFW";
		String username = "admin";
		service.processCart(cart, promotion, username, new Message());
		Message msg = service.validatePromotionCode("garylamj@gmail.com", "QWERTYUI", new Message());
		Assert.assertTrue(msg.isValid());
	}
}

package com.tmo.storefront.test.services;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.web.services.SecurityService;

@ActiveProfiles(profiles = "development")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:production_profiles.xml",
		"classpath:development_profiles.xml",
		"classpath:springmvc-servlet.xml", "classpath:applicationContext.xml",
		"classpath:spring-security.xml" })
public class SecurityTests {
	@Autowired
	SecurityService service;
	private Logger logger = Logger.getLogger(SecurityTests.class);

	@Test
	public void PasswordGenerator() {
		String password = service.generateRandomPassword();
		logger.info("New Password" + password);
		Assert.assertTrue(password !=null);

	}

}

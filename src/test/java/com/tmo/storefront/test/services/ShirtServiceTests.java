package com.tmo.storefront.test.services;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.web.services.ShirtServices;

@ActiveProfiles(profiles = "development")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:production_profiles.xml",
		"classpath:development_profiles.xml",
		"classpath:springmvc-servlet.xml", "classpath:applicationContext.xml",
		"classpath:spring-security.xml" })
public class ShirtServiceTests {
	@Autowired
	ShirtServices service;

	@Test
	public void getShirtByRefKey() {
		Shirt shirt = service.getShirtByReferenceKey("TMO18001");
		Assert.assertEquals(79.90f, shirt.getPrice());
	}
	
	@Test
	public void cannotGetShirtByRefKey() {
		Shirt shirt = service.getShirtByReferenceKey("TMO18001xx");
		Assert.assertNull(shirt);
	}
	
	@Test
	public void getListOfShirts(){
		List<Shirt> shirts = service.getShirtsByReferenceKey("TMO");
		Assert.assertTrue("The shirt list should containts more than one TMO shirt", shirts.size() > 0);
	}
}

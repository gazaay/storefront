package com.tmo.storefront.test.services;

import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tmo.storefront.domain.Invoice;
import com.tmo.storefront.domain.Shirt;
import com.tmo.storefront.domain.Wish;
import com.tmo.storefront.web.services.InvoiceService;
import com.tmo.storefront.web.services.ShirtServices;
import com.tmo.storefront.web.services.WishService;

@ActiveProfiles(profiles = "local")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:local_profiles.xml",
		"classpath:springmvc-servlet.xml", "classpath:applicationContext.xml",
		"classpath:spring-security.xml",
		"classpath:spring-test.xml"})
public class WishServiceTest {

	@Autowired
	WishService wishService;
	@Autowired
	ShirtServices shirtService;

	@Autowired
	Helper helper;

	Logger logger = Logger.getLogger(WishServiceTest.class);

	@Test
	public void testCreateNewWishItem() {
		Shirt shirt = helper.createMockShirt();
		String username = "gary";
		String session_id = "test@#$@#";
		wishService.saveWishListWithShirt(shirt, username, session_id);
		Wish wish = wishService.getWishBy(username, session_id);
		Assert.assertNotNull(wish);
		Assert.assertNotNull(wish.getShirt());
		wish.getShirt().setAttributes(shirtService.getShirtAttributesFromJSON(shirt.getPattern_json()));
		Assert.assertNotNull(wish.getShirt().getAttributes());
		Assert.assertTrue(wish.getShirt().getAttributes().size() > 0);
	}
}
